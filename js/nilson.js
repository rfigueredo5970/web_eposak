$(document).ready(function()
{
	var server = window.location.origin+"/esposak/";
	var server_production = window.location.origin+"/";
	
	function initMap() {
		var long = parseFloat($('#long-map').val());
		var lat = parseFloat($('#lat-map').val());
		var myLatLng = {lat: lat, lng: long};
		// Create a map object and specify the DOM element for display.
		var map = new GMaps( {
			div: "#map",
			scrollwheel: false,
			zoom: 12,
			lat:lat,
			lng:long
		});

		// Create a marker and set its position.
		map.addMarker({
			lat: lat,
			lng: long,
		});
	}
	
	if($('#long-map')[0] && $('#lat-map')[0] && $('#map')[0]){
		initMap()
	}
	$('.dropdown-button').dropdown();
	
	
	/***********************CAROUSEL VIAJE INDIVIDUAL*********************************************************/
	$(".selectImg").on("click", function(value){
		$('#big-img').css('background-image', 'url('+server+'images/' + $(this).data("img") + ')');
	})
	
});