$(document).ready(function()
{
 
	$('.loadMore').on("click", function(e) 
	{
		//console.log("loadMore", $(this));
		
		e.preventDefault();
				var current_page = parseInt($(this).attr("id"));
        var id = $(this).data("travel");
        var getLastId, html = "";
        if (id != "" || id != "undefined") {

             $.ajaxSetup({
                headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : "GET",
                url : window.location.origin+"/esposak/masviajes/"+id+"?page="+(current_page + 1),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                beforeSend: function()
                {
                },
                success : function(data) 
                {
									$(".loadMore").attr("id",data.current_page);
									if(data.last_page == data.current_page){
										$(".loadMore").hide();
									}
									
									jQuery.each( data.data, function( i, val ) {
										var html = "<div class='row'><div class='col s12 padding0 viaex' style='background-image:url("+window.location.origin+"/esposak/images/"+val.multimedias[Math.floor(Math.random() * (val.multimedias.length))].name+")'><h1>"+val.title_es+"<br></h1><h2><i class='fa fa-map-marker' aria-hidden='true'></i>"+val.destiny.name_es+"</h2><div class='verdeprecio'><p>PRECIO Bs "+val.prices+"</p></div><div class='carnedia'><p>"+val.days+" DÌAS </p></div><img src='"+window.location.origin+"/esposak/maqueta/img/fondonegro.png'></div></div>";
										$("#container-travels").append(html);
									});
	                 
                    document.getElementById('final') //.scrollIntoView(true);
                },
                error: function(err)
                {
                	//alert("error");
                }
            });
        }
        return false;
    });
});