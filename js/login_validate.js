function validateEmail(email) {
	// verifica si el patrón del string corresponde a un dirección de correo válida
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}



$(document).ready(function() {




	$("#btn-login").click(function(e) {

		e.preventDefault();

		$('#validation_error').text("").hide();
		$('#validation_message').text("").hide();

		var userName = $("#user_name_2").val();

		var password = $("input[name='password_2']").val();

		var rememberMe = $("#filled-in-box").val();

    console.log(rememberMe);

		if (userName.replace(/\s/g, '').length < 3) {
			$('#validation_error').text("El nombre de usuario debe tener más de 3 caracteres").show();
		} else if (password.replace(/\s/g, '').length < 3) {

			$('#validation_error').text("la contraseña debe tener más de 3 caracteres").show();

		} else {

			var input = {
				"user_name": userName,
				"password": password,
				"remember": rememberMe
			}

				
							


			$.ajax({
				url: "http://developers.comwaredigital.com.ve/esposak/login_ajax",
				method: "POST",
				dataType: "json",
				data: input,
				success: function(d) {


					$('#validation_error').text("").hide()



					$('#validation_message').text(d.message).show();

					location.reload();




				},

				error: function(e) {
					if (e.status == 500) {


						console.log(e.responseJSON.error);

						$('#validation_message').text("").hide();
						$('#validation_error').text(e.responseJSON.error).show();

					}
					if (e.status == 404) {
						console.log(e.responseJSON.error);
						$('#validation_message').text("").hide();
						$('#validation_error').text(e.responseJSON.error).show();

					}

				}


			});

		}

	});



});