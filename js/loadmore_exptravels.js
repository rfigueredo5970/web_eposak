$(document).ready(function()
{ 
	var server = window.location.origin+"/esposak/";
	var server_production = window.location.origin+"/";
	$('#loadMoreButton').on("click", function(e) 
	{
		//e.preventDefault();
		var travel_type = $(this).data("travel");
		var page = $(this).data("page");
            
		$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });

		$.ajax({
                type : "GET",
                url : server+"masviajes/"+travel_type+"?page="+page,
                success : function(data) 
                {
									$('#loadMoreButton').data("page", parseInt(data.current_page) + 1);
									if(data.current_page == data.last_page){
										$('#loadMoreButton').hide();
									}
									
									jQuery.each( data.data, function( key, val ) {
										var html = '<div class="row">';
											html += '<a href="'+server+'viaje/individual/'+val.slug+'">';
											html += '<div class="col s12 padding0 viaex" style="background-image:url('+server+'images/'+val.multimedias[Math.floor(Math.random()*val.multimedias.length)].name+')">';
											html +=  '<h1>'+val.title_es+'<br></h1>';
											html += '<h2><i class="fa fa-map-marker" aria-hidden="true"></i>'+val.destiny.name_es+'</h2>';
											html += '<div class="verdeprecio"><p>PRECIO Bs '+val.prices+'</p></div>';
											html += '<div class="carnedia"><p>'+val.days+' DÌAS </p></div>';
											html += '<img src="'+server+'/maqueta/img/fondonegro.png" alt="" class="">';
											html += '</div></a></div>';
										
										$('#container-travels').append(html);
									});
                },
                error: function(error)
                {
                	console.log(error);
                }
            });
		return false;
	});
	
	$('.loadMoreDestiny').on("click", function(e) 
	{
		//e.preventDefault();
		var travel_type = $(this).data("type");
		var page = $(this).data("page");
		var destiny = $(this).data("destiny");
		var id = $(this).attr("id");
		
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});

		$.ajax({
                type : "GET",
                url : server+"loadTravelByDestiny/"+travel_type+"/"+destiny+"?page="+page,
                success : function(data) 
                {
									$('#loadMoreButton').data("page", parseInt(data.current_page) + 1);
									
									if(data.current_page == data.last_page){
										if(id == "experiencial-travel"){
											$('#experiencial-travel').hide();
										}else{
											$('#voluntary-travel').hide();
										}
									}
								
									jQuery.each( data.data, function( key, val ) {
										var html = '<div class="row">';
											html += '<a href="'+server+'viaje/individual/'+val.slug+'">';
											html += '<div class="col s12 padding0 viaex" style="background-image:url('+server+'images/'+val.multimedias[Math.floor(Math.random()*val.multimedias.length)].name+')">';
											html +=  '<h1>'+val.title_es+'<br></h1>';
											html += '<h2><i class="fa fa-map-marker" aria-hidden="true"></i>'+val.destiny.name_es+'</h2>';
											html += '<div class="verdeprecio"><p>PRECIO Bs '+val.prices+'</p></div>';
											html += '<div class="carnedia"><p>'+val.days+' DÌAS </p></div>';
											html += '<img src="'+server+'/maqueta/img/fondonegro.png" alt="" class="">';
											html += '</div></a></div>';
										
												if(id == "experiencial-travel"){
													$('#container-experiencial-travel').append(html);
												}else{
													$('#container-voluntary-travel').append(html);
												}
									});
                },
                error: function(error)
                {
                	console.log(error);
                }
            });
		return false;
	});
});