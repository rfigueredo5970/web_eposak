function validateEmail(email) {
	// verifica si el patrón del string corresponde a un dirección de correo válida
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



$(document).ready(function(){
	
		$("#btn-register").click(function(){
			
			$('#validation_error').text("").hide();
			$('#validation_message').text("").hide();
			
			var userName = $("#user_name").val();
			
			var email = $("#email").val();
			
			var password = $("input[name='password']").val()
			
			var confirmation = $("input[name='password_confirmation']").val()
			
		
		if (userName.replace(/\s/g,'').length < 3 ){
			$('#validation_error').text("El nombre de usuario debe tener más de 3 caracteres").show();
		} else if(email.replace(/\s/g,'').length < 3){
			
			$('#validation_error').text("la dirección de correo debe tener más de 3 caracteres").show();
			
			}else if(!validateEmail(email)){
				
					$('#validation_error').text("debe introducir una dirección de correo válida").show();
				 
			}	else if(password.replace(/\s/g,'').length < 3 ){
				
					$('#validation_error').text("la contraseña debe tener más de 3 caracteres").show();
			
			} else if(confirmation.replace(/\s/g,'').length < 3 ){
				
					$('#validation_error').text("La contraseña no coincide con la confirmación").show();
			
		} else if(confirmation !== password){
			
			$('#validation_error').text("La contraseña no coincide con la confirmación").show();
		}else{
			
			
	
					
		var input = { "user_name": userName, "email": email, "password": password, "password_confirmation":confirmation}
		
		console.log(input);
		
		
		
		$.ajax({
			url: "http://developers.comwaredigital.com.ve/esposak/registration_form",
			method: "POST",
			dataType: "json",
			data: input ,	
			success: function(d){
				
						
						$('#validation_error').text("").hide();
				

								 console.log(d);
							 console.log(d.message);
				
           
					
					$('#validation_message').text(d.message).show();        
				
			}, 
			
			error :function(e){
				if(e.status == 500)
					{   
						
						
						console.log(e.responseJSON.error);
						
						$('#validation_message').text("").hide();
						$('#validation_error').text(e.responseJSON.error ).show();
						
					}
				if(e.status == 404)
					{
						console.log(e.responseJSON.error);
					$('#validation_message').text("").hide();	
					$('#validation_error').text(e.responseJSON.error).show();
						
					}
				
			}
			
			
		}); 
	
		}
			
	});	
	
	
	
});


	
