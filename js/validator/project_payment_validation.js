// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
 
  
  $("form[name='project']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      firstname: "required",
      lastname: "required",
      nationality: "required",
      document: "required",
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      phone: {
        required: true,
        number: true
      },
      
       contribution: {
        number: true,
         min:50
      },
  
    },
    // Specify validation error messages
    messages: {
      firstname: "Introduce tu nombre",
      lastname: "Introduce tu apellido", 
      nationality: "Ingresa tu nacionalidad",
      email: "Ingresa una dirección de email válida",
      number:"Ingresa solo números en este campo"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.valid();
    }
  });
  
});