@extends('layouts.master-admin')

@section('content')
               	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Informacion general de esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="generalinformation/create">Crear nuevo Destino</a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Titulo (Es)</th>
                                            <th class="column-title">seccion de navedor </th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($general as $information)
                                        <tr class="even pointer">
                                            <td class=" ">{{$information->id}}</td>
                                            <td class=" ">{{$information->title_es}}</td>
                                            <td class=" ">{{$information->section_navbar->title_es}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="generalinformation/{{ $information->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/generalinformation/{{ $information->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop