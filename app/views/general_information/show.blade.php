@extends('layouts.master-admin')

@section('content')
                <!-- /top tiles -->
        			<div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>{{ $general->title_es }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/generalinformation" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{ Form::open(array('url'=> 'generalinformation/'.$general->id, 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>{{ $general->title_es }} </h1><br>
                                <h3 style="margin-top:-10px;" >{{ $general->title_en }} </h3>
                                <br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Español-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $general->description_es }}</p>
                                        <h6 style="opacity:0.50;">-----------------Ingles-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $general->description_en }}</p>
                                    </div>
                                    @if(isset($general->url_multimedia))
                                    <div class="text-center col-md-6" >

                                        <div class="col-md-12" >
                                            <span>URL Multimedia</span>
                                            {{ $general->url_multimedia }}
                                        </div>
                                    </div>
                                    @else
                                        <div class="text-center col-md-6" >

                                        <div class="col-md-12" >
                                            <span>URL Multimedia</span>
                                            <h4>Este Registro no posee Url</h4>
                                        </div>
                                    </div>
                                    @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop