@extends('layouts.master-admin')
    @section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Artículos Esposak<small> listado de articulos internos</small></h2>
                        <div class="clearfix"></div>	
                    </div>
                    @foreach($articles as $article)
                        <div class="col-md-55">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                    {{ HTML::Image('/images/'.$article->image, null ,array('style' => 'width: 100% ;display: block')) }}
                                    <div class="mask no-caption">
                                        <div class="tools tools-bottom">
                                            <a href="/esposak/articles/{{ $article->id }}/edit"><i class="fa fa-pencil"></i></a>
                                    		<a href="articles/{{ $article->id }}">Ver</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p><strong>{{ $article->title_es }}</strong>
                                    </p>
                                    <p></p>
                                </div>
                            </div>
                        </div>
					           @endforeach
                </div>
            </div>
        </div>
    @stop
                       