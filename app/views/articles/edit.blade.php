@extends('layouts.master-admin')

@section('content')

                <!-- top tiles -->
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Editar Artículo </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
<!--                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li> -->
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                   {{Form::open(array('url' => 'articles/' . $article->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Título español <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_es', $article->title_es,array('class' => 'form-control'))}}
                                                <div class="error" style="color:red;">{{ $errors->first('title_es') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Título ingles<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_en', $article->title_en,array('class' => 'form-control')) }}  
                                                <div class="error" style="color:red;">{{ $errors->first('title_en') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción español<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_es', $article->description_es,array('class' => 'form-control' , 'style' => 'resize: none;')) }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('description_es') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción ingles<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_en', $article->description_en,array('class' => 'form-control' , 'style' => 'resize: none;')) }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('description_en') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Imagen<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a href="/esposak/articles" class="btn btn-info" >back</a> 
                                                {{Form::submit('Guardar',array('class' => 'btn btn-success'))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                <!-- /top tiles -->

           
                <br />
                @stop
			
    