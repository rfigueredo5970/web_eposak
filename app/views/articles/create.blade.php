@extends('layouts.master-admin')

@section('content')
                
                <!-- /top tiles -->
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Nuevo Artículo<small>Formulario para crear un nuevo Artículo</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/articles" class="btn ">Volver</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'articles', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                    


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Título español<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_es',Input::old('title_es'),array('class' => 'form-control')) }}
                                                <div class="error">{{ $errors->first('title_es') }}</div>
                                                
                                                <br/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Título ingles<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_en',Input::old('title_en'),array('class' => 'form-control')) }}
                                                <div class="error">{{ $errors->first('title_en') }}</div>
                                                <br/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción español<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textArea('description_es',Input::old('description_es'),array('class' => 'form-control' , 'style' => 'resize: none;')) }}
                                                <div class="error">{{ $errors->first('description_es') }}</div>
                                                
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción ingles<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_en',Input::old('description_en '),array('class' => 'form-control' , 'style' => 'resize: none;')) }}
                                               <div class="error">{{ $errors->first('description_en') }}</div>
                                               
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Imagen</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div class="error">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                {{Form::submit('Guardar',array('class' => 'btn btn-success btn-block ')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop