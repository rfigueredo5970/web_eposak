@extends('layouts.master-admin')

@section('content')
        			<div class="x_panel" >
                        <div class="x_title">
                            <h2>{{ $navbar->name }} </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/navbar" class="btn" ><i>Back</i></a>
                                </li>
                               <!-- <li>
                                    {{Form::open(array('url'=> 'navbar/'.$navbar->id, 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}
                                </li>-->
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>{{ $navbar->title_es }} </h1><br>
                                <h3 style="margin-top:-10px;" >{{ $navbar->title_en }} </h3>
                                <br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Español-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $navbar->description_es }}</p>
                                        <h6 style="opacity:0.50;">-----------------Ingles-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $navbar->description_en }}</p>
                                    </div>
                                    <div class="text-center col-md-6 row" >
                                        <div class="col-md-6">
                                            {{ HTML::image('/images/'.$navbar->image,null,array('style' =>'width: 100%'))}}
                                        </div>
                                        @if(isset($navbar->imagen_2))
                                            <div class="col-md-6">
                                                {{ HTML::image('/images/'.$navbar->imagen_2,null,array('style' =>'width: 100%'))}}
                                            </div>
                                        @else
                                            <div class="col-md-6">
                                                Esta seccion no posee segunda imagen
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="text-center row">
                                    <div class="col-md-12">
                                        <span>URL Section(video,imagen,sitio web..):</span>
                                        {{$navbar->url_section}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop