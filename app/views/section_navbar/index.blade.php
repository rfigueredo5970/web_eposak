@extends('layouts.master-admin')

@section('content')
           	 <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                    <ul class="nav navbar-right panel_toolbox">
                                        <a class="btn btn-primary" href="navbar/create">Crear nueva Seccion</a>
                                    </ul>
                                <div class="x_title">
                                    <h2>Secciones Esposak<small> listado de secciones de Esposak</small></h2>
                                    
                                    <div class="clearfix"></div>	
                                </div>
                                    @foreach($navbar as $nav)
                                        <div class="col-md-55">
                                            <div class="thumbnail">
                                                <div class="image view view-first">
                                                    {{ HTML::Image('/images/'.$nav->image, null ,array('style' => 'width: 100% ;display: block')) }}
                                                    <div class="mask no-caption">
                                                        <div class="tools tools-bottom">
                                                            <a href="/esposak/navbar/{{ $nav->id }}/edit"><i class="fa fa-pencil"></i></a>
                                                    		<a href="navbar/{{ $nav->id }}">Ver</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="caption">
                                                    <p><strong>{{ $nav->name }}</strong>
                                                    </p>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
									@endforeach

                                    </div>

                                </div>
                            </div>
                            @stop