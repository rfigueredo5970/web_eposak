@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Editar Seccion<small>Formulario para editar Seccion Esposak</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/navbar" class="btn" >Back</a> 
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                   {{Form::open(array('url' => 'navbar/' . $navbar->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre de seccion <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name', $navbar->name,array('class' => 'form-control'))}}
                                                <div class="error" style="color:red;">{{ $errors->first('name') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">titulo español <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_es', $navbar->title_es,array('class' => 'form-control'))}}
                                                <div class="error" style="color:red;">{{ $errors->first('title_es') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">titulo ingles<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_en', $navbar->title_en,array('class' => 'form-control')) }}  
                                                <div class="error" style="color:red;">{{ $errors->first('title_en') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Url Section<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('url_section', $navbar->url_section,array('class' => 'form-control')) }}  
                                                <div class="error" style="color:red;">{{ $errors->first('url_section') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion español<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_es', $navbar->description_es,array('class' => 'form-control' , 'style' => 'resize: none;')) }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('description_es') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion ingles<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_en', $navbar->description_en,array('class' => 'form-control' , 'style' => 'resize: none;')) }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('description_en') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">imagen<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">imagen 2<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('imagen_2') }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('imagen_2') }}</div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block'))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop