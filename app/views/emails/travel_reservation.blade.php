<div>
	<p>El usuario <strong>{{ $name . " " . $last_name}} </strong>  ha reservado  un viaje </p>
	<br/>
	 <h3> Datos Usuario:<h3>
	 <table border="2px">
	 	<thead>
	 		<tr>
     			<th>Nombre</th>
     			<th>Apellido</th>
     			<th>Email</th>
     			<th>Telefono</th>
  			</tr>
	 	</thead>
	 	<tbody>	
	 			<tr>	 				
					<td>{{$name}}</td>
     				<td>{{$last_name}}</td>
     				<td>{{$email}}</td>
     				<td>{{$phone_number}}</td>	 			
	 			</tr>
	 	</tbody>
	 </table>
	 <br/>
	 <h3> Datos Viaje Reservado:<h3>
	 <table border="2px">
	 	<thead>
	 		<tr>
     			<th>Titulo (es)</th>
     			<th>Titulo (en)</th>
     			<th>Destino</th>
     			<th>Status</th>
     			<th>Precio</th>
     			<th>Días</th>
     			<th>Noches</th>
     			<th>Inicio</th>
     			<th>Fin</th>
  			</tr>
	 	</thead>
	 	<tbody>	
	 			<tr>	 				
					<td>{{$title_es}}</td>
     				<td>{{$title_en}}</td>
     				<td>{{$destiny}}</td>
     				<td>{{$status}}</td>
     				<td>{{$prices}}</td>
     				<td>{{$days}}</td>
     				<td>{{$nights}}</td>
     				<td>{{$start_at}}</td>
     				<td>{{$end_at}}</td>	 			
	 			</tr>
	 	</tbody>
	 </table>
</div>

