@extends('layouts.master-admin')

@section('content')
<div class="x_panel" style="height:600px;">
    <div class="x_title">
        <h2>{{ $collaborator->name }}</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="/esposak/collaborator" class="btn" ><i>Back</i></a>
            </li>
            <li  >
                {{ Form::open(array('url'=> 'collaborator/'.$collaborator->id, 'method' => 'delete')) }}
                {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                {{Form::close()}}
            </li>
        </ul>
        <div class="clearfix"></div>
        <div class="container well text-center " >    
            <h1>{{ $collaborator->users->user_name }} </h1><br>
            <h3 style="margin-top:-10px;" >{{ $collaborator->email }} </h3>
            <h5 style="margin-top:-10px;" >{{ $collaborator->phone_number }} </h5>
            <br>
            <br>
            <div class="row container-fluid " >
                <div class="col-md-6" >
                    <h6 style="opacity:0.50;">-----------------Mensaje(Español)-----------------</h6>
                    <p style="word-wrap: break-word;" >{{ $collaborator->message_es }}</p>
                    <h6 style="opacity:0.50;">-----------------Mensaje(Ingles)-----------------</h6>
                    <p style="word-wrap: break-word;" >{{ $collaborator->message_en }}</p>
                </div>
                <div class="text-center col-md-4" >
                    <h6>-----------------Nacionalidad-----------------</h6>
                    <p style="word-wrap: break-word;" >{{ $collaborator->nationality }}</p><h6>-----------------Contribucion-----------------</h6>
                    <p style="word-wrap: break-word;" >{{ $collaborator->contribution }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@stop