@extends('layouts.master-admin')

@section('content')
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <div class="pricing">
                        <div class="title" style="position: relative;">
                            {{ HTML::image('/images/'.$page->logo,null,array('image-size' => 'cover', 'height' => '50%'))}}
                            <h1>detalles de pagina</h1>
                        </div>
                        <div class="x_content">
                            <div class=" row ">
                                <div class="pricing_features col-md-offset-5">
                                    <ul class="list-unstyled text-left">
                                        <li>boton 1 en <i class="fa fa-check text-success">{{$page->button_1_en}}</i>
                                        <li>boton 1 es <i class="fa fa-check text-success">{{$page->button_1_es}}</i>  
                                        <li>boton 2 en<i class="fa fa-check text-success">{{$page->button_2_en}}</i>
                                        <li>boton 2 es<i class="fa fa-check text-success">{{$page->button_2_es}}</i>
                                        <li>boton 3 en<i class="fa fa-check text-success">{{$page->button_3_en}}</i>
                                        <li>boton 3 es<i class="fa fa-check text-success">{{$page->button_3_es}}</i>
                                        <li>boton 4 en<i class="fa fa-check text-success">{{$page->button_4_en}}</i>
                                        <li>boton 4 es<i class="fa fa-check text-success">{{$page->button_4_es}}</i>
                                        <li>boton 5 en<i class="fa fa-check text-success">{{$page->button_5_en}}</i>
                                        <li>boton 5 es<i class="fa fa-check text-success">{{$page->button_5_es}}</i>
                                        <li>reglas en<i class="fa fa-check text-success">{{$page->legal_rights_en}}</i>
                                        <li>reglas es <i class="fa fa-check text-success">{{$page->legal_rights_es}}</i>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing_footer">
                                <a href="/esposak/page"" class="btn btn-success btn-block" role="button">Back</a>

                            {{ Form::open(array('url'=> 'page/'.$page->id, 'method' => 'delete')) }}
                            {{Form::submit('Eliminar',array('class'=>'btn-block btn btn-danger'))}}
                            {{Form::close()}}    
                            </div>
                        </div>
                    </div>
                </div>
                @stop