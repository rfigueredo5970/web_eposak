@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Form Design <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li> <a class="btn" href="/esposak/page">back</a> 
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                   {{Form::open(array('url' => 'page/' . $page->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">boton 1 Español <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('button_1_en', $page->button_1_en)}} 
                                                <div style="color:red;" class="error">{{ $errors->first('button_1_en') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">boton 1 Ingles <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('button_1_es', $page->button_1_es) }}  
                                                <div style="color:red;" class="error">{{ $errors->first('button_1_es') }}</div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 2  Español  <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_2_en', $page->button_2_en) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_2_en') }}</div>

                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 2 Ingles  <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_2_es', $page->button_2_es) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_2_es') }}</div>

                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 3 Español <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_3_en', $page->button_3_en) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_3_en') }}</div>

                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 3 Ingles <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_3_es', $page->button_3_es) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_3_es') }}</div>

                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 4 Español <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_4_en', $page->button_4_en ) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_4_en') }}</div>

                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 4 Ingles <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_4_es', $page->button_4_es ) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_4_es') }}</div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 5  Español <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_5_en', $page->button_5_en) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_5_en') }}</div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">boton 5 Ingles <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_5_es', $page->button_5_es) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('button_5_es') }}</div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Bases legales Español <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('legal_rights_en', $page->legal_rights_en) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('legal_rights_en') }}</div>

                                            </div>                                        
                                        </div>  
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Bases legales Ingles Español <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('legal_rights_es', $page->legal_rights_es) }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('legal_rights_es') }}</div>

                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> Logo <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('logo') }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('logo') }}</div>

                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                               
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block'))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop