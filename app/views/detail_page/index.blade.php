@extends('layouts.master-admin')

@section('content')
               	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>botones y bases legales<small><a class="btn btn-success" href="page/create">Crear nuevo</a></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">boton 1 en </th>
                                            <th class="column-title">boton 1 es </th>
                                            <th class="column-title">boton 2 en </th>
                                            <th class="column-title">boton 2 es</th>
                                            <th class="column-title">boton 3 en</th>
                                            <th class="column-title">boton 3 es</th>
                                            <th class="column-title">boton 4 en</th>
                                            <th class="column-title">boton 4 es</th>
                                            <th class="column-title">boton 5 en</th>
                                            <th class="column-title">boton 5 es</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($page as $p)
                                        <tr class="even pointer">
                                            <td class=" ">{{$p->id}}</td>
                                            <td class=" ">{{$p->button_1_en}}</td>
                                            <td class=" ">{{$p->button_1_es}}</td>
                                            <td class=" ">{{$p->button_2_en}}</td>
                                            <td class=" ">{{$p->button_2_es}}</td>
                                            <td class=" ">{{$p->button_3_en}}</td>
                                            <td class=" ">{{$p->button_3_es}}</td>
                                            <td class=" ">{{$p->button_4_en}}</td>
                                            <td class=" ">{{$p->button_4_es}}</td>
                                            <td class=" ">{{$p->button_5_en}}</td>
                                            <td class=" ">{{$p->button_5_es}}</td>
                                            <td class=" last">
                                                <a class="btn btn-primary btn-block" href="page/{{ $p->id }}">ir</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/page/{{ $p->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop