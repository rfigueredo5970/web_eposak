@extends('layouts.master-admin')

@section('content')
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Form Design <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/page" class="btn ">back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'page', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="button_1_en">boton 1 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('button_1_en',null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('button_1_en') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="button_1_es">boton 1 es<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('button_1_es',null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('button_1_es') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="button_2_en" class="control-label col-md-3 col-sm-3 col-xs-12">boton 2 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('button_2_en',null,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('button_2_en') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="button_2_es" class="control-label col-md-3 col-sm-3 col-xs-12">boton 2 es<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_2_es',null,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('button_2_es') }}</div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="button_3_en" class="control-label col-md-3 col-sm-3 col-xs-12">boton 3 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_3_en',null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('button_3_en') }}</div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="button_3_es" class="control-label col-md-3 col-sm-3 col-xs-12">boton 3 es<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_3_es',null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('button_3_es') }}</div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="button_4_en" class="control-label col-md-3 col-sm-3 col-xs-12">boton 4 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_4_en',null,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('button_4_en') }}</div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="button_4_es" class="control-label col-md-3 col-sm-3 col-xs-12">boton 4 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_4_es',null,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('button_4_es') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="button_5_en" class="control-label col-md-3 col-sm-3 col-xs-12">boton 5 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_5_en',null,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('button_5_en') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="button_5_es" class="control-label col-md-3 col-sm-3 col-xs-12">boton 5 en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('button_5_es',null,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('button_5_es') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="legal_rights_en" class="control-label col-md-3 col-sm-3 col-xs-12">Bases legales en<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textarea('legal_rights_en',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('legal_rights_en') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="legal_rights_es" class="control-label col-md-3 col-sm-3 col-xs-12">Bases legales es<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textarea('legal_rights_es',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('legal_rights_es') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="logo" >logo<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('logo') }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('logo') }}</div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop