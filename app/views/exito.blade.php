@extends('layouts.master')

  @section('content')
<!--:::::::::INICIO INICIO CASOS DE EXITO:::::::::-->
<section class="comunidad-inter">
   <div class="principalimg" style="background-image:url({{ asset('/images/'.$navbar->image)}})"" >
        <div class="container">
            <div class="row">
                   <div class="col s12">
                        <a href= {{  route('fundacion') }} ><h6><i class="fa fa-chevron-left" aria-hidden="true"></i>REGRESAR</h6></a>
                   </div>
            </div>
            <div class="row">
                   <div class="col s12">
                        <h1>{{$navbar->name}}</h1>
                   </div>
            </div>
        </div>
    </div>
</section>
<section class="exito">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h1>{{$navbar->title_es}}</h1>
                <p>{{$navbar->description_es}}</p>
            </div>
        </div>
        <div class="row">
           <div class="col s12">
               {{-- <h2>ordenar por: <a href=""> <span data-activates='dropdown1' class="dropdown-button">fecha <i class="fa fa-angle-down" aria-hidden="true"></i></span> --}}</a></h2>
                <!-- Dropdown Structure -->
                  <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="#!">cultura educación</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">agricultura</a></li>
                    <li class="divider"></li>                    
                    <li><a href="#!">monto del proyecto</a></li>
                  </ul>
            </div>
        </div>
        <div class="row">
           <!--:::::::/*EMPRENDEDOR*/:::::::-->
           <div class="col s12 m6">
             <div class="ttlemprendedor valign-wrapper">
                 <h1 class="valign">emprendimiento</h1>
             </div>
           @foreach($project_emprender as $p)
              <div class="col s12 padding0">
                   <div class="ficha valign-wrapper">
                    <div class="col s4 valign">
                        <!--
                        -->
                        @if(isset($p->entrepreneurs->image))
                        {{HTML::image('/images/'.$p->entrepreneurs->image,null,array('class' =>'circle responsive-img center-block','height'=>'150','width'=>'150'))}}
                        @else
                          <img src= {{ url("/images/user.png") }}  alt="" class="circle responsive-img center-block" height="150" width="150">
                        @endif
                    </div>
                    <div class="col s8">
                        <h3>{{$p->entrepreneurs->name}} </h3>
                        <p>{{$p->entrepreneurs->description_es}}</p>
                        <h3><a href= {{  route('proyecto_comunitario',$p->id ) }} ><span>ver más</span></a></h3>
                    </div>
                   </div>                
              </div>
              @endforeach
              <!--
              <div class="col s12 padding0">
                   <div class="ficha valign-wrapper">
                    <div class="col s4 valign">
                        <img src= {{ url( "img/emprendedor.jpg" ) }}  alt="" class="circle responsive-img center-block" height="150" width="150">
                    </div>
                    <div class="col s8">
                        <h3>nombre del emprendedor</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quas illum ad, magnam ex.</p>
                        <h3><a href="proyecto-comunitario.html"><span>ver más</span></a></h3>
                    </div>
                   </div>                
              </div> 
              -->
           </div>
           <!--:::::::/*COMUNITARIO*/:::::::-->
           <div class="col s12 m6">
               <div class="ttlemprendedor valign-wrapper">
                  <h1 class="valign">comunitarios</h1>
               </div>
               @foreach($project_comunitario as $p)
               <div class="col s12 padding0">
                   <div class="ficha-2 valign-wrapper">
                    <div class="col s4 padding0 back-comu" style="background-image:url({{ asset('/images/'.$p->multimedias[rand(0,sizeof($p->multimedias)-1)]->name)}})" >
                        <div class="mas-expan valign-wrapper"><i class="fa fa-expand center-block valign" aria-hidden="true"></i></div>
                    </div>
                    <div class="col s8 ">
                       <div class="valign">
                            <h3>{{$p->title_es}}</h3>
                            <p>{{$p->description_es}}</p>
                            <h3><a href= {{  route('proyecto_comunitario',$p->id ) }} ><span>ver más</span></a></h3>
                        </div>
                    </div>
                   </div>                
              </div>
              @endforeach
              <!--
              <div class="col s12 padding0">
                   <div class="ficha-2 valign-wrapper">
                    <div class="col s4 padding0 back-comu">
                        <div class="mas-expan valign-wrapper"><i class="fa fa-expand center-block valign" aria-hidden="true"></i></div>
                    </div>
                    <div class="col s8 ">
                       <div class="valign">
                            <h3>nombre del proyecto</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quas illum ad, magnam ex.</p>
                            <h3><a href="proyecto-comunitario.html"><span>ver más</span></a></h3>
                        </div>
                    </div>
                   </div>                
              </div>
              -->
           </div>  
        </div>
    </div>
</section>
<!--::::::::::/*BOTON CARGAR MAS*/::::::::::-->
<section class="comunidad-inter">
    <div class="center-block" style="width: 30%">
        {{-- <a class="waves-effect waves-light btn">CARGAR MÁS</a>  --}}
    </div>
</section>
<!--::::::::::/*BOTON CARGAR MAS*/::::::::::-->
<!--::::::::FIN INICIO CASOS DE EXITO::::::::-->
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="row feposak-2 valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom->image)}})" >
            <div class="col s12 m8 valign">
                <p>{{$bottom->description_es}}</p>
            </div>
            <div class="col s12 m4 valign">
                <a href= {{ url($bottom->link)}} ><img src= {{ url(  "/maqueta/img/comunidades-png.png" ) }}  alt="" class="responsive-img center-block"></a>
            </div>
        </div>
</section>
<!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
<section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer>
@stop