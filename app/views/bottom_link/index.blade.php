@extends('layouts.master-admin')

@section('content')
            <!-- page content -->
<div class="" role="main">
   	<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel container">
                <div class="x_title">
                    <h2>Botones al final de la pagina<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                       <a class="btn btn-success" href="bottomlink/create">Crear nuevo Boton-Final</a>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content  ">
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">ID </th>
                                <th class="column-title">Seccion de pagina </th>
                                <th class="column-title">Apuntar</th>
                                <th class="column-title"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bottom as $b)
                            <tr class="even pointer">
                                <td class=" ">{{$b->id}}</td>
                                <td class=" ">{{$b->section_navbar->name}}</td>
                                <td class=" ">{{$b->link}}</td>
                                <td class=" last">
                                    <a class="btn btn-info btn-block" href="bottomlink/{{ $b->id }}">Ver</a>
                                    <br>    
                                    <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/bottomlink/{{ $b->id }}/edit">editar</a>
                                </td>
                            </tr>
                            @endforeach    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
