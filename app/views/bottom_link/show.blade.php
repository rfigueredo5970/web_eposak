@extends('layouts.master-admin')

@section('content')
                
    <!-- /top tiles -->
		<div class="x_panel" style="height:600px;">
            <div class="x_title">
                <h2>{{ $bottom->link }}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a href="/esposak/bottomlink" class="btn" ><i>Back</i></a>
                    </li>
                    <!--
                    <li  >
                        {{ Form::open(array('url'=> 'bottomlink/'.$bottom->id, 'method' => 'delete')) }}
                        {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                        {{Form::close()}}
                    </li>
                    -->
                </ul>
                <div class="clearfix"></div>
                <div class="container well text-center " >    
                    <h1>{{ $bottom->link }} </h1><br>
                    <br>
                    <br>
                    <div class="row container-fluid " >
                        <div class="col-md-6" >
                            <p style="word-wrap: break-word;" >{{ $bottom->section_navbar->title_es }}</p>
                            <h6 style="opacity:0.50;">-----------------Español-----------------</h6>
                            <p style="word-wrap: break-word;" >{{ $bottom->description_es }}</p>
                            <h6 style="opacity:0.50;">-----------------Ingles-----------------</h6>
                            <p style="word-wrap: break-word;" >{{ $bottom->description_en }}</p>
                        </div>
                        <div class="text-center col-md-4 row" >
                            <div class="col-md-3  col-md-offset-6">
                                <span >Imagen</span>
                                {{ HTML::image('/images/'.$bottom->image,null,array('style' =>'height: 150px'))}}
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
@stop
                