@extends('layouts.master-admin')

@section('content')
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Form Design <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/bottomlink" class="btn">Back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'bottomlink','route'=>'post', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Link<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('link',Input::old('link'),array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('link') }}</div>
                                                
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion español<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textArea('description_es',Input::old('description_es'),array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                                                
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion ingles<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_en',Input::old('description_en '),array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                                               
                                            </div>
                                        </div> 
                                        
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seccion de navedor <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              {{ Form::select('section_navbar_id',$navbar,null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('section_navbar_id') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">imagen</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div class="error">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
@stop
           