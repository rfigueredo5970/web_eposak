@extends('layouts.master-admin')

@section('content')
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Form Design <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/contacto_form" class="btn">Back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'contacto_form','route'=>'post', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Titulo de seccion<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('titulo',null,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('titulo') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Icono de titulo<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('icono',null,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('icono') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre del primer input<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('input1',null,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('input1') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre del Segundo input<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('input2',null,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('input2') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion de seccion<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textarea('descripcion',null,array('class' => 'form-control','style' => 'resize:none; width: 300px; height: 150px')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('descripcion') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre de campo de mensaje<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('textarea',null,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('textarea') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre para el boton de enviar<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('btn',null,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('btn') }}</div>
                                            </div>
                                        </div>

                                        <!--
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">imagen</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('') }}<br/>
                                               <div class="error">{{ $errors->first('') }}</div>
                                            </div>
                                        </div>
                                        -->
    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop