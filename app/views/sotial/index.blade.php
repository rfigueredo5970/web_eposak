@extends('layouts.master-admin')

@section('content')
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Redes sociales Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="sotialred/create">Agregar nueva Red social </a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Nombre (Red social)</th>
                                            <th class="column-title">Correo Electronico</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sotial as $s)
                                        <tr class="even pointer">
                                            <td class=" ">{{$s->id}}</td>
                                            <td class=" ">{{$s->name}}</td>
                                            <td class=" ">{{$s->email}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="sotialred/{{ $s->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/sotialred/{{ $s->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop