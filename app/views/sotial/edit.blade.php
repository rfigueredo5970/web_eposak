@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Editar Red Social<small>Formulario para editar Red social</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/sotialred" class="btn" >back</a> 
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                   {{Form::open(array('url' => 'sotialred/' . $sotial->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name',$sotial->name,array('class' => 'form-control'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                <div style="color:red;" class="error">{{ $errors->first('name') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Correo Electronico<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('email',$sotial->email,array('class' => 'form-control'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                <div style="color:red;" class="error">{{ $errors->first('email') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">icono</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('icon') }}<br/><ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                               <div style="color:red;" class="error">{{ $errors->first('icon ') }}</div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class'=>'btn btn-block btn-success'))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop