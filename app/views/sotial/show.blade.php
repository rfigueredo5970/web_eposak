@extends('layouts.master-admin')

@section('content')
                    <div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>Red social: ' {{ $sotial->name }} '</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/sotialred" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{Form::open(array('url'=> 'sotialred/'.$sotial->id , 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}    
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Correo Electronico-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $sotial->email }}</p>
                                    </div>
                                    <div class="text-center col-md-6" >
                                        {{ HTML::image('/images/'.$sotial->icon,null,array('style' =>'height: 200px'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop