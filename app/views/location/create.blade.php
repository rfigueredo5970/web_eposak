@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Redes sociales<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a href="/esposak/location" class="btn">Back</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <br>
                        {{Form::open(array('url' => 'location','route'=>'post', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Direccion<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('address',null,array('class' => 'form-control')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('address') }}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Cordenada longitud<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('lng',null,array('class' => 'form-control')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('lng') }}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Cordenada latitud<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('lat',null,array('class' => 'form-control')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('lat') }}</div>
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    
                                    {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                </div>
                            </div>

                        {{Form::close() }}
                    </div>
                </div>
                @stop