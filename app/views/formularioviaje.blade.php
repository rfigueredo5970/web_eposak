@extends('layouts.master')

  @section('content')
<!--:::::::::::INICIO FORMULARIO VIAJE:::::::::::-->
  <section class="formviaje">
      <div class="container-fluid">
          <div class="row">
              <img src="/esposak/maqueta/img/formviaje.jpg" alt="" class="responsive-img">
          </div>
      </div>
      <div class="formcontacto">
          <div class="container">
              <div class="row">
                  <div class="col s12">
                      <h1>{{$viaje->title_es}}</h1>
                      <p>envíanos tus datos y  el equipo Eposak te estará contactando para organizar tu viaje.</p>
                  </div>
              </div>
              <div class="row">
                {{Form::open(array('url' => 'passenger','route'=>'post'))}}
                  <div class="col s12 m6">
                   <div class="row">
                   <input type="hidden" name="travels_id" value="{{$viaje->id}}" >
                      <div class="input-field-vi col s12">
                        <input id="first_name" name="name" type="text" class="validate">
                        <label class="label-epo" for="first_name">Nombre</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field-vi col s12">
                        <input id="last_name" name="last_name" type="text" class="validate">
                        <label class="label-epo" for="last_name">Apellido</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field-vi col s12">
                        <input id="email" type="email" name="email" class="validate">
                        <label class="label-epo" for="email">Email</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field-vi col s12">
                        <input id="icon_telephone" name="phone_number" type="tel" class="validate">
                        <label class="label-epo" for="icon_telephone">Teléfono</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        {{ Form::submit('ENVIAR', ['class' => 'btn waves-effect waves-light btnenviar','onclick' => 'Materialize.toast("Su mensaje fue enviado con éxito", 4000)' ]) }}
                        <!--
                         <a class="btn waves-effect waves-light btnenviar" onclick="Materialize.toast('Su mensaje fue enviado con éxito', 4000)"><i class="fa fa-check-circle-o" aria-hidden="true"></i> ENVIAR</a>
                         -->
                      </div>
                  </div>
                  </div>
                {{Form::close() }}
              </div>
          </div>
      </div>
  </section>
<!--:::::::::::::FIN FORMULARIO VIAJE:::::::::::::-->
   
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="row feposak valign-wrapper">
            <div class="col s12 m8 valign">
                <p>Conoce  la fundación eposak, los proyectos que <br> tenemos y cómo puedes colaborar en la comunidad</p>
            </div>
            <div class="col s12 m4 valign">
                <a href="fundacion.html"><img src="img/comunidad4.png" alt="" class="responsive-img center-block"></a>
            </div>
        </div>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer> 
@stop