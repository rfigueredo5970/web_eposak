@extends('layouts.master')

  @section('content')
  <input   id="long-map" type="hidden" name="" value="{{ $destiny->lng}}">
  <input   id="lat-map" type="hidden" name="" value="{{ $destiny->lat}}">
    <!--::::::::::INICIO COMUNIDAD KAMARATA::::::::::-->
    <section class="comunidad-inter">
       <div class="principalimg" style="background-image:url({{ asset('/images/'.$destiny->image)}})">
            <div class="container">
                <div class="row">
                       <div class="col s12">
                            <a href= {{  URL::previous() }} ><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                       </div>
                </div>
                <div class="row">
                       <div class="col s12">
                            <h1>{{$destiny->name_es}}</h1>
                       </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col s12 m6 padding0">
                  <div style="height:400px" class="col-md-12">
                      <div id="map" style="width: 100%; height: 100%;"></div>
                  </div>
                </div>
                <div class="col s12 m6 padding80">
                    <p>{{$destiny->description_es}}</p>          
                </div>
            </div>
        </div>
        <!--::::::::::INICIO VIAJE EXPERENCIAL::::::::::-->
        <div class="container">
            <div class="row">
                <h2>{{$travel_type[0]->name_es}}
                  {{--<span>ordenar por <a href="" data-activates='dropdown1' class="dropdown-button ">fecha de salida <i class="fa fa-chevron-down" aria-hidden="true"></i>
    </a></span> --}}
                </h2>
           <!-- Dropdown Structure -->
              <ul id='dropdown1' class='dropdown-content drop-rojo'>
              <li><a href="#!">cultura educación</a></li>
              <li class="divider"></li>
              <li><a href="#!">agricultura</a></li>
              <li class="divider"></li>                    
              <li><a href="#!">monto del proyecto</a></li>
            </ul>
            </div>
          <div id="container-experiencial-travel">
            @foreach($travel_exp as $t_e )
              <div class="row">
                  <a href= {{  route('viaje_individual',$t_e->slug ) }} ><div class="col s12 padding0 viaex" style="background-image:url({{ asset('/images/'.$t_e->multimedias[rand(0,sizeof($t_e->multimedias)-1)]->name)}})">
                      <h1>{{$t_e->title_es}}<br></h1>
                      <h2><i class="fa fa-map-marker" aria-hidden="true"></i> {{$t_e->destiny->name_es}}</h2>
                      <div class="verdeprecio">
                          <p>precio bs.{{$t_e->prices}}</p>
                      </div>
                      <div class="carnedia">
                          <p>{{$t_e->days  }} días</p>
                      </div>
                      <img src= {{ url("/maqueta/img/fondonegro.png") }} alt="" class="">
                  </div></a>
              </div>
            @endforeach
          </div>
            <div class="center-block" style="width: 30%">
              <a class="waves-effect waves-light btn loadMoreDestiny" id="experiencial-travel" data-page="2" data-type="{{$travel_type[0]->id}}" data-destiny="{{$destiny->id}}">CARGAR MÁS</a>
            </div> 
        </div>
    </section>
    <section class="comunidad-inter fondo-viaje">
         <!--:::::::::::INICIO VIAJES DE VOLUNTARIADO:::::::::::-->
        <div class="container">
            <div class="row">
                <h2>{{$travel_type[1]->name_es}}
                 {{--<span>ordenar por <a href="" data-activates='dropdown1' class="dropdown-button ">fecha de salida <i class="fa fa-chevron-down" aria-hidden="true"></i>
    </a></span> --}}
    </h2>
           <!-- Dropdown Structure -->
                      <ul id='dropdown1' class='dropdown-content drop-rojo'>
                        <li><a href="#!">cultura educación</a></li>
                        <li class="divider"></li>
                        <li><a href="#!">agricultura</a></li>
                        <li class="divider"></li>                    
                        <li><a href="#!">monto del proyecto</a></li>
                      </ul>
            </div>
            <div id="container-voluntary-travel">
              @foreach($travel_vol as $t_v )
              <div class="row">
                  <a href= {{ route('viaje_individual',$t_v->slug ) }} ><div class="col s12 padding0 viaex" style="background-image:url({{ asset('/images/'.$t_v->multimedias[rand(0,sizeof($t_v->multimedias)-1)]->name)}})">
                      <h1>{{$t_v->title_es}}<br></h1>
                      <h2><i class="fa fa-map-marker" aria-hidden="true"></i> {{$t_v->destiny->name_es}}</h2>
                      <div class="verdeprecio">
                          <p>precio bs.{{$t_v->prices}}</p>
                      </div>
                      <div class="carnedia">
                          <p>{{$t_v->days  }} días</p>
                      </div>
                      <img src= {{ url("/maqueta/img/fondonegro.png") }} alt="" class="">
                  </div></a>
              </div>
              @endforeach
            </div>
                   <div class="center-block" style="width: 30%">
                    <a class="waves-effect waves-light btn loadMoreDestiny" id="voluntary-travel" data-page="2" data-type="{{$travel_type[1]->id}}" data-destiny="{{$destiny->id}}">CARGAR MÁS</a>
                   </div>                
        </div>
    </section>
    <!--::::::::::FIN COMUNIDAD KAMARATA::::::::::-->
    <!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
    <section class="comunidad">
            <div class="row feposak valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom->image)}})">
                <div class="col s12 m8 valign">
                    <p>{{$bottom->description_es}}</p>
                </div>
                <div class="col s12 m4 valign">
                    <a href= {{ url($bottom->link)}} ><img src= {{ url("/maqueta/img/comunidad4.png") }} alt="" class="responsive-img center-block"></a>
                </div>
            </div>
    </section>
    <!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
    <section class="btn-subir">
        <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
        <p>SUBIR</p></a>
    </section>
    <!--:::::::::::::FOOTER:::::::::::::-->
    <footer>
        <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
    </footer>
  @stop  