@extends('layouts.master')

  @section('content')



<!--::::::::::INICIO INICIO VIAJES EXRENCIALES::::::::::-->
  <section class="comunidad-inter">
     <div class="viajexperencial" style="background-image:url({{ asset('/images/'.$section->image)}})">
      <div class="container">
        <div class="row">
          <div class="col s12">
            <a href= {{  URL::previous() }} ><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <h1>{{$section->name}}</h1>
          </div>
        </div>
      </div>
    </div>
    <!--::::::::::INICIO VIAJE EXPERENCIAL::::::::::-->
    <div class="container">
      
      <!-- Dropdown Trigger -->
   

      <div class="row">
        
        <h2>{{$travel_type->name_es}}</h2>
        
        <div class="dropdown dropdown-style" style="float:right; width:400px!important;"> 
        
        <h2> <span style="float:left;">ordenar por </span></h2> <a id="dropdownMenu1" data-target="#" href="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class=" ">  <span style="float:left !important;  text-transform: uppercase; font-family:montserratregular; font-size: 18px; color:#DA604C; padding-left:10px; vertical-align:bottom!important;">
           
         {{ $order }}  <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
        </a> 
        
        
        
        
       <!-- Dropdown Structure -->
        <ul class="dropdown-menu dropdown-menu-style2 dropdown-menu-right drop-rojo"  aria-labelledby="dropdownMenu1">
          <li style="width:250px;  height:50px; font-size:16px;     font-family: montserrat!important; text-transform: uppercase !important;"><a  href={{ route('viajes_order', array( $slug ,"start_at") ) }} style="display:block; line-height: 22px; padding: 14px 16px; ">Fecha de salida</a></li>
          <li  class="divider"></li>
          <li style="width:250px;  height:50px; font-size:16px;     font-family: montserrat!important; text-transform: uppercase !important;"><a  href={{ route('viajes_order', array( $slug ,"end_at") ) }} style="display:block; line-height: 22px; padding: 14px 16px; ">Fecha de regreso</a></li>
          <li  class="divider"></li>
          <li style="width:250px;  height:50px; font-size:16px;     font-family: montserrat!important; text-transform: uppercase !important;"><a  href={{ route('viajes_order',array( $slug ,"title_es") ) }} style="display:block; line-height: 22px; padding: 14px 16px; ">Nombre del proyecto </a></li>
          <li  class="divider"></li> 
          <li style="width:250px;  height:50px; font-size:16px;     font-family: montserrat!important; text-transform: uppercase !important;"><a  href={{ route('viajes_order', array( $slug ,"prices" ) ) }} style="display:block; line-height: 22px; padding: 14px 16px; ">Monto del proyecto</a></li>
        </ul>
      </div>
        
      </div>
      
      <div id="container-travels">
      @foreach($travel as $t)
        <div class="row">
          <a  href= {{ route('viaje_individual',$t->slug ) }} >
          <div class="col s12 padding0 viaex" style="background-image:url({{ asset('/images/'.$t->multimedias[rand(0,sizeof($t->multimedias)-1)]->name)}}) ">
            <h1>{{$t->title_es}}<br></h1>
            <h2><i class="fa fa-map-marker" aria-hidden="true"></i>{{$t->destiny->name_es}}</h2>
            <div class="verdeprecio">
              <p>PRECIO Bs {{$t->prices}}</p>
            </div>
            <div class="carnedia">
              <p>{{$t->days}} DÌAS </p>
            </div>
              <img src= {{ url("/maqueta/img/fondonegro.png") }} alt="" class="">
          </div>
          </a>    
        </div>
      @endforeach
      </div>
    
      <div class="center-block before" style="width: 30%">
        <a class="waves-effect waves-light btn expand loadMore" id="loadMoreButton" style="text-decoration=none;" data-travel="{{$travel_type->id}}" data-page="1" href="">CARGAR MÁS</a>
        <div id="final"></div>  
      </div> 
    </div>
  </section>
  <section class="comunidad-inter fondogris">
  </section>
<!--::::::::::FIN COMUNIDAD KAMARATA::::::::::-->
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
  <section class="comunidad">
    <div class="row feposak valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom->image)}})">
        <div class="col s12 m8 valign">
          <p>{{$bottom->description_es}}</p>
        </div>
        <div class="col s12 m4 valign">
            <a href {{  route( $bottom->link ) }} ><img src= {{ url("/maqueta/img/comunidad4.png") }} alt="" class="responsive-img center-block"></a>
        </div>
      </div>




<!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
  <section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
  </section>
<!--:::::::::::::FOOTER:::::::::::::-->
  <footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
  </footer>


@stop
