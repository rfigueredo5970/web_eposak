@extends('layouts.master-admin')

@section('content')
  <div class="" role="main">
    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2> Viajes Experienciales <small> listado de Proyectos</small></h2>
            <div class="clearfix"></div>	
          </div>
          @foreach($t_ex[0] as $p)
            <div class="col-md-55">
              <div class="thumbnail">
                <div class="image view view-first">
                  @if (sizeof($p->multimedias) > 0)
                    {{ HTML::Image('/images/'.$p->multimedias[0]->name, null ,array('style' => 'width: 100% ;display: block')) }}
                  @endif
                  <div class="mask no-caption">
                    <div class="tools tools-bottom">
                      <a href="/travel/{{ $p->id }}/edit"><i class="fa fa-pencil"></i></a>
                		  <a href="travel/{{ $p->id }}">Ver</a>
                    </div>
                  </div>
                </div>
                <div class="caption">	
                  <p>
                    <strong>{{ $p->title_es }}</strong>
                  </p>
                  <p>{{ $p->lugar}}</p>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2> Viajes de Voluntariados <small> listado de viajes voluntariados</small></h2>
                <div class="clearfix"></div>	
            </div>
            @foreach($t_vo[0] as $p)
              <div class="col-md-55">
                <div class="thumbnail">
                  <div class="image view view-first">
                    @if (sizeof($p->multimedias) > 0)
                        {{ HTML::Image('/images/'.$p->multimedias[0]->name, null ,array('style' => 'width: 100% ;display: block')) }}
                    @endif
                    <div class="mask no-caption">
                      <div class="tools tools-bottom">
                        <a href="/travel/{{ $p->id }}/edit"><i class="fa fa-pencil"></i></a>
                  		  <a href="travel/{{ $p->id }}">Ver</a>
                      </div>
                    </div>
                  </div>
                  <div class="caption">	
                    <p><strong>{{ $p->title_es }}</strong>
                    </p>
                    <p>{{ $p->lugar}}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h2> Destinos <small> listado de destinos</small></h2>
              <div class="clearfix"></div>	
            </div>
            @foreach($destiny as $p)
              <div class="col-md-55">
                <div class="thumbnail">
                  <div class="image view view-first">
                    {{ HTML::Image('/images/'.$p->image, null ,array('style' => 'width: 100% ;display: block')) }}
                    <div class="mask no-caption">
                      <div class="tools tools-bottom">
                        <a href="/destiny/{{ $p->id }}/edit"><i class="fa fa-pencil"></i></a>
                        <a href="destiny/{{ $p->id }}">Ver</a>
                      </div>
                    </div>
                  </div>
                  <div class="caption">	
                    <p><strong>{{ $p->title_es }}</strong>
                    </p>
                    <p>{{ $p->lugar}}</p>
                  </div>
                </div>
              </div>
		        @endforeach
          </div>
        </div>
      </div>

    </div>
@stop