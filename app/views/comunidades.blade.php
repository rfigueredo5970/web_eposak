@extends('layouts.master')

  @section('content')
  <!--:::::::::::::INICIO COMUNIDAD:::::::::::::-->
    <div class="comunidad margin-topc">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h1>{{$navbar->name}}</h1>
                    <p>{{$navbar->description_es}}</p>
                    <a href="{{$navbar->url_section}}"><h6><i class="fa fa-play-circle-o" aria-hidden="true"></i> VER VIDEO</h6></a>
                </div>
            </div>
        </div>
        <div class="">
            <div class="row padding0 ver-todo">
              @foreach($destiny as $d)
                <div class="col s12 l4 padding0">
                  @if($d->id == 3)
                    <div class="kamaratafondo-2" style="background-image:url({{ asset('/images/'.$imagen_destiny_travel_kamarata)}})">
                  @else
                    <div class="kamaratafondo-2" style="background-image:url({{ asset('/images/'.$imagen_destiny_travel_birongo)}})">
                  @endif
                        <h3 style="text-transform: uppercase;" >{{$d->name_es}}</h3>
                        <div class="rosac">
                            <a href= {{ route('destino', $d->slug ) }} ><p>VER MÁS</p></a>
                        </div>
                    </div>    
                </div>
                @endforeach
                <div class="col s12 l4 padding0">
                 @foreach($travel_type as $t_t)
                 <div class="valign-wrapper">
                     <a href= {{ route('viajes', $t_t->slug ) }} ><h2 class="valign">Ver todos los {{$t_t->name_es}} <i class="fa fa-chevron-right" aria-hidden="true"></i></h2></a>
                 </div>
                 @endforeach
                </div>
            </div>
            <div class="feposak valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom->image)}})">
               <div class="row">
                    <div class="col s12 m8 valign">
                        <p class="valign">{{$bottom->description_es}}</p>
                    </div>
                    <div class="col s12 m4 valign">
                        <a href= {{ route($bottom->link) }} ><img src= {{ url("maqueta/img/comunidad4.png")}}  alt="" class="responsive-img center-block"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--:::::::INICIO BOTON SUBIR:::::::-->
    <div class="btn-subir">
        <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
        <p>SUBIR</p></a>
    </div>
    <!--:::::::::::::FOOTER:::::::::::::-->
    <footer>
        <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
    </footer>
  @stop