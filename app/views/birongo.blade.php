<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birongo - Eposak</title>
    <!--Import Google Icon Font-->
      {{ HTML::style('http://fonts.googleapis.com/icon?family=Material+Icons'); }}
      <!--Import materialize.css-->
       {{ HTML::style('maqueta/css/materialize.css'); }}

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      
       {{ HTML::style('maqueta/css/style.css'); }}
       {{ HTML::style('maqueta/css/font-awesome.css'); }}

      <!--favicon-->
<body>
<!--:::::::::::::INICIO BARRA NAVEGACION:::::::::::::-->
  <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
          @endif
          @foreach($page as $p)
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <div class="container-fluid">
              <div class="row">
                <div class="col s12 m2 hide-on-med-only hide-on-small-only">
                  <a href="home" class="brand-logo">{{ HTML::image('/images/'.$p->logo,null,array('class' => 'center-block')); }}</a>
                </div>
                <div class="col s12 m2 hide-on-large-only ">
                  <a href="home" class="brand-logo ">{{ HTML::image('/images/'.$p->logo,null,array('class' => 'center-block')); }} </a>
                </div>
                <div class="col s7 m8">
                  <ul class="right hide-on-med-and-down">
                    <li><a href="eposak" class="verdeoscuro">{{$p->button_1_es}} </a></li>
                    <li><a href="comunidades" class="verdeclaro">{{$p->button_2_es}} </a></li>
                    <li><a href="fundacion" class="carne">{{$p->button_3_es}}</a></li>
                    <li><a href="contacto" class="carne2">{{$p->button_4_es}}</a></li>
                    @if(Auth::user())
                      <li><a href="#" class="naranja opcionmmenu regisuser">{{$p->button_6_es}}</a></li>
                    @else
                      <li><a href="#" class="naranja opcionmmenu regisuser">{{$p->button_5_es}}</a></li>
                    @endif
                      <li class="buscajs cerrarmegamenu"><a href="#" class="rojo"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                  </ul>
                </div>

                <!-- ESTO ESTA DANDO ERROR -->
                <div class="col s3 m2 redesnav">
                  <ul class="left hide-on-med-and-down" style="margin-top: -12px;">
                    @foreach($principal_netting as $p_n )
                      <li><a href="{{$p_n->cuenta}}"><i class="{{$p_n->icon}}" aria-hidden="true"></i></a></li>
                    @endforeach
                  </ul>
                </div>

                   <!-- ESTO ESTA DANDO ERROR -->

                <div class="idiomanav">
                  <ul class="hide-on-med-and-down">
                    <li>
                      <p>
                        {{-- <a href="" class="idiomafuera">EN</a> --}}
                        <span style="color=""> / </span>
                        <a href="" class="idiomadentro"> ES</a>
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!--/*::::::::::::::::PARTE MOVIL::::::::::::::::*/-->
            <ul class="side-nav" id="mobile-demo">
              <li><a href="eposak">{{$p->button_1_es}}</a></li>
              <li><a href="comunidades">{{$p->button_2_es}}</a></li>
              <li><a href="fundacion">{{$p->button_3_es}}</a></li>
              <li><a href="contacto">{{$p->button_4_es}}</a></li>
              <li><a href="#" class="opcionmmenu">{{$p->button_5_es}}</a></li>
              <li class="buscajs"><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
              <li><p><a href="">EN</a><a href="">ES</a></p></li>
            </ul>
          @endforeach
        </div>
      </nav>
    </div>
    <!--fin barra de navegacion-->
<!--:::::::::::::FIN BARRA NAVEGACION:::::::::::::-->
 <!--/*:::::::::::::INICIO REGISTRO:::::::::::::*/-->
<section class="registro" id="megamenu">
    <div class="container">
        <div class="row">
            <div class="col s12 l6">
                   <h1>registro</h1>
               <div class="col s12 l6 padding0">
                   <form>
                     <div class="row">
                        <div class="input-field-5">
                          <input id="first_name" type="text" class="validate" tabindex="1">
                          <label for="first_name" class="label-epo">Usuario:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="email" class="validate" tabindex="2">
                          <label for="email" class="label-epo">Email:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="password" class="validate" tabindex="4">
                          <label for="email" class="label-epo">Contraseña:</label>
                        </div>
                      </div>
                    </form>
               </div>
               <div class="col s12 l6 padding0">
                   <form>
                      <div class="row">
                        <div class="input-field-5" style="margin-top: 36px;">
                          <input id="email" type="email" class="validate" tabindex="3">
                          <label for="email" class="label-epo">Confirmar:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="password" class="validate" tabindex="5">
                          <label for="email" class="label-epo">Confirmar:</label>
                        </div>
                      </div>
                    </form>
                     <p>o registrate con</p>
                    <div class="row">
                       <ul>
                           <a href=""><li><i class="fa fa-facebook-square" aria-hidden="true"></i></li></a>
                           <a href=""><li><i class="fa fa-twitter-square" aria-hidden="true"></i></li></a>
                       </ul>
                        <a class="btn waves-effect waves-light btnenviar cerrarmegamenu" style="float: right;">REGISTRO</a>
                    </div>
               </div>              
            </div>
            <div class="col s12 offset-l1 l4">
                  <h1>inicio de sesión</h1>
                   <form>
                     <div class="row">
                        <div class="input-field-5">
                          <input id="first_name" type="text" class="validate" tabindex="6">
                          <label for="first_name" class="label-epo">Usuario:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="password" class="validate" tabindex="7">
                          <label for="email" class="label-epo">Contraseña:</label>
                        </div>
                      </div>
                    </form>
                    <div class="row">
                       <ul>
                           <li><a href=""><p>¿Olvido su contraseña?</p></a></li>
                           <li>
                               <p>
                           <input class="with-gap" name="group1" type="radio" id="test1" tabindex="8">
                           <label for="test1">Recordarme</label>
                        </p>
                           </li>
                       </ul>
                    </div>
                    <div class="row">
                        <a class="btn waves-effect waves-light btnenviar cerrarmegamenu" style="float: right;">INICIAR</a>
                    </div>
            </div>
        </div>
    </div>
</section>
    
<!--/*:::::::::::::FIN REGISTRO:::::::::::::*/--> 
<!--:::::::::::::::INICIO BUSCA:::::::::::::::-->
<section class="buscar" id="megabuscar">
    <div class="container">
        <div class="col s12">
            <div class="col-1">
                <input type="text" placeholder="BUSCAR" id="buscar">
            </div>
            <div class="col-2 valign-wrapper">
                <a href="#" for="buscar" class="center-block cerrarbuscar"><i class="fa fa-search valign" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
<!--:::::::::::::::::FIN BUSCA::::::::::::::::-->
<!--:::::::::::::::INICIO RESULTADO:::::::::::::::-->
<section class="resultado" style="display:none">
    <div class="container">
       <div class="row">
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
            <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
            <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
            <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
       </div>
       <center>
           <ul class="pagination">
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">2</a></li>
                <li class="waves-effect"><a href="#!">3</a></li>
                <li class="waves-effect"><a href="#!">4</a></li>
                <li class="waves-effect"><a href="#!">5</a></li>
                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </ul>
       </center>        
    </div>
</section>
<!--:::::::::::::::FIN RESULTADO:::::::::::::::-->
<!--::::::::::INICIO COMUNIDAD KAMARATA::::::::::-->
<section class="comunidad-inter">
   <div class="birongo-iner">
        <div class="container">
            <div class="row">
                   <div class="col s12">
                        <a href="/comunidades"><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                   </div>
            </div>
            <div class="row">
                   <div class="col s12">
                        <h1>birongo</h1>
                   </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col s12 m6 padding0">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125542.97276646776!2d-66.32004104092813!3d10.483481694216955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c2ba1f7484889cd%3A0x74e0348213dc2bf2!2sBirongo%2C+Miranda!5e0!3m2!1ses!2sve!4v1468442922982" width="100%" height="563" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<img src= {{url("img/mapab.jpg")}} alt="" class="responsive-img">-->
            </div>
            <div class="col s12 m6 padding80">
                <p>La música de sus tambores te guiará hasta el pueblo, hará que tu corazón palpite a otro ritmo y, sin darte cuenta, hará que tus pies se comiencen a mover solos para bailar con plena alegría bajo el brillante sol que ilumina sus calles, su gente y sus ríos color esmeralda en donde puedes refrescarte y limpiar todos tus problemas.</p>
                <p>El río es esencial para la cultura bironguera: sus aguas alimentan las plantaciones de su famoso cacao y a sus orillas se cocinan los mejores sancochos del país, se practican los mágicos baños de flores y se hace sonar el sonido único del tambor de agua.</p>
                <p>Para los que les gusta la aventura dentro de la zona de Birongo hay muchas cuevas para visitar, entre ellas la famosa cueva Alfredo Jahn, que con su imponente entrada te invita conocer las insólitas formaciones rocosas que guarda y toda la tradición africana que oculta.</p>
                <p>El nombre Birongo significa “medicina tradicional”, así que te aseguramos que si te animas a pasar unos días por este mágico lugar te curarás de todos tus males. La calidez de su gente, el exquisito chocolate que fabrican y la belleza de sus paisajes te invitarán a volver una y otra vez.</p>                
            </div>
        </div>
    </div>
    <!--::::::::::INICIO VIAJE EXPERENCIAL::::::::::-->
    <div class="container">
        <div class="row">
            <h2>viajes experencial <span>ordenar por <a href="" data-activates='dropdown1' class="dropdown-button ">fecha de salida <i class="fa fa-chevron-down" aria-hidden="true"></i>
</a></span></h2>
       <!-- Dropdown Structure -->
                  <ul id='dropdown1' class='dropdown-content drop-rojo'>
                    <li><a href="#!">cultura educación</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">agricultura</a></li>
                    <li class="divider"></li>                    
                    <li><a href="#!">monto del proyecto</a></li>
                  </ul>
        </div>
        <div class="row">
            <a href="viajeindividual.html"><div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src= {{ url("img/fondonegro.png") }} alt="" class="">
            </div></a>
        </div>
        <div class="row">
            <a href="viajeindividual.html"><div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src= {{ url("img/fondonegro.png") }} alt="" class="">
            </div></a>
        </div>
        <div class="row">
            <a href="viajeindividual.html"><div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img {{ url("img/fondonegro.png") }} alt="" class="">
            </div></a>
        </div>
               <div class="center-block" style="width: 30%">
                  {{-- <a class="waves-effect waves-light btn">CARGAR MÁS</a>  --}}
               </div> 
    </div>
</section>
<section class="comunidad-inter fondo-viaje">
     <!--:::::::::::INICIO VIAJES DE VOLUNTARIADO:::::::::::-->
    <div class="container">
        <div class="row">
            <h2>viajes voluntariado <span>ordenar por <a href="" data-activates='dropdown1' class="dropdown-button ">fecha de salida <i class="fa fa-chevron-down" aria-hidden="true"></i>
</a></span></h2>
       <!-- Dropdown Structure -->
                  <ul id='dropdown1' class='dropdown-content drop-rojo'>
                    <li><a href="#!">cultura educación</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">agricultura</a></li>
                    <li class="divider"></li>                    
                    <li><a href="#!">monto del proyecto</a></li>
                  </ul>
        </div>
        <div class="row">
            <a href="viajeindividual.html"><div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src= {{ url("img/fondonegro.png") }} alt="" class="responsive-img">
            </div></a>
        </div>
        <div class="row">
            <a href="viajeindividual.html"><div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src= {{ url("img/fondonegro.png") }} alt="" class="responsive-img">
            </div></a>
        </div>
        <div class="row">
            <a href="viajeindividual.html"><div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src= {{ url("img/fondonegro.png") }} alt="" class="responsive-img">
            </div></a>
        </div>
               <div class="center-block" style="width: 30%">
                  {{-- <a class="waves-effect waves-light btn">CARGAR MÁS</a>  --}}
               </div>                
    </div>
</section>
<!--::::::::::FIN COMUNIDAD KAMARATA::::::::::-->
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="feposak valign-wrapper">
           <div class="row">
               <div class="col s12 m8 valign">
                    <p>Conoce  la fundación eposak, los proyectos que <br> tenemos y cómo puedes colaborar en la comunidad</p>
                </div>
                <div class="col s12 m4 valign">
                    <a href="fundacion.html"><img src= {{ url("img/fondonegro.png") }} alt="" class="responsive-img center-block"></a>
                </div>
           </div>
            
        </div>
</section>
<!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
<section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer> 
   <!--Import jQuery before materialize.js-->
      {{ HTML::Script('https://code.jquery.com/jquery-2.1.1.min.js'); }}
      {{ HTML::Script('maqueta/js/materialize.js'); }}
      {{ HTML::Script('maqueta/js/slider.js'); }}
      {{ HTML::Script('maqueta/js/menu.js'); }}
      {{ HTML::Script('maqueta/js/parallax.js'); }}
      {{ HTML::Script('maqueta/js/carousel.js'); }}
      {{ HTML::Script('maqueta/js/subir.js'); }}
      {{ HTML::Script('maqueta/js/megamenu.js'); }}
</body>
</html>