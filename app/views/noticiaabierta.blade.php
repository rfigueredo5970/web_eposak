@extends('layouts.master')

  @section('content')
    <!--:::::::::::::INICIO NOTICIA ABIERTA:::::::::::::-->
    <div class="noticiabierta">
        <div class="container">
            <div class="row">
                <div class="col s12 m5">
                    <a href="/esposak/"><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                </div>
                <div class="col m7 hide-on-small-only fondogrisnoticia"></div>
            </div>
            <div class="row">
                <div class="col m5 hide-on-small-only padding0">
                    <!--ES LA MISMA IMAGEN SOLO SE MOSTRARA EN MEDIO Y LARGO-->
                    {{HTML::image('/images/'.$today->image,null,array('class' =>'responsive-img'))}}
                </div>
                <div class="col s12 m7 fondogrisnoticia-2">
                    <h6>{{$today->created_at}}</h6>
                    <h1>{{$today->title_es}}</h1>
                    <!--ES LA MISMA IMAGEN ESTA SE MONSTRARA SOLO EN MOVIL-->
                    {{HTML::image('/images/'.$today->image,null,array('class' =>'responsive-img hide-on-med-and-up'))}}

                    <p>{{$today->description_es}}</p>
                </div>
            </div>
        </div>    
    </div>
    <!--:::::::::::::FOOTER:::::::::::::-->
    <footer>
        <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
    </footer>
  @stop