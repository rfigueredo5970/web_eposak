@extends('layouts.master')

@section('content')
  <div class="comunidad-inter">
    <div class="principalimg" style="background-image:url({{ asset('/images/'.$navbar->image)}})">
      <div class="container">
        <div class="row">
          <div class="col s12">
            <a href= {{ URL::previous() }} ><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <h1>{{$navbar->title_es}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="voluntariado">
    <div class="container">
      <div class="row">
        <h1>{{$general[0]->title_es}}</h1>
        <p>{{$general[0]->description_es}}</p>
      </div>
    </div>
    @foreach($voluntary as $v )
    <div class="fndgris">
      <div class="container">
        <div class="row">
          <h1>{{$v->name}}<a href= {{ route('contacto') }}><span>contáctanos para aplicar</span></a></h1>
          <p>{{$v->message}} </p>
          <div class="masymenos valign-wrapper" id="{{$v->id}}">
            <p class="valign center-block">
              <strong class="s{{$v->id}}">+</strong>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="container fnblanco" id="f{{$v->id}}" style="display:none">
      <div class="row">
        <p>{{$v->description_es;}}</p>
      </div>
    </div>
    @endforeach
    <!-- ********************************************************************* -->
<!--     <div class="fndgris">
        <div class="container">
            <div class="row">
                <h1>voluntariado 1 <a href=""><span>contáctanos para aplicar</span></a></h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate dolore quod aperiam commodi doloribus eveniet corporis, iusto! Illum ipsam, dolor cum suscipit veritatis ad accusamus inventore ipsa autem sapiente. Illum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia recusandae, ad ex non dolores aut id! Accusamus nisi laudantium natus eum. Nesciunt ullam, provident dolores? Placeat minima neque dolorem, ea!</p>
                <div class="masymenos valign-wrapper" id="2"><p class="valign center-block">
                    <strong class="s2">+</strong>
                </p></div>
            </div>
        </div>
    </div> -->
<!--      <div class="container fnblanco" id="f2" style="display:none">
        <div class="row">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate dolore quod aperiam commodi doloribus eveniet corporis, iusto! Illum ipsam, dolor cum suscipit veritatis ad accusamus inventore ipsa autem sapiente. Illum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia recusandae, ad ex non dolores aut id! Accusamus nisi laudantium natus eum. Nesciunt ullam, provident dolores? Placeat minima neque dolorem, ea! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi molestiae libero ipsam consectetur esse ducimus, praesentium tempora quae enim aspernatur vel quam aperiam, quaerat consequatur atque dolorum eos vitae quas.</p>
        </div>
    </div> -->


        <!-- **************************************************************** -->
   </div>
<!--:::::::::FIN VOLUNTARIADO:::::::::-->
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->

  <div class="comunidad">
    <div class="row feposak-2 valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom_l->image)}})" >
      <div class="col s12 m8 valign">
        <p>{{$bottom_l->description_es}}</p>
      </div>
      <div class="col s12 m4 valign">
        <a href=  {{ route($bottom_l->link) }} ><img src="maqueta/img/comunidades-png.png" alt="" class="responsive-img center-block"></a>
      </div>
    </div>
  </div>

<!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->

  <div class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
  </div>

<!--:::::::::::::FOOTER:::::::::::::-->
  <footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
  </footer> 
@stop