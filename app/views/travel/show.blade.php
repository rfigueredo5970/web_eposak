@extends('layouts.master-admin')

@section('content')
                <div class="x_panel"">
                    <div class="x_title">
                        <h2>{{ $travel->title_es }} </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a href="/esposak/travel" class="btn" ><i>Back</i></a>
                            </li>
                            <li  >
                                {{Form::open(array('url'=> 'travel/'.$travel->id , 'method' => 'delete')) }}
                                {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                {{Form::close()}}    
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                            <div class="row col-md-3" style="margin-bottom:3px;">
                                <div class="col-md-6">
                                    <h6  class="text-center alert-warning" >ESPAÑOL </h6> 
                                </div>
                                <div class="col-md-6">
                                    <h6  class="text-center  alert-success">INGLES </h6>
                                </div>
                            </div>
                            <br>
                            <br>
                        <div class="container well text-center row" >    
                            <h1 class="alert alert-warning text-center">{{ $travel->title_es }} </h1><br>
                            <h3 class="alert alert-success text-center" style="margin-top:-10px;" >{{ $travel->title_en }} </h3>
                            <br>
                            <br>

                            <div class="row col-md-6">
                                <div class="row container-fluid " >

                                    <div class="col-md-12 well" >
                                        <h6 class="alert alert-warning text-center">Descripcion</h6>
                                        <p style="word-wrap: break-word;" >{{ $travel->description_es }}</p>
                                        <h6 class="alert alert-success text-center">Description</h6>
                                        <p style="word-wrap: break-word;" >{{ $travel->description_en }}</p>
                                    </div>
                                </div>
                                    

                                <div class="row  containe-fuid " >
                                    <div class="col-md-12 row " >
                                        <div class="col-md-6" > 
                                            <h6 class="alert alert-warning text-center">Recomendaciones (Es)</h6>
                                            <p style="word-wrap: break-word;" >{{ $travel->recommendation_es }}</p>
                                        </div>
                                        <div class="col-md-6" > 
                                            <h6 class="alert alert-success text-center">Recomendaciones (En)</h6>
                                            <p style="word-wrap: break-word;" >{{ $travel->recommendation_en }}</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row containe-fuid ">
                                    <div class="col-md-12 row ">
                                        <div class="col-md-6" >
                                            <h6 class="alert alert-warning text-center">Incluido (Es)</h6>
                                            <p style="word-wrap: break-word;" >{{ $travel->include_es }}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="alert alert-success text-center">Incluido (En)</h6>
                                            <p style="word-wrap: break-word;" >{{ $travel->include_en }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row containe-fuid ">
                                    <div class="col-md-12 row ">
                                        <div class="col-md-6" >
                                            <h6 class="alert alert-warning text-center">No Incluido (Es)</h6>
                                            <p style="word-wrap: break-word;" >{{ $travel->dont_include_es }}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="alert alert-success text-center">No Incluido (En)</h6>
                                            <p style="word-wrap: break-word;" >{{ $travel->dont_include_en }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 row">
                                @if (sizeof($travel->multimedias) > 0)
                                    <div style="width:100%; height:380px;" id="carousel-example-generic" 
                                    class="well container-fluid col-md-12 carousel slide" 
                                    data-ride="carousel">
                                  <!-- Wrapper for slides -->
                                        <div style="width:100%;height:340px;" 
                                        class="carousel-inner" 
                                        role="listbox">
                                            {{--*/ $isFirst = true; /*--}}
                                            @foreach($travel->multimedias as $t)                         
                                                <div  class="item{{{ $isFirst ? ' active' : '' }}}">
                                                    {{Form::open(array('url'=> 'multimedia/'.$t->id , 'method' => 'delete')) }}
                                                     {{Form::submit('Eliminar')}}
                                                     {{Form::close()}} 
                                                    {{ 
                                                        HTML::Image('/images/'.$t->name, null ,array('class' => 'media-object', 'style' => 'width: 100% ;display: block;border:solid black 1px; border-radius:15px;')) 
                                                    }}
                                                </div>
                                                {{--*/ $isFirst = false; /*--}}
                                            @endforeach
                                        </div>

                                      <!-- Controls -->
                                          @if(sizeof($travel->multimedias) > 1)
                                              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                              </a>
                                              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                              </a>
                                          @endif
                                    </div> 
                                @endif
                                <div class="row" >
                                    <div class="col-md-12">
                                        <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">Precio total BsF </th>
                                                    <th class="column-title">Dias </th>
                                                    <th class="column-title">Noches</th>
                                                    <th class="column-title">Fecha de salida</th>
                                                    <th class="column-title">Fecha de retorno</th>
                                                    <th class="column-title">Lugar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="even pointer">
                                                    <td class=" "> {{ $travel->prices }}</td>
                                                    <td class=" "> {{ $travel->days }}</td>
                                                    <td class=" ">{{ $travel->nights }}</td>
                                                    <td class=" ">{{ $travel->start_at }}</td>
                                                    <td class=" ">{{ $travel->end_at }}</td>
                                                    <td class=" ">{{ $travel->destiny->name_es }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-12">
                                        <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title">Dia</th>
                                                    <th class="column-title">Viaje (Es)</th>
                                                    <th class="column-title">fecha</th>
                                                    <th class="column-title">fecha y hora de comenzar</th>
                                                    <th class="column-title">fecha y hora de finalizar</th>
                                                    <th class="column-title"><a class="btn btn-default" href="/esposak/itinerary/create">Crear nuevo Itinerario</a></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(sizeof($travel->itinerary) > 0)
                                                   @foreach($travel->itinerary as $i)
                                                        <tr class="even pointer">
                                                            <td class=" ">{{$i->day}}</td>
                                                            <td class=" ">{{$i->travel->title_es}}</td>
                                                            <td class=" ">{{$i->date}}</td>
                                                            <td class=" ">{{$i->start_at }}</td>
                                                            <td class=" ">{{$i->end_at}}</td>
                                                            <td class=" last">
                                                                <a class="btn btn-info btn-block" href="/esposak/itinerary/{{ $i->id }}">Ver</a>
                                                                <br>    
                                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/itinerary/{{ $i->id }}/edit">editar</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                </div>
                @stop