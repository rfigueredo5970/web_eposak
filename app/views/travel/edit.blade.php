@extends('layouts.master-admin')

@section('content')

{{-- @if ($errors->any())
        <?php echo "<pre>"; print_r($errors); echo "</pre>"; ?>
@endif --}}

                <!-- /top tiles -->
<div class="x_panel">
    <div class="x_title">
        <h2>Form Design <small>different form elements</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="/esposak/travel" class="btn"><i>Volver</i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="display: block;">
        <br>
        {{Form::open(array('url' => 'travel/' . $travel->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">titulo español<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('title_es',$travel->title_es,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('title_es') }}</div>
                    
                    <br/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">titulo ingles<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('title_en',$travel->title_en,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('title_en') }}</div>
                    <br/>
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion español<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::textArea('description_es',$travel->description_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                    <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                    
                </div>
            </div> 

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion ingles<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textArea('description_en',$travel->description_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                   
                </div>
            </div> 

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Recomendaciones (Es)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::text('recommendation_es',$travel->recommendation_es,array('class' => 'form-control' , 'style' => 'resize: none;')) }}
                   <div style="color:red;" class="error">{{ $errors->first('recommendation_es') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Recomendaciones (En)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('recommendation_en',$travel->recommendation_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('recommendation_en') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Incluido (ingles)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('include_en',$travel->include_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('include_en') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">incluido (español)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('include_es',$travel->include_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('include_es') }}</div>
                   
                </div>
            </div>


            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No incluido (español)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('dont_include_es',$travel->dont_include_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('dont_include_es') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> No incluido (ingles)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('dont_include_en',$travel->dont_include_en,array('class' => 'form-control' , 'style' => 'resize: none','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('dont_include_en') }}</div>
                </div>
            </div>

            {{Form::macro('custom', function($type, $name, $value = null, $options = array()){
                $value = ((is_null($value) or $value == '')) ? Input::old($name) : $value;
                $input =  '<input type="'. $type .'" name="' . $name . '" value="' . $value . '"';
                foreach ($options as $key => $value) {
                    $input .= ' ' . $key . '="' . $value . '"';
                    }
                return $input.'>';
                });
            }}


            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de salida actual<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h4>
                        {{$travel->start_at}}
                    </h4>
                </div>
            </div>


            <div class="form-group filter">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar Fecha de salida <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="date" name="start_at" class="form-control" >
                    <div class="error" style="color:red;">{{ $errors->first('start_at') }}</div>
                </div>
            </div>



            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Retorno Actual<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h4>
                        {{$travel->end_at}}
                    </h4>
                </div>
            </div>




            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar Fecha de Retorno<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="date" name="end_at" class="form-control" >
                    <div class="error" style="color:red;">{{ $errors->first('end_at') }}</div>
                </div>
            </div>


            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Precio Actual<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h4>
                        {{$travel->prices}}
                    </h4>
                </div>
            </div>


            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar precio<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   <input type="number"  step="0.01" name="prices" class="form-control">
                   <div style="color:red;" class="error">{{ $errors->first('prices') }}</div>
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione el destino<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('destiny_id',$destiny,null,array('class' => 'form-control')) }}
                   <div style="color:red;" class="error">{{ $errors->first('destiny_id') }}</div>
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione Tipo o Categoria de viaje<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('travel_type_id',$travel_type,null,array('class' => 'form-control')) }}
                   <div style="color:red;" class="error">{{ $errors->first('travel_type_id') }}</div>
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione archivo multimedia <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{ Form::file('name[]',array('multiple'=>true),$media,null,array('class' => 'form-control')) }}
                   <div style="color:red;" class="error">{{ $errors->first('name') }}</div>
                </div>
            </div>




            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    
                    {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                </div>
            </div>

        {{Form::close() }}
    </div>
</div>
@stop