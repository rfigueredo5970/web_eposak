@extends('layouts.master-admin')

@section('content')

       	        <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2> Viajes Esposak <small> listado de viajes</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="travel/create">Crear nuevo Viaje</a>
                                </ul>
                                <div class="clearfix"></div>	
                            </div>
                            @foreach($travel as $t)
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            @if (sizeof($t->multimedias) > 0)
                                                {{ HTML::Image('/images/'.$t->multimedias[0]->name, null ,array('style' => 'width: 100% ;display: block')) }}
                                            @endif
                                            <div class="mask no-caption">
                                                <div class="tools tools-bottom">
                                                     <a href="/esposak/travel/{{ $t->id }}/edit"><i class="fa fa-pencil"></i></a>
                                            		<a href="travel/{{ $t->id }}">Ver</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="caption">
                                            <p><strong>{{ $t->title_es }}</strong>
                                            </p>
                                            <p>{{$t->destiny->name_es}}</p>
                                        </div>
                                    </div>
                                </div>
							@endforeach
                        </div>
                    </div>
                </div>
                @stop