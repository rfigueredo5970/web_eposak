@extends('layouts.master-admin')

@section('content')
    			<div class="x_panel" >
                    <div class="x_title">
                        <h2>{{$destiny->name_es}}</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a href="/esposak/destiny" class="btn" ><i>Back</i></a>
                            </li>
                            <li  >
                                {{ Form::open(array('url'=> 'destiny/'.$destiny->id, 'method' => 'delete')) }}
                                {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                {{Form::close()}}
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="container well text-center " >    
                            <h1>{{ $destiny->name_es }} </h1><br>
                            <h3 style="margin-top:-10px;" >{{ $destiny->name_en }} </h3>
                            <br>
                            <br>
                            <div class="row container-fluid " >
                                <div class="col-md-6" >
                                    <h6 style="opacity:0.50;">-----------------Español-----------------</h6>
                                    <p style="word-wrap: break-word;" >{{ $destiny->description_es }}</p>
                                    <h6 style="opacity:0.50;">-----------------Ingles-----------------</h6>
                                    <p style="word-wrap: break-word;" >{{ $destiny->description_en }}</p>
                                </div>
                                <div class="text-center col-md-4" >
                                    {{ HTML::image('/images/'.$destiny->image,null,array('style' =>'height: 200px'))}}
                                    <br>

                                    {{$destiny->country->nombre}}
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                @stop