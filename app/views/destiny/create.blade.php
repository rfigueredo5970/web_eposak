@extends('layouts.master-admin')

@section('content')
                <!-- /top tiles -->
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Nuevo destino<small>Crear un nuevo destino Esposak!</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/destiny" class="btn ">back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'destiny', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="button_1_en">Nombre(Es) <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name_es',null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('name_es') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="button_1_es">Nombre (En)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name_en',null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('name_en') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion (Es)<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textarea('description_es',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion (En)<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textarea('description_en',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione el pais<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              {{ Form::select('country_id',$country,null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('country_id') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="logo" >Imagen<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div style="color:red;" class="error">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop