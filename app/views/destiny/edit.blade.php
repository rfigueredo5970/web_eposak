@extends('layouts.master-admin')

@section('content')
                <!-- /top tiles -->
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Modificar Destino <small>Editar destino Esposak! </small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/destiny" class="btn">back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'destiny/' . $destiny->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                    


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">titulo español<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name_es',$destiny->name_es,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('name_es') }}</div>
                                                
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">titulo ingles<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name_en',$destiny->name_en,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('name_en') }}</div>
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion español<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textArea('description_es',$destiny->description_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                                                
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion ingles<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textArea('description_en',$destiny->description_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                                               
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Latitud<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('lat',$destiny->lat,array('class' => 'form-control' , 'style' => 'resize: none;')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('lat') }}</div>
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> longitud <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::text('lng',$destiny->lng,array('class' => 'form-control' , 'style' => 'resize: none;')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('lng') }}</div>
                                               
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione el pais<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              {{ Form::select('country_id',$country ,null,array('class' => 'form-control')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('country_id') }}</div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Imagen<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>




                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop