@extends('layouts.master-admin')

@section('content')

            <!-- page content -->
            <div class="" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Emprendedor Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="entrepeneur/create">Agregar nuevo Aliado</a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Nombre</th>
                                            <th class="column-title">Apellido</th>
                                            <th class="column-title">Proyecto</th>
                                            <th class="column-title">localidad</th>
                                            <th class="column-title">Status</th>
                                            <th class="column-title">Tipo de Proyecto</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($entrepreneur as $e)
                                        <tr class="even pointer">
                                            <td class=" ">{{$e->id}}</td>
                                            <td class=" ">{{$e->name}}</td>
                                            <td class=" ">{{$e->last_name}}</td>
                                            <td class=" ">{{$e->project->title_es}}</td>
                                            <td class=" ">{{$e->project->destiny->name_es}}</td>
                                            <td class=" ">{{$e->project->status}}</td>
                                            <td class=" ">{{$e->project->type}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="entrepeneur/{{ $e->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/entrepeneur/{{ $e->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @stop