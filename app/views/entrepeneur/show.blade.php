@extends('layouts.master-admin')

@section('content')
                    <div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>Emprendedor del proyecto: ' {{ $entrepreneur->project->title_es }} '</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/entrepeneur" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{Form::open(array('url'=> 'entrepeneur/'.$entrepreneur->id , 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}    
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------nombre-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $entrepreneur->name }}</p>
                                    </div>
                                    <div class="text-center col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------apellido-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $entrepreneur->last_name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop