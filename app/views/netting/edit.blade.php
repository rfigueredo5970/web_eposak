@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Redes sociales<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a href={{"/esposak/netting/"  . $netting->id}} class="btn"><i>Volver</i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <br>
                        {{Form::open(array('url' => 'netting/' . $netting->id, 'method' => 'patch','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('name',$netting->name,array('class' => 'form-control','id' => 'name')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('name') }}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cuenta" class="control-label col-md-3 col-sm-3 col-xs-12">Cuenta<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('cuenta', $netting->cuenta ,array('class' => 'form-control','id' => 'cuenta')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('cuenta') }}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="icon" class="control-label col-md-3 col-sm-3 col-xs-12">Icono<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('icon', $netting->icon, array('class' => 'form-control','id'=>'icon')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('icon') }}</div>
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href={{"/esposak/netting/" . $netting->id}} class="btn btn-primary"> Cancel </a>
                                    {{Form::submit('Guardar',array('class' => 'btn btn-success')) }}
                                </div>
                            </div>

                        {{Form::close() }}
                    </div>
                </div>
                @stop