@extends('layouts.master-admin')

@section('content')
<div class="x_panel" style="height:600px;">
    <div class="x_title">
        <h2>nombre: ' {{ $netting->name }} '</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="/esposak/netting" class="btn" ><i>Volver</i></a>
            </li>
          
            <li>
                {{Form::open(array('url' => 'netting/' . $netting->id . '/edit', 'method' => 'get', null, 'class' => 'form-horizontal form-label-left;' )) }}
                {{Form::submit('Edit',array('class'=>'btn btn-warning btn-block'))}}
                {{Form::close()}}
            </li>
            <li  >
                {{ Form::open(array('url'=> 'netting/'.$netting->id, 'method' => 'delete')) }}
                {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                {{Form::close()}}
            </li>

        </ul>
        <div class="clearfix"></div>
        <div class="container well text-center " >    
            <h1>{{ $netting->name }} </h1><br>
            <br>
            <br>
            <div class="row container-fluid " >
                <div class="col-md-12" >
                   <li class="{{ $netting->icon}} "><h5>{{ $netting->cuenta }}</h5></li>
                    <p style="word-wrap: break-word;" >{{ $netting->phone_number }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
	
@stop