@extends('layouts.master')

  @section('content')
<!--:::::::::INICIO INICIO CASOS DE EXITO:::::::::-->
<section class="comunidad-inter">
   <div class="birongo-iner">
        <div class="container">
            <div class="row">
                   <div class="col s12">
                        <a href= {{ URL::previous() }} ><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                   </div>
            </div>
            <div class="row">
                   <div class="col s12">
                        <h1>proyectos birongo</h1>
                   </div>
            </div>
        </div>
    </div>
</section>
<section class="exito">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h1>aporta a la comunidad</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro architecto suscipit, alias quis, perspiciatis saepe rem mollitia nesciunt sapiente voluptates id libero. Deleniti nesciunt incidunt, quidem nihil! Debitis vero, fuga. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis possimus, cum odio in nisi est cumque eaque hic adipisci, repudiandae magni aliquam, aperiam ducimus recusandae quia. Assumenda accusamus, dolorem quisquam!</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <h2>ordenar por: <a href=""><span data-activates='dropdown1' class="dropdown-button">fecha <i class="fa fa-angle-down" aria-hidden="true"></i></span></a></h2>
                <!-- Dropdown Structure -->
                  <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="#!">cultura educación</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">agricultura</a></li>
                    <li class="divider"></li>                    
                    <li><a href="#!">monto del proyecto</a></li>
                  </ul>
            </div>
        </div>
        <div class="row">
           <!--:::::::/*EMPRENDEDOR*/:::::::-->
           <div class="col s12 m6">
             <div class="ttlemprendedor valign-wrapper">
                 <h1 class="valign">emprendimiento</h1>
             </div>
              <div class="col s12 padding0">
                   <div class="ficha valign-wrapper">
                    <div class="col s4 valign">
                        <img src="img/emprendedor.jpg" alt="" class="circle responsive-img center-block" height="150" width="150">
                    </div>
                    <div class="col s8">
                        <h3>nombre del emprendedor</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quas illum ad, magnam ex.</p>
                        <h3><a href="proyecto-comunitario.html"><span>ver más</span></a></h3>
                    </div>
                   </div>                
              </div>
              <div class="col s12 padding0">
                   <div class="ficha valign-wrapper">
                    <div class="col s4 valign">
                        <img src="img/emprendedor.jpg" alt="" class="circle responsive-img center-block" height="150" width="150">
                    </div>
                    <div class="col s8">
                        <h3>nombre del emprendedor</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quas illum ad, magnam ex.</p>
                        <h3><a href="proyecto-comunitario.html"><span>ver más</span></a></h3>
                    </div>
                   </div>                
              </div> 
           </div>
           <!--:::::::/*COMUNITARIO*/:::::::-->
           <div class="col s12 m6">
               <div class="ttlemprendedor valign-wrapper">
                  <h1 class="valign">comunitarios</h1>
               </div>
               <div class="col s12 padding0">
                   <div class="ficha-2 valign-wrapper">
                    <div class="col s4 padding0 back-comu">
                        <div class="mas-expan valign-wrapper"><i class="fa fa-expand center-block valign" aria-hidden="true"></i></div>
                    </div>
                    <div class="col s8 ">
                       <div class="valign">
                            <h3>nombre del proyecto</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quas illum ad, magnam ex.</p>
                            <h3><a href="proyecto-comunitario.html"><span>ver más</span></a></h3>
                        </div>
                    </div>
                   </div>                
              </div>
              <div class="col s12 padding0">
                   <div class="ficha-2 valign-wrapper">
                    <div class="col s4 padding0 back-comu">
                        <div class="mas-expan valign-wrapper"><i class="fa fa-expand center-block valign" aria-hidden="true"></i></div>
                    </div>
                    <div class="col s8 ">
                       <div class="valign">
                            <h3>nombre del proyecto</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quas illum ad, magnam ex.</p>
                            <h3><a href="proyecto-comunitario.html"><span>ver más</span></a></h3>
                        </div>
                    </div>
                   </div>                
              </div>
           </div>            
        </div>
    </div>
</section>
<!--::::::::::/*BOTON CARGAR MAS*/::::::::::-->
<section class="comunidad-inter">
    <div class="center-block" style="width: 30%">
        <a class="waves-effect waves-light btn">CARGAR MÁS</a> 
    </div>
</section>
<!--::::::::::/*BOTON CARGAR MAS*/::::::::::-->
<!--::::::::FIN INICIO CASOS DE EXITO::::::::-->
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="row feposak-2 valign-wrapper">
            <div class="col s12 m8 valign">
                <p>Conoce  la fundación eposak, los proyectos que <br> tenemos y cómo puedes colaborar en la comunidad</p>
            </div>
            <div class="col s12 m4 valign">
                <a href=""><img src="img/comunidades-png.png" alt="" class="responsive-img center-block"></a>
            </div>
        </div>
</section>
<!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
<section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer>
@stop