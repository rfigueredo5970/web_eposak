@extends('layouts.master')

  @section('content')
<!--:::::::::::INICIO FORMULARIO VIAJE:::::::::::-->

<section>
	<div class="container-fluid">
          <div class="row " >
              <img src= {{ url( "images/", $navbar->image)}} alt="" class="principalimg responsive-img">
          </div>
      </div>
</section>

  <section class="container">
		<div class="row">
			<div class="offset-s4 offset-m4 col s4 m4 valign">
				{{ $botonpago }}
			</div>

		</div>
  </section>
<!--:::::::::::::FIN FORMULARIO VIAJE:::::::::::::-->


   
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="feposak valign-wrapper">
           <div class="row">
                <div class="col s12 m8 valign">
                    <p>Conoce  la fundación eposak, los proyectos que <br> tenemos y cómo puedes colaborar en la comunidad</p>
                </div>
                <div class="col s12 m4 valign">
                    <a href="comunidades.html"><img src= {{ url("maqueta/img/comunidades-png.png")}} alt="" class="responsive-img center-block"></a>
                </div>
            </div>
        </div>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer> 
@stop