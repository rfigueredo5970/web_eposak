@extends('layouts.master-admin')

@section('content')
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Aliados Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="allies/create">Agregar nuevo Aliado</a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Nombre (Aliado)</th>
                                            <th class="column-title">Url Pagina (Aliado) </th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($allies as $a)
                                        <tr class="even pointer">
                                            <td class=" ">{{$a->id}}</td>
                                            <td class=" ">{{$a->name}}</td>
                                            <td class=" ">{{$a->url_page}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="allies/{{ $a->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/allies/{{ $a->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop