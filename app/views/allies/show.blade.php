@extends('layouts.master-admin')

@section('content')
        			<div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>{{ $allies->name }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/allies" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{ Form::open(array('url'=> 'allies/'.$allies->id, 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>{{ $allies->name }} </h1><br>
                                <br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h1 style="word-wrap: break-word;" >{{ $allies->url_page}}</h1>
                                    </div>
                                    <div class="text-center col-md-4 row" >
                                        <div class="col-md-3  col-md-offset-6">
                                            <span >Imagen</span>
                                            {{ HTML::image('/images/'.$allies->image,null,array('style' =>'height: 200px'))}}
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop