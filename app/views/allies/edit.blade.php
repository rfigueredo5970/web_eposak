@extends('layouts.master-admin')

@section('content')
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Form Design <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/allies" class="btn">back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'allies/' . $allies->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                    


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">URL de pagina de Empresa Aliada<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('url_page',$allies->url_page,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('url_page') }}</div>
                                                
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre Del Aliado<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name',$allies->name,array('class' => 'form-control')) }}
                                                <div style="color:red;" class="error">{{ $errors->first('name') }}</div>
                                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">imagen<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::file('image') }}<br/>
                                               <div class="error" style="color:red;">{{ $errors->first('image') }}</div>
                                            </div>
                                        </div>

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop