@extends('layouts.master-admin')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Crear Usuarios<small>Puedes crear usuarios de tipo administrador y de tipo invitado</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="display: block;">
        <br>
        {{Form::open(array('url' => 'users','route'=>'post', 'files' => true, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Usuario<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('user_name',null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('user_name') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Contraseña<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::password('password',array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('password') }}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Confirmar<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::password('password_confirmation',array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('password_confirmation') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Balance</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('balance',null,array('class' => 'form-control')) }}
                </div>
            </div> 
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Número telefónico</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('phone',null,array('class' => 'form-control')) }}
                     <div style="color:red;" class="error">{{ $errors->first('phone') }}</div>
                </div>
            </div> 
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Documento de Identidad</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::text('document',null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('document') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Permisos</label>
                <div class="col-md-6 col-sm-6    col-xs-12">
                   {{Form::select('level', array('admin' => 'Administrador', 'free' => 'Miembro'),null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('level') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Imagen</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::file('picture') }}<br/>
                   <div style="color:red;" class="error">{{ $errors->first('picture') }}</div>
                </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="/esposak/users" class="btn btn-primary">Cancelar</a>
                    {{Form::submit('Guardar',array('class' => 'btn btn-success')) }}
                </div>
            </div>
        {{Form::close() }}
    </div>
</div>
@stop