@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Editar Usuario <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href={{"/esposak/users/"  . $user->id}} class="btn"><i>Volver</i></a>
                                        </li>
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                    {{Form::open(array('url' => 'users/' . $user->id, 'method' => 'patch','files' => true, null, 'class' => 'form-horizontal form-label-right;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_name">Usuario <span class="required">*</span>
                                               
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('user_name', $user->user_name,array('class' => 'form-control','id' => 'user_name')) }}
                                                    <div style="color:red;" class="error">{{ $errors->first('document') }}</div>
                                                </div>                                            
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre <span class="required">*</span>
                                                
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('firstname', $user->firstname,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                </div>                                            
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" >Apellido <span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('lastname', $user->lastname,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                </div>                                                     
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" >Correo<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('email', $user->email,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                </div>
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" >Dirección de habitación<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('address', $user->address,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                </div>
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" >Teléfono<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('phone', $user->phone,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                </div>
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Balance<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('balance', $user->balance,array('class' => 'form-control')) }}  <ul class="parsley-errors-list" id="parsley-id-8218"></ul>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label  class="control-label col-md-3 col-sm-3 col-xs-12">Documento de Identidad
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::text('document', $user->document,array('class' => 'form-control')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group filter">
                                            <label  class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Nacimiento<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="date" name="birthdate" class="form-control" value={{$user->birthdate}}> 
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::password('password',array('class' => 'form-control')) }}
                                                    <div style="color:red;" class="error">{{ $errors->first('password') }}</div>
                                                </div>
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar<span class="required">*</span>
                                            
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::password('password_confirmation',array('class' => 'form-control')) }}
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">imagen
                                                
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::file('picture') }}<br/>
                                                    <div style="color:red;" class="error">{{ $errors->first('picture') }}</div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label  class="control-label col-md-3 col-sm-3 col-xs-12">Biografía<span class="required">*</span>
                                                
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {{Form::textArea('bio',Input::old('bio',$user->bio),array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                                    <div style="color:red;" class="error">{{ $errors->first('bio') }}</div>
                                                
                                                </div>
                                            </label>
                                        </div> 

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a href= "/esposak/users" class="btn btn-primary"> Cancelar </a>
                                                {{Form::submit('Guardar',array('class' => 'btn btn-success' ))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                <!-- /top tiles -->
@stop