@extends('layouts.master-admin')

@section('content')
                <!-- /top tiles -->
                <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2> Usuarios <small>listado de usuarios</small></h2>
                               <!-- <div class="filter">
                                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span>July 20, 2016 - August 18, 2016</span> <b class="caret"></b>
                                        </div>
                                    </div> -->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div class="col-md-11 col-sm-12 col-xs-12">
                                        <div>
                                            <div class="x_title">
                                                <h2>Nombres</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <ul class="list-unstyled top_profiles scroll-view" tabindex="5001" style="overflow: hidden; outline: none; cursor: -webkit-grab;">
                                            @foreach($users as $user)
                                                <li class="media event">
                                                
                                                    <div  class="pull-left border-aero profile_thumb">
                                                        <i class="fa fa-user aero"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <a class="title" href="users/{{ $user->id }}">{{ $user->user_name }}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
@stop