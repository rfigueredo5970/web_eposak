<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Viaje Voluntariado - Eposak</title>
    <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      
      <link type="text/css" rel="stylesheet" href="css/style.css"/>
      <link type="text/css" rel="stylesheet" href="css/font-awesome.css"/> 
      <!--favicon-->
      <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
      <link rel="icon" href="img/favicon.ico" type="image/x-icon">
</head>
<body>
<!--:::::::::::::INICIO BARRA NAVEGACION:::::::::::::-->
<div class="navbar-fixed">
   <nav>
    <div class="nav-wrapper">
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <div class="container-fluid">
          <div class="row">
              <div class="col s12 m2 hide-on-med-only hide-on-small-only">
                  <a href="index.html" class="brand-logo"><img src="img/logo.png" alt="" class="center-block"></a>
              </div>
               <div class="col s12 m2 hide-on-large-only ">
                  <a href="index.html" class="brand-logo "><img src="img/logo-m.png" alt="" class="center-block"></a>
              </div>
              <div class="col s7 m8">
                  <ul class="right hide-on-med-and-down">
                    <li><a href="eposak.html" class="verdeoscuro">Qué es Eposak</a></li>
                    <li><a href="comunidades.html" class="verdeclaro">Comunidades Eposak</a></li>
                    <li><a href="fundacion.html" class="carne">Fundación Eposak</a></li>
                    <li><a href="contacto.html" class="carne2">Contacto</a></li>
                    <li><a href="#" class="naranja opcionmmenu regisuser">Registro / Login</a></li>
                    <li class="buscajs cerrarmegamenu"><a href="#" class="rojo"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                  </ul>
              </div>
              <div class="col s3 m2 redesnav">
                  <ul class="left hide-on-med-and-down" style="margin-top: -12px;">
                      <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href=""><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                      <li><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                  </ul>
              </div>
              <div class="idiomanav">
                  <ul class="hide-on-med-and-down">
                      <li>
                          <p><a href="" class="idiomafuera">EN</a><a href="" class="idiomadentro">- ES</a></p>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
      <!--/*::::::::::::::::PARTE MOVIL::::::::::::::::*/-->
      <ul class="side-nav" id="mobile-demo">
        <li><a href="eposak.html">Qué es Eposak</a></li>
        <li><a href="comunidades.html">Comunidades Eposak</a></li>
        <li><a href="fundacion.html">Fundación Eposak</a></li>
        <li><a href="contacto.html">Contacto</a></li>
        <li><a href="#" class="opcionmmenu">Registro / Login</a></li>
        <li class="buscajs"><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
        <li><p><a href="">EN</a><a href="">ES</a></p></li>
      </ul>
    </div>
  </nav>
    </div>
<!--:::::::::::::FIN BARRA NAVEGACION:::::::::::::-->
 <!--/*:::::::::::::INICIO REGISTRO:::::::::::::*/-->
<section class="registro" id="megamenu">
    <div class="container">
        <div class="row">
            <div class="col s12 l6">
                   <h1>registro</h1>
               <div class="col s12 l6 padding0">
                   <form>
                     <div class="row">
                        <div class="input-field-5">
                          <input id="first_name" type="text" class="validate" tabindex="1">
                          <label for="first_name" class="label-epo">Usuario:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="email" class="validate" tabindex="2">
                          <label for="email" class="label-epo">Email:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="password" class="validate" tabindex="4">
                          <label for="email" class="label-epo">Contraseña:</label>
                        </div>
                      </div>
                    </form>
               </div>
               <div class="col s12 l6 padding0">
                   <form>
                      <div class="row">
                        <div class="input-field-5" style="margin-top: 36px;">
                          <input id="email" type="email" class="validate" tabindex="3">
                          <label for="email" class="label-epo">Confirmar:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="password" class="validate" tabindex="5">
                          <label for="email" class="label-epo">Confirmar:</label>
                        </div>
                      </div>
                    </form>
                     <p>o registrate con</p>
                    <div class="row">
                       <ul>
                           <a href=""><li><i class="fa fa-facebook-square" aria-hidden="true"></i></li></a>
                           <a href=""><li><i class="fa fa-twitter-square" aria-hidden="true"></i></li></a>
                       </ul>
                        <a class="btn waves-effect waves-light btnenviar cerrarmegamenu" style="float: right;">REGISTRO</a>
                    </div>
               </div>              
            </div>
            <div class="col s12 offset-l1 l4">
                  <h1>inicio de sesión</h1>
                   <form>
                     <div class="row">
                        <div class="input-field-5">
                          <input id="first_name" type="text" class="validate" tabindex="6">
                          <label for="first_name" class="label-epo">Usuario:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-5">
                          <input id="email" type="password" class="validate" tabindex="7">
                          <label for="email" class="label-epo">Contraseña:</label>
                        </div>
                      </div>
                    </form>
                    <div class="row">
                       <ul>
                           <li><a href=""><p>¿Olvido su contraseña?</p></a></li>
                           <li>
                               <p>
                           <input class="with-gap" name="group1" type="radio" id="test1" tabindex="8">
                           <label for="test1">Recordarme</label>
                        </p>
                           </li>
                       </ul>
                    </div>
                    <div class="row">
                        <a class="btn waves-effect waves-light btnenviar cerrarmegamenu" style="float: right;">INICIAR</a>
                    </div>
            </div>
        </div>
    </div>
</section>
    
<!--/*:::::::::::::FIN REGISTRO:::::::::::::*/--> 
<!--:::::::::::::::INICIO BUSCA:::::::::::::::-->
<section class="buscar" id="megabuscar">
    <div class="container">
        <div class="col s12">
            <div class="col-1">
                <input type="text" placeholder="BUSCAR" id="buscar">
            </div>
            <div class="col-2 valign-wrapper">
                <a href="#" for="buscar" class="center-block cerrarbuscar"><i class="fa fa-search valign" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
<!--:::::::::::::::::FIN BUSCA::::::::::::::::-->
<!--:::::::::::::::INICIO RESULTADO:::::::::::::::-->
<section class="resultado" style="display:none">
    <div class="container">
       <div class="row">
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
            <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
            <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
            <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
           </div>
           <div class="col s12 m6">
                <a href="#"><h1>Lorem ipsum dolor sit <span>petición</span>, consectetur adipisicing elit.</h1></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, <span>petición</span>, nostrum! Atque impedit in reiciendis, odit. </p>
            </div>
       </div>
       <center>
           <ul class="pagination">
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">2</a></li>
                <li class="waves-effect"><a href="#!">3</a></li>
                <li class="waves-effect"><a href="#!">4</a></li>
                <li class="waves-effect"><a href="#!">5</a></li>
                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </ul>
       </center>        
    </div>
</section>
<!--:::::::::::::::FIN RESULTADO:::::::::::::::-->
<!--::::::::::INICIO INICIO VIAJES EXRENCIALES::::::::::-->
<section class="comunidad-inter">
   <div class="viaje-volun">
        <div class="container">
            <div class="row">
                   <div class="col s12">
                        <a href="index.html"><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                   </div>
            </div>
            <div class="row">
                   <div class="col s12">
                        <h1>viaje de voluntariado</h1>
                   </div>
            </div>
        </div>
    </div>
    <!--::::::::::INICIO VIAJE EXPERENCIAL::::::::::-->
    <div class="container">
        <div class="row">
            <h2>viajes experencial <span>ordenar por <a href="" data-activates='dropdown1' class="dropdown-button ">fecha de salida <i class="fa fa-chevron-down" aria-hidden="true"></i>
</a></span></h2>
       <!-- Dropdown Structure -->
                  <ul id='dropdown1' class='dropdown-content drop-rojo'>
                    <li><a href="#!">cultura educación</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">agricultura</a></li>
                    <li class="divider"></li>                    
                    <li><a href="#!">monto del proyecto</a></li>
                  </ul>
        </div>
        <div class="row">
            <div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src="img/fondonegro.png" alt="" class="">
            </div>
        </div>
        <div class="row">
            <div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src="img/fondonegro.png" alt="" class="">
            </div>
        </div>
        <div class="row">
            <div class="col s12 padding0 viaex">
                <h1>Chicken drumstick flank doner<br>strip steak. Chicken pork loin</h1>
                <h2><i class="fa fa-map-marker" aria-hidden="true"></i> kamarata, edo. bolívar</h2>
                <div class="verdeprecio">
                    <p>precio bs.00000</p>
                </div>
                <div class="carnedia">
                    <p>00 días</p>
                </div>
                <img src="img/fondonegro.png" alt="" class="">
            </div>
        </div>
               <div class="center-block" style="width: 30%">
                  <a class="waves-effect waves-light btn">CARGAR MÁS</a> 
               </div> 
    </div>
</section>
<section class="comunidad-inter fondogris">
</section>
<!--::::::::::FIN COMUNIDAD KAMARATA::::::::::-->
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="row feposak valign-wrapper">
            <div class="col s12 m8 valign">
                <p>Conoce  la fundación eposak, los proyectos que <br> tenemos y cómo puedes colaborar en la comunidad</p>
            </div>
            <div class="col s12 m4 valign">
                <a href="fundacion.html"><img src="img/comunidad4.png" alt="" class="responsive-img center-block"></a>
            </div>
        </div>
</section>
<!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
<section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer> 
   <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.js"></script>
      <script type="text/javascript" src="js/slider.js"></script>
      <script type="text/javascript" src="js/menu.js"></script>
      <script type="text/javascript" src="js/parallax.js"></script>
      <script type="text/javascript" src="js/carousel.js"></script>
      <script type="text/javascript" src="js/subir.js"></script>
      <script type="text/javascript" src="js/megamenu.js"></script>
</body>
</html>