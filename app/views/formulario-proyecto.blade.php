@extends('layouts.master')

  @section('content')
<!--:::::::::::INICIO FORMULARIO VIAJE:::::::::::-->
  <section class="formviaje">
      <div class="container-fluid">
          <div class="row " >
              <img src= {{ url( "images/", $navbar->image)}} alt="" class="principalimg responsive-img">
          </div>
      </div>
      <div class="formcontacto">
          <div class="container">
               {{ Form::open(['route' => 'collaborate','method'=>'post']) }}
                <input type="hidden" name="project" value="{{$project->id}}" >
              <div class="row">
                  <div class="col s12">
                      <h1>proyecto desarrollo</h1>
                      <p>formulario de aporte</p>
                  </div>
              </div>
              <div class="row">
                    <div class="col s12 m6">
                     <div class="row">
                        <div class="input-field-2 col s12">
                          <input id="firstname" name="firstname" type="text" class="validate" required="required" tabindex="1" value="{{$user->firstname}} ">
                          <label for="firstname" class="label-epo">Nombre:</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field-2 col s12">
                          <input disabled="disabled" name="email" id="email" type="email" class="validate" tabindex="5" value="{{$user->email}}" >
                          <label for="email" class="label-epo">Email:</label>
                        </div>
                      </div>
                    <div class="row">
                        <div class="input-field-2 col s12">
                          <input id="nationality" name="nationality" type="text" class="validate" required="required" tabindex="1" value="{{$user->nationality}} ">
                          <label for="nationality" class="label-epo">Nacionalidad:</label>
                        </div>
                    </div>
                    </div>
                    <!--::::::::::::/*DERECHA*/::::::::::::-->
                    <div class="col s12 m6">
                      <div class="row">
                        
                        <div class="input-field-2 col s12">
                          <input id="lastname" type="text" name="lastname" class="validate" required="required" tabindex="2" value="{{$user->lastname}}">
                          <label for="lastname" class="label-epo">Apellido:</label >
                        </div>
                      </div>

                      <div class="row">
                        <div class="input-field-2 col s12">
                          <input id="icon_telephone" name="phone" type="tel" class="validate" required="required" tabindex="7" value="{{$user->phone}}" >
                          <label for="icon_telephone" class="label-epo">Teléfono:</label>
                        </div>
                      </div>
                     <div class="row">
                        <div class="input-field-2 col s12">
                          <input id="document" name="document" type="text" class="validate"  required="required" tabindex="1" value="{{$user->document}} ">
                          <label for="document" class="label-epo">Cédula de Id.:</label>
                        </div>
                    </div>
                    </div>
                    <!--::::::::::::/*FIN DERECHA*/::::::::::::-->
              </div>
              <div class="row">
                 <div class="col s12 l6">
                      <div class="input-field-3">
                        <textarea id="textarea1" name="message_es" class="materialize-textarea textarea-1" tabindex="9"></textarea>
                        <label for="textarea1" class="textarea-label">Mensaje Opcional:</label>
                      </div>
                  </div>
                  <div class="col s12 l6">
                      <div class="aporte-epo">
                             <div class="col s12">
                                 <h1>APORTE:</h1>
                             </div>                              
                              <div action="#">
                                   <div class="col s6">
                                       <p>
                                          <input class="with-gap" name="group1" type="radio" value="default" id="default_contribution" class="validate"  checked />
                                          <label for="default_contribution">50 Bs</label>
                                       </p>
                                   </div>
                                   <div class="col s6">
                                       <p>
                                          <input class="with-gap" name="group1" type="radio" value="other" id="other_contribution" class="validate"  />
                                            <label for="other_contribution"> OTRO MONTO (Bs.): <input id="other-contribution" name="contribution" type="number" min="50" class="validate input-monto" tabindex="11"> </label>
                                       </p>
                                           
                                   </div>                                   
                              </div>
                          </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col s12">
                      {{ Form::submit('COLABORA', [ 'route'=>'collaborate', 'class' => 'btn waves-effect waves-light btnenviar-2','onclick' => 'Materialize.toast("Su solicitud esta siendo enviada", 4000)' ]) }}
                  </div>
              </div>
              {{Form::close() }}
          </div>
      </div>
  </section>
<!--:::::::::::::FIN FORMULARIO VIAJE:::::::::::::-->
   
<!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
<section class="comunidad">
        <div class="feposak valign-wrapper">
           <div class="row">
                <div class="col s12 m8 valign">
                    <p>Conoce  la fundación eposak, los proyectos que <br> tenemos y cómo puedes colaborar en la comunidad</p>
                </div>
                <div class="col s12 m4 valign">
                    <a href="comunidades.html"><img src= {{ url("maqueta/img/comunidades-png.png")}} alt="" class="responsive-img center-block"></a>
                </div>
            </div>
        </div>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer> 



@stop

 