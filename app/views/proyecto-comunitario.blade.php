@extends('layouts.master')

  @section('content')
  
<!--::::::::::INICIO COMUNIDAD KAMARATA::::::::::-->
<section class="comunitario">
   <div class="principalimg" style="background-image:url({{ asset('/images/'.$section->image)}})">
        <div class="container">
            <div class="row">
                   <div class="col s12">
                        <a href= {{  URL::previous()  }} ><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                   </div>
            </div>
            <div class="row">
                   <div class="col s12">
                        <h1>{{$project->title_es}}</h1>
                   </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col s12 m6">
                <div class="titulogaleria-2 z-depth-1">
                    <h1><i class="fa fa-camera" aria-hidden="true"></i> galería</h1>
                </div>
                <div class="galeimg" style="background-image:url({{ asset('/images/'.$project->multimedias[rand(0,sizeof($project->multimedias)-1)]->name)}})"></div>
                @foreach($project->multimedias as $p)
                  <div class="col s6 m4 relative">
                      <a href=""><div class="expand">
                      <i class="fa fa-expand" aria-hidden="true"></i>
                      </div></a>
                      {{ 
                        HTML::Image('/images/'.$p->name, null ,array('class' => 'responsive-img center-block')) 
                                                    }}
                  </div>
                @endforeach
                <!--
                <div class="col s6 m4 relative">
                   <a href=""><div class="expand">
                    <i class="fa fa-expand" aria-hidden="true"></i>
                    </div></a>
                    <img src="img/miniatura.jpg" alt="" class="responsive-img center-block">
                </div>
                <div class="col s6 m4 relative">
                   <a href=""><div class="expand">
                    <i class="fa fa-expand" aria-hidden="true"></i>
                    </div></a>
                    <img src="img/miniatura.jpg" alt="" class="responsive-img center-block">
                </div>
                <div class="col s6 hide-on-med-and-up relative">
                   <a href=""><div class="expand">
                    <i class="fa fa-expand" aria-hidden="true"></i>
                    </div></a>
                    <img src="img/miniatura.jpg" alt="" class="responsive-img center-block">
                </div>
                -->
            </div>
            <div class="col s12 m5 offset-m1">
                <div class="ttltecnica">
                    <h1>especificaciones técnicas</h1>
                </div>
                <div class="col s12 padding0">
                    <h2><span>COSTO TOTAL DEL PROYECTO:</span></h2>
                    <h2>{{$project->total_cost}} (BS/$)</h2>
                    <h2><span>RECAUDADO:</span> {{ $project->collected}} (BS/$)</h2>
                </div>
                <div class="col s12 padding0">
                    <p>{{$project->description_es}}</p>
                </div>
                <div class="col s6 m12  l6">
                    <h2>Compartir en:</h2>
                    <ul>
                      @foreach($principal_netting as $p_n)
                        <a href="{{$p_n->cuenta}}"><li><i class="{{$p_n->icon}}" aria-hidden="true"></i></li></a>
                      @endforeach
                    </ul>
                </div>
                <div class="col s6 m12 l6">
                    <!--CUANDO EL PROYECTO ESTA ABIERTO-->
                    @if(Auth::user()  && $project->status !== 'end' )
                    <a class="waves-effect waves-light btn" href={{route("form_proyecto", $project->id)}} ><i class="fa fa-check-circle-o" aria-hidden="true"></i> COLABORA</a>
                    @elseif( $project->status !== 'end')
                    <a href={{route("form_proyecto", $project->id)}}  class="waves-effect waves-light btn opcionmmenu">Colabora</a>
                    @else
                    <a class="waves-effect waves-light btn disabled"><i class="fa fa-star" aria-hidden="true"></i> FINALIZADO</a>
                    @endif
                    <!--CUANDO EL PROYECTO ESTA CERRADO Y ES CASO DE EXITO-->
                    <!--
                    -->
                </div>
                <div class="col s12">
                    <div class="fondo-descrip-2">
                    <h1>beneficiados</h1>
                    <ul>
                      @foreach($project->benefit as $b)
                        <li>{{$b->description_es}}</li>
                      @endforeach
                    </ul>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="ttlficha">
                    <h1><i class="fa fa-bar-chart" aria-hidden="true"></i> ficha proyecto</h1>
                </div>
            </div>
            <div class="col s12 l4">
               <div class="total valign-wrapper">
                   <h1 class="valign center-block"><span>total a financiar:</span> {{$project->total_finance}} (BS/$)</h1>
               </div>
           </div>
           <div class="col s12 l8">
               <div class="financiamiento">
                 <div class="row">
                     <div class="col s12">
                      <h1 style="margin-top: 16px;"><span>financiamiento obtenido:</span> {{$project->obtained_funding}} (BS/$)</h1>
                  </div>
                 </div>
                  <div class="row">
                      <div class="col s12">
                      <ul class="center-block" style="width:80%; margin:0 auto; margin-top: -20px;">
                       <li><h1>0%</h1></li>
                       <li class="listaw"><div class="rango" style="width:80%"></div></li>
                       <li><h1>100%</h1></li>
                      </ul> 
                   </div>
                  </div>
                   <div class="row">
                       <div class="col s12">
                           <h1 style="margin-top: -28px;"><span>repago:</span> {{$project->repago}} (BS/$)</h1>
                      </div> 
                   </div>
                  <div class="row">
                      <div class="col s12">
                       <ul class="center-block" style="width:80%; margin:0 auto; margin-top: -37px;">
                           <li><h1>0%</h1></li>
                           <li class="listaw"><div class="rango" style="width:50%"></div></li>
                           <li><h1>100%</h1></li>
                       </ul>
                   </div> 
                  </div>                                     
               </div>
           </div>
           <div class="col s12">
               <div class="objetivo"> 
                   <h1>objetivo</h1>
                   <p>{{$project->objective_es}}</p>
               </div>
           </div>
        </div>
    </div>
</section>
<!--/*::::::::INICIO TESTIMONIO::::::::*/-->
<section class="testimonio">
    <div class="container">
        <div class="row">
           <div class="col s12">
            <h1>TESTIMONIOS</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et natus rerum ducimus impedit repellendus, quasi sint suscipit enim tempore iste alias explicabo eaque in non labore, eum earum voluptate sequi? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium libero, vero fuga animi accusamus. Eum voluptas amet ratione voluptate fugit? Unde sit tempora laudantium veritatis ipsum quod ad commodi? Neque. <span>Nombre Aprellido</span></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, possimus expedita ipsam ex alias nisi optio, quo, dolores magni illo minima libero ratione, molestias magnam. Rerum inventore labore a perspiciatis. <span>Nombre y Apellido</span></p>
           </div>
        </div>
    </div>
</section>
<!--/*::::::::FIN TESTIMONIO::::::::*/-->

<section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer>
@stop