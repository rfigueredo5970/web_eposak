@extends('layouts.master-admin')

@section('content')
        			<div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>Testimonio: ' {{ $testimony->title_en }} '</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/testimony" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{Form::open(array('url'=> 'testimony/'.$testimony->id , 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}    
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>{{ $testimony->title_es }} </h1><br>
                                <h3 style="margin-top:-10px;" >{{ $testimony->title_en }} </h3>
                                <br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Descripcion Español-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $testimony->description_es }}</p>
                                    </div>
                                    <div class="text-center col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Descripcion Ingles-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $testimony->description_en }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop