@extends('layouts.master-admin')

@section('content')
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Editar Testimonio <small>Formulario para editar Testimonio</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/testimony" class="btn" >back</a> 
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                   {{Form::open(array('url' => 'testimony/' . $testimony->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Titulo (Es)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_es',$testimony->title_es,array('class' => 'form-control'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                <div style="color:red;" class="error">{{ $errors->first('title_es') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Titulo (En)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('title_en',$testimony->title_en,array('class' => 'form-control'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                <div style="color:red;" class="error">{{ $errors->first('title_en') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descripcion (Es)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textarea('description_es',$testimony->description_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descripcion (En)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textarea('description_en',$testimony->description_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                                <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                                            </div>
                                        </div>

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class'=>'btn btn-block btn-success'))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                            @stop