@extends('layouts.master-admin')

@section('content')
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Testimonios Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="testimony/create">Agregar nuevo Testimonio </a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Titulo (Españo)</th>
                                            <th class="column-title">Titulo (Ingles)</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($testimony as $testimonio)
                                        <tr class="even pointer">
                                            <td class=" ">{{$testimonio->id}}</td>
                                            <td class=" ">{{$testimonio->title_es}}</td>
                                            <td class=" ">{{$testimonio->title_en}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="testimony/{{ $testimonio->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/testimony/{{ $testimonio->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop