@extends('layouts.master-admin')

@section('content')
  <div class="x_panel">
    <div class="x_title">
        <h2>Form Design <small>different form elements</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="/esposak/projects" class="btn">back</a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="display: block;">
        <br>
        {{Form::open(array('url' => 'projects/' . $project->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

        


            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">titulo español<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('title_es',$project->title_es,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('title_es') }}</div>
                    
                    <br/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">titulo ingles<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('title_en',$project->title_en,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('title_en') }}</div>
                    <br/>
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion español<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::textArea('description_es',$project->description_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                    <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                    
                </div>
            </div> 

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">descripcion ingles<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textArea('description_en',$project->description_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                   
                </div>
            </div> 

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">costo total<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::text('total_cost',$project->total_cost,array('class' => 'form-control' , 'style' => 'resize: none;')) }}
                   <div style="color:red;" class="error">{{ $errors->first('total_cost') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Objetivos a Cumplir (español)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('objective_es',$project->objective_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('objective_es') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Objetivos a Cumplir (ingles)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('objective_en',$project->objective_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('objective_en') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">beneficios para la comunidad (español)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('community_benefits_es',$project->community_benefits_es ,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('community_benefits_es') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">beneficios para la comunidad (ingles)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('community_benefits_en',$project->community_benefits_en,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('community_benefits_en') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> Oferta o Fin del proyecto (español)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('final_project_es',$project->final_project_es,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('final_project_es') }}</div>
                   
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> Oferta o Fin del proyecto (ingles)<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textarea('final_project_en',$project->final_project_en,array('class' => 'form-control' , 'style' => 'resize: none','size' => '30x5')) }}
                   <div style="color:red;" class="error">{{ $errors->first('final_project_en') }}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Progreso</label>
                <div class="col-md-6 col-sm-6    col-xs-12">
                   {{Form::select('status', array('stop' => 'En Espera', 'start' => 'Comenzado','end'=>'Terminado'),null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('status') }}</div>
                </div>
            </div>

            
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Progreso</label>
                <div class="col-md-6 col-sm-6    col-xs-12">
                   {{Form::select('type', array('comunitario' => 'comunitario', 'emprendimiento' => 'emprendimiento'),null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('type') }}</div>
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione el pais<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('country_id',$country,null,array('class' => 'form-control')) }}
                   <div style="color:red;" class="error">{{ $errors->first('country_id') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione el Destino<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('destiny_id',$destiny,null,array('class' => 'form-control')) }}
                   <div style="color:red;" class="error">{{ $errors->first('country_id') }}</div>
                </div>
            </div>



            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">seleccione archivo multimedia <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{ Form::file('name[]',array('multiple'=>true),$media,null,array('class' => 'form-control')) }}
                   <div style="color:red;" class="error">{{ $errors->first('name') }}</div>
                </div>
            </div>




            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    
                    {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                </div>
            </div>

        {{Form::close() }}
    </div>
    </div>
@stop