@extends('layouts.master-admin')

@section('content')
                
                <!-- /top tiles -->
        			<div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>Plain Page</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/projects" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{Form::open(array('url'=> 'projects/'.$project->id , 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}    
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>{{ $project->title_es }} </h1><br>
                                <h3 style="margin-top:-10px;" >{{ $project->title_en }} </h3>
                                <br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Español-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $project->description_es }}</p>
                                        <h6 style="opacity:0.50;">-----------------Ingles-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $project->description_en }}</p>
                                    </div> 
                                    <div class="col-md-6 row">
                                @if (sizeof($project->multimedias) > 0)
                                    <div style="width:100%; height:380px;" id="carousel-example-generic" 
                                    class="well container-fluid col-md-12 carousel slide" 
                                    data-ride="carousel">
                                  <!-- Wrapper for slides -->
                                        <div style="width:100%;height:340px;" 
                                        class="carousel-inner" 
                                        role="listbox">
                                            {{--*/ $isFirst = true; /*--}}
                                            @foreach($project->multimedias as $p)                         
                                                <div  class="item{{{ $isFirst ? ' active' : '' }}}">
                                                    {{Form::open(array('url'=> 'multimedia/'.$p->id , 'method' => 'delete')) }}
                                                     {{Form::submit('Eliminar')}}
                                                     {{Form::close()}} 
                                                    {{ 
                                                        HTML::Image('/images/'.$p->name, null ,array('class' => 'media-object', 'style' => 'width: 100% ;display: block;border:solid black 1px; border-radius:15px;')) 
                                                    }}
                                                </div>
                                                {{--*/ $isFirst = false; /*--}}
                                            @endforeach
                                        </div>

                                      <!-- Controls -->
                                          @if(sizeof($project->multimedias) > 1)
                                              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                              </a>
                                              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                              </a>
                                          @endif
                                    </div> 
                                @endif
                                                <!--{{ HTML::Image('/images/'.$p->name, null ,array('style' => 'width: 100% ;display: block;border:solid black 1px; border-radius:15px')) }}-->
                                </div>
                                <div class="row container-fluid " >
                                    <div class="col-md-3" >
                                        <h6 style="opacity:0.50;">-----------------Objetivos Español-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $project->objective_es }}</p>
                                        <h6 style="opacity:0.50;">-----------------Objetivos Ingles-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $project->objective_en }}</p>
                                    </div>
                                    <div class="text-center col-md-3" >
                                        <h6 style="opacity:0.50;">-----Beneficios Comunitarios Español-----</h6>
                                        <p style="word-wrap: break-word;" >{{ $project->community_benefits_es }}</p>
                                        <h6 style="opacity:0.50;">-----Beneficios Comunitarios Ingles------</h6>
                                        <p style="word-wrap: break-word;" >{{ $project->community_benefits_en }}</p>
                                    </div>
                                    <div class="text-center col-md-6" >
                                        <p class="text-center" style="word-wrap: break-word;" >tipo de proyecto: {{ $project->type }}</p>
                                        <p class="text-center" style="word-wrap: break-word;" >precio total Bsf: {{ $project->total_cost }}</p>
                                        <p class="text-center" style="word-wrap: break-word;" >destino: {{ $project->destiny->name_es }}</p>
                                        <p class="text-center" style="word-wrap: break-word;" >Emprendedor: {{ $project->status }}</p>
                                        @if( isset($project->entrepreneurs))
                                        <p class="text-center" style="word-wrap: break-word;" >status: {{ $project->entrepreneurs->name }}</p>
                                        @endif

                                         {{Form::open(array('url' => 'changestatus/' . $project->id, 'method' => 'put','files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}
                                         <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Progreso</label>
                                                    <div class="col-md-9 col-sm-6    col-xs-12">
                                                    {{Form::select('status', array('stop' => 'En Espera', 'start' => 'Comenzado','end'=>'Terminado'),null,array('class' => 'form-control')) }}
                                                    <div style="color:red;" class="error">{{ $errors->first('status') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                {{Form::submit('guardar',array('class' => 'btn btn-success ')) }}
                                            </div>  
                                         </div>
                                        {{Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop