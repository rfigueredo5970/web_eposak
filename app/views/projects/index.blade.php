@extends('layouts.master-admin')

@section('content')

       	        <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2> Proyectos Esposak <small> listado de Proyectos</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="projects/create">Crear nuevo Proyecto</a>
                                </ul>
                                <div class="clearfix"></div>	
                            </div>
                            @foreach($project as $p)
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            @if (sizeof($p->multimedias) > 0)
                                                {{ HTML::Image('/images/'.$p->multimedias[0]->name, null ,array('style' => 'width: 100% ;display: block')) }}
                                            @endif
                                            <div class="mask no-caption">
                                                <div class="tools tools-bottom">
                                                     <a href="/esposak/projects/{{ $p->id }}/edit"><i class="fa fa-pencil"></i></a>
                                            		<a href="projects/{{ $p->id }}">Ver</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="caption">
                                            <p><strong>{{ $p->title_es }}</strong>
                                            </p>
                                            <p>{{ $p->destiny->name_es}}</p>
                                        </div>
                                    </div>
                                </div>
							@endforeach
                        </div>
                    </div>
                </div>
                @stop