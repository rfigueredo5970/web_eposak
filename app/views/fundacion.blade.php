@extends('layouts.master')

  @section('content')
  <!--::::::::::INICIO FUNDACION EPOSAK::::::::::-->
    <div class="fundacionepo">
      <div class="funda-inter" style="background-image:url({{ asset('/images/'.$navbar->image)}})">
        <div class="container">
          <div class="row">
            <div class="col s12">
              <h1>{{$navbar->name}}</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col s12 m3">
            <div class="valign-wrapper" style="min-height: 500px;">
              <div class="valign center-block">
                <a href="{{$navbar->url_section}}"><img src="maqueta/img/fundacioncr.png" alt="" class="responsive-img center-block hvertical"></a>
              </div>
            </div>                
          </div>
          <div class="col s12 m3 hide-on-med-and-up">
            {{-- <ul>
              <li><h4 class="fa fa-circle" aria-hidden="true"></h4> <a href="">Fundación Eposak</a></li>
              <li><h4 class="fa fa-circle" aria-hidden="true"></h4> <a href="">Título 2</a></li>
              <li><h4 class="fa fa-circle" aria-hidden="true"></h4> <a href="">Título 3</a></li>
            </ul> --}}
          </div>
          <div class="col s12 m6">
            <p>{{$navbar->description_es}}</p>
          </div>
          <div class="col s12 m3 hide-on-small-only">
            {{-- <ul>
              <li><h4 class="fa fa-circle" aria-hidden="true"></h4> <a href="">Fundación Eposak</a></li>
              <li><h4 class="fa fa-circle" aria-hidden="true"></h4> <a href="">Título 2</a></li>
              <li><h4 class="fa fa-circle" aria-hidden="true"></h4> <a href="">Título 3</a></li>
            </ul> --}}
          </div>
        </div>
      </div>
    </div>
    <div class="fundacionepo">
      <div class="container-fluid">
        <div class="row" style="height: 500px;">
                 <!--::::::::::::INICIO CAROUSEL::::::::::::-->
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              {{--*/ $isFirst = true; /*--}}
              @foreach($today as $i)
                <div class="item{{{ $isFirst ? ' active' : '' }}}">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col s12 m12 l6 padding0">
                        <div class="imgnoti" style="background-image:url({{ asset('/images/'.$i->image)}})">      
                        </div>
                      </div>
                      <div class="col s12 m12 l6">
                        <img src="maqueta/img/logo.png" class="responsive-img center-block margintop20">
                        <h2>{{$i->title_es}} <a href= {{ route('noticia', $i->id ) }} ><i class="fa fa-chevron-right" aria-hidden="true"></i></a></h2>
                        <h6>{{$i->create_at}}</h6>
                        <h3>Bacon ipsum dolor amet tongue turkey pork chop, strip steak picanha</h3>
                        <p>{{$i->description_es}}</p>
                        <h1><spam><a href= {{  route('noticia',$i->id ) }} >VER MÁS</a></spam></h1>
                      </div>
                    </div>
                  </div>
                </div>
                {{--*/ $isFirst = false; /*--}}
              @endforeach
            </div>
          </div>
                 <!--:::::::::::::FIN CARROUSEL:::::::::::::-->
        </div>
      </div>
    </div>
    <center>
      <ul class="pagination">
        <li class="waves-effect" ><a role="button" data-slide="prev"  href="#carousel-example-generic"><i class="material-icons fa fa-chevron-left"></i></a></li>

        @for ($i = 1; $i <= sizeof($today); $i++)
          <li class="waves-effect" data-target="#carousel-example-generic" data-slide-to="{{ $i -1 }}" class=""><a href="#!">{{ $i }}</a></li>
        @endfor

<!-- ************************************************************************* -->

            <!--
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"><a href="#!">1</a></li>
            <li data-target="#carousel-example-generic" data-slide-to="1" class="waves-effect"><a href="#!">2</a></li>
            <li data-target="#carousel-example-generic" data-slide-to="2" class="waves-effect"><a href="#!">3</a></li>
            <li data-target="#carousel-example-generic" data-slide-to="3" class="waves-effect"><a href="#!">4</a></li>
            <li data-target="#carousel-example-generic" data-slide-to="4" class="waves-effect"><a href="#!">5</a></li> -->

   <!-- **************************************************************************** -->

        <li class="waves-effect"><a href="#carousel-example-generic" role="button" data-slide="next"><i class="material-icons fa fa-chevron-right"></i></a></li>
      </ul>
    </center>

  <!--::::::::::/*BOTON CARGAR MAS*/::::::::::-->
    <div class="comunidad">
      <div class="">
        <div class="row aportepo valign-wrapper">
          <div class="col s12 valign">
            <h1>Aportes eposak</h1>
          </div>
        </div>
        <div class="row padding0 ver-todo">
          @foreach($destiny as $d)
            <div class="col s12 l4 padding0">
              @if($d->id == 3)
                <div class="kamaratafondo-2" style="background-image:url({{ asset('/images/'.$imagen_destiny_project_kamarata)}})">
              @else
                <div class="kamaratafondo-2" style="background-image:url({{ asset('/images/'.$imagen_destiny_project_birongo)}})">
              @endif
              <h3 style="text-transform: uppercase;" >{{$d->name_es}}</h3>
              <div class="rosac">
                <a href= {{ route('proyecto_kam', $d->id ) }} ><p>VER MÁS</p></a>
              </div>
            </div>    
            </div>
          @endforeach
            <div class="col s12 l4 padding0">
              <div class="valign-wrapper">
                <a href= {{ route('exito') }} ><h2 class="valign">Casos de éxito <i class="fa fa-chevron-right" aria-hidden="true"></i></h2>
                </a>
              </div>
              <div class="lineacomu"></div>
                <div class="valign-wrapper">
                  <a href= {{ route('voluntariado') }}><h2 class="valign">Ver oportunidades de voluntariado <i class="fa fa-chevron-right" aria-hidden="true"></i></h2></a>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!--::::::::::FIN FUNDACION EPOSAK::::::::::-->
  <!--:::::::::::INICIO BANNER FOOTER:::::::::::-->

      <div class="comunidad">
        <div class="row feposak-2 valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom->image)}})">
          <div class="col s12 m8 valign">
            <p class="valign">{{$bottom->description_es}}</p>
          </div>
          <div class="col s12 m4 valign">
            <a href= {{ route($bottom->link) }} ><img src= {{ url("maqueta/img/comunidades-png.png")}} alt="" class="responsive-img center-block"></a>
          </div>
        </div>
      </div>
  <!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->

      <section class="btn-subir">
        <a href="">
          <h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
          <p>SUBIR</p>
        </a>
      </section>

<!--:::::::::::::FOOTER:::::::::::::-->

  <footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
  </footer>

@stop