@extends('layouts.master')

  @section('content')
<!--:::::::::::/*INICIO QUE ES EPOSAK*/:::::::::::-->
<section class="quees">
    <div class="container-fluid">
        <div class="row">
            <div class="col s12 l4 padding0">
                <div class="altura-movil" style="background-image:url({{ asset('/images/'.$section_eposak->image)}})"></div>
            </div>
            <div class="col s12 l4">
                 <!--::::::::::::INICIO CAROUSEL::::::::::::-->
                 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                      {{--*/ $isFirst = true; /*--}}
                        @foreach($information as $i)
                        <div class="item{{{ $isFirst ? ' active' : '' }}}">
                          <h1>{{$i->title_en}}</h1>
                          <p>{{$i->description_es}}</p>
                          @if($i->url_multimedia != null)
                            <a href="{{$i->url_multimedia}}"><h2>ver video <i class="fa fa-play-circle-o" aria-hidden="true"></i></h2></a>
                          @endif
                        </div>
                        {{--*/ $isFirst = false; /*--}}
                        @endforeach
                        
                        
                         <div class="item">
                          <h1>Aliados</h1>
                              <div class="row">
                                @foreach($aliados as $a)
                                  <div class="col s6 m4">
                                      <a href="{{$a->url_page}} ">{{ HTML::image('/images/'.$a->image,null,array('class' => 'responsive-img center-block'));}}</a>
                                  </div>
                                @endforeach 
                              </div>
                        </div>
                      </div>
                 </div>
            </div>
                 <!--:::::::::::::FIN CARROUSEL:::::::::::::-->
                 <div class="col s12 l4 padding0">
                   <div class="altura-movil-2" style="background-image:url({{ asset('/images/'.$section_eposak->imagen_2)}})""></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="mini-menu hide-on-med-and-down">
                 <ul class="center-block">

                 
                  @for ($i = 0; $i <= sizeof($information)-1; $i++)
                    <li class="waves-effect" data-target="#carousel-example-generic" data-slide-to="{{ $i  }}" class="">{{ $information[$i]->title_es }}</a></li>
                    @if($i == sizeof($information)-1)
                      <li class="waves-effect" data-target="#carousel-example-generic" data-slide-to="{{ $i+1  }}">aliados</li>
                    @endif
                  @endfor
                </ul>
            </div>
        </div>
    </div>
</section>
<!--:::::::::::/*FIN QUE ES EPOSAK*/:::::::::::-->
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer> 
@stop