<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>Eposak</title>
    <!--Import Google Icon Font-->
      {{ HTML::style('http://fonts.googleapis.com/icon?family=Material+Icons'); }}
      <!--Import materialize.css-->
      

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
     <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 100%; }
    </style>
      
      {{ HTML::style('maqueta/css/font-awesome.css'); }}
      {{ HTML::style('maqueta/css/bootstrap.css'); }} 
      <!--favicon-->
      {{ HTML::style('maqueta/css/materialize.css'); }} 
      {{ HTML::style('maqueta/css/style.css'); }} 
  </head>

  <body>

{{--   <?php echo $principal_netting->first()->icon; die; ?> --}}

    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
         
           <div style="float:left; position: absolute;">                       
            @if(Session::has('message')) 
              <p class="btn" id="message" style="float:left;position: relative;">{{ Session::get('message') }}</p>
            @endif
             
             
            @if($errors->has())
         
                <p class="btn" id="error_message" style="position: relative;">               
                    {{ $errors->first() }}
             </p>
                <br>  
                        
            @endif   
          </div>
          
          <div class="clearfix"></div>

          @foreach($page as $p)
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <div class="container-fluid">
              <div class="row">
                <div class="col s12 m2 hide-on-med-only hide-on-small-only">
                  <a href= {{ route('home') }}  class="brand-logo">{{ HTML::image('/images/'.$p->logo,null,array('class' => 'center-block')); }}</a>
                </div>
                <div class="col s12 m2 hide-on-large-only ">
                  <a href= {{ route('home') }} class="brand-logo ">{{ HTML::image('/images/'.$p->logo,null,array('class' => 'center-block')); }} </a>
                </div>
                <div class="col s7 m8">
                  <ul class="right hide-on-med-and-down">
                    <li><a href= {{ route('eposak') }} class="verdeoscuro">{{$p->button_1_es}} </a></li>
                    <li><a href= {{ route('comunidades') }} class="verdeclaro">{{$p->button_2_es}} </a></li>
                    <li><a href= {{ route('fundacion') }}  class="carne">{{$p->button_3_es}}</a></li>
                    <li><a href= {{ route('contacto') }} class="carne2">{{$p->button_4_es}}</a></li>
                    @if(Auth::user())
                      <li><a href="#" class="naranja opcionmmenu regisuser">{{$p->button_6_es}}</a></li>
                    @else
                      <li><a href="#" class="naranja opcionmmenu regisuser">{{$p->button_5_es}}</a></li>
                    @endif
                      <li class="buscajs cerrarmegamenu"><a href="#" class="rojo"><i class="fa fa-search" aria-hidden="true"></i></a></li> 
                  </ul>
                </div>



             
                <div class="col s3 m2 redesnav">
                  <ul class="left hide-on-med-and-down" style="margin-top: -12px;">
                    @foreach($principal_netting as $p_n )
                      
                      <li><a target="_blank" href={{$p_n->cuenta}} ><i class ="{{ $p_n->icon }} "   aria-hidden="true"></i></a></li> 

                    @endforeach
                  </ul>
                </div>

            

                <div class="idiomanav">
                  <ul class="hide-on-med-and-down">
                    <li>
                      <p>
                        {{-- <a href="" class="idiomafuera">EN</a> --}}
                        <span style="color"> / </span>
                        <a href="" class="idiomadentro"> ES</a>
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!--/*::::::::::::::::PARTE MOVIL::::::::::::::::*/-->
            <ul class="side-nav" id="mobile-demo">
              <li><a href= {{ route('eposak') }} >{{$p->button_1_es}}</a></li>
              <li><a href= {{ route('comunidades') }}>{{$p->button_2_es}}</a></li>
              <li><a {{ route('fundacion') }}>{{$p->button_3_es}}</a></li>
              <li><a {{ route('contacto') }} >{{$p->button_4_es}}</a></li>
              <li><a href="#" class="opcionmmenu">{{$p->button_5_es}}</a></li>
            {{--   <li class="buscajs"><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li> --}}
              <li><p><a href="">EN</a><a href="">ES</a></p></li>
            </ul>
          @endforeach
        </div>
      </nav>
    </div>
    <!--fin barra de navegacion-->
    <div class="registro" id="megamenu">
       
       <div  style="float:left; position: absolute;">
          <p class="btn btn-danger" id="validation_error" style="position: relative; display:none; background-color:#DA604C; color:white">               
                    
           </p>
          <p class="btn btn-info" id="validation_message" style="position: relative; display:none;  color:white">               
                    
           </p>
        </div> 
      
      <div class="container">
        
        
        <div class="row">
          @if(Auth::user())
            <div class="col s12 l6">
              <h1>{{Auth::user()->user_name}}</h1>
              <div class="col s12 l6 padding0">
                <a href="logout" class="btn waves-effect waves-light btnenviar cerrarmegamenu" style="float: right;">Salir</a>
              </div>             
            </div>
          @else
            <div class="col s12 l6">
              <h1>registro</h1>
                <div class="col s12 l6 padding0">
                 {{Form::open(array('url' => 'users', 'name' => 'user_registration','route'=>'post', 'files' => true, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}
                  <div class="row">
                    <div class="input-field-5 active truncate">
                      {{Form::text('user_name',null,array('class' => 'validate','tabindex'=>'1','id'=>'user_name'))}}
                   
                      <label for="user_name" class="label-epo">Usuario:</label>
                      <div style="color:red;" class="error">{{$errors->first('user_name')}}</div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="input-field-5 truncate">
                        {{Form::email('email',null,array('class' => 'validate','tabindex'=>'1','id'=>'email'))}}
                        
                        <label for="email" class="label-epo">Email:</label>
                      </div>
                    </div>
                </div>
                <div class="col s12 l6 padding0">
                  <div class="row">
                    <div class="input-field-5 truncate" >
                    {{Form::password('password',null,array('class' => 'validate','tabindex'=>'1', 'name'=>'password', 'id'=>'password', "autocomplete" => "off")) }}                    
                      <label for="password" class="label-epo">Contraseña:</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field-5 truncate">
                     {{Form::password('password_confirmation',null,array('class' => 'validate','tabindex'=>'1','name'=>'password_confirmation', 'id'=>'confirmation')) }}                   
                      <label for="confirmation" class="label-epo">Confirmar:</label>
                        <div style="color:red;" class="error">{{ $errors->first('password_confirmation') }}</div>
                    </div>
                  </div>
                  <p>o regístrate con</p>
                  <div class="row">
                    <ul>
                      <a href=""><li><i class="fa fa-facebook-square" aria-hidden="true"></i></li></a>
                      <a href=""><li><i class="fa fa-twitter-square" aria-hidden="true"></i></li></a>
                    </ul>
                    {{ Form::button('REGISTRO', ['class' => 'btn waves-light btnenviar','id' => 'btn-register']) }}
                 
                    {{Form::close() }}
                  </div>
                </div>              
              </div>
              <div class="col s12 offset-l1 l4">
                <h1>inicio de sesión</h1>
                {{ Form::open(['url' => 'login']) }}
                  @if(Session::has('error_message'))
                    <div class="alert alert-danger" style="z-index:100000" >{{ Session::get('error_message') }}</div>
                  @endif
                    <div class="row">
                      <div class="input-field-5 truncate">
                        {{ Form::text('user_name_2',null,['class'=>'validate','tanabindex'=>'6','id'=>'user_name_2']) }}

                        <label for="user_name_2" class="label-epo">Usuario:</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field-5 truncate">
                        {{ Form::password('password_2',null,['class'=>'validate','tabindex'=>'7','id'=>'password_2'])}}
                    
                        <label for="password_2" class="label-epo">Contraseña:</label>
                      </div>
                    </div>
                    <div class="row">
                      <ul>
                        <li><a href=""><p>¿Olvidó su contraseña?</p></a></li>
                        <li>
                          <p>
                            <input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" />
                            <label for="filled-in-box">Recordarme</label>
                          </p>
                        </li>
                      </ul>
                    </div>
                    <div class="row">
                      {{ Form::submit('Iniciar', ['class' => 'btn waves-effect waves-light btnenviar', 'style'=> 'float: right;', 'id'=>'btn-login']) }}
                    
                      {{ Form::close() }}
                    </div>
                  @endif                  
              </div>
        </div>
      </div>
      
    </div>
    <div class="buscar" id="megabuscar">
      <div class="container">
        {{ Form::open() }}
          <div class="col s12">
            <div class="col-1">

              <input type="text" placeholder="BUSCAR" id="buscar">
            </div>
            <div class="col-2 valign-wrapper">


              <a href="#" for="buscar" class="center-block cerrarbuscar">
                <i id="search-icon" class="fa fa-search valign" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        {{Form::close()}}
      </div>
    </div>
    <div class="resultado" style="display:none">
      <div class="container result-container">
        <div class="row">
               
        </div>

        <center>               
               <div id="pagination"></div>
             </center> 
       {{--  <center>
          <ul class="pagination">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><a href="#!">1</a></li>
            <li class="waves-effect"><a href="#!">2</a></li>
            <li class="waves-effect"><a href="#!">3</a></li>
            <li class="waves-effect"><a href="#!">4</a></li>
            <li class="waves-effect"><a href="#!">5</a></li>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
          </ul>
        </center>   --}}      

  {{--       <ul class = "pagination">
   <li><a href = "#">&laquo;</a></li>
   <li><a href = "#">1</a></li>
   <li><a href = "#">2</a></li>
   <li><a href = "#">3</a></li>
   <li><a href = "#">4</a></li>
   <li><a href = "#">5</a></li>
   <li><a href = "#">&raquo;</a></li>
</ul> --}}

      </div>
    </div>
    @yield('content')

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
    <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script> 
    
    

    {{ HTML::Script('maqueta/js/materialize.js'); }}
    {{ HTML::Script('maqueta/js/slider.js'); }}
    {{ HTML::Script('maqueta/js/menu.js'); }}
    {{ HTML::Script('maqueta/js/parallax.js'); }}
    {{ HTML::Script('maqueta/js/carousel.js'); }}
    {{ HTML::Script('maqueta/js/bootstrap.js'); }}
    {{ HTML::Script('maqueta/js/subir.js'); }}
    {{ HTML::Script('maqueta/js/megamenu.js'); }}
    {{ HTML::Script('maqueta/js/modal.js'); }}
    {{ HTML::Script('maqueta/js/abajo.js'); }}
    {{ HTML::Script('maqueta/js/general.js'); }}
    {{ HTML::Script('js/validator/validator.js'); }}
    {{ HTML::Script('js/loadmore_exptravels.js'); }}
    {{ HTML::Script('js/registration_validate.js'); }}
    {{ HTML::Script('js/login_validate.js'); }}
    {{ HTML::Script('js/dropdown.js'); }}
    
    
    
    
    {{ HTML::Script('js/gmaps.js'); }}
    {{ HTML::Script('js/nilson.js'); }}
    {{ HTML::Script('js/search.js'); }}
    {{ HTML::Script('js/fade-message.js'); }}
    {{ HTML::Script('js/jquery.validate.min.js'); }}
    {{ HTML::Script('js/validator/project_payment_validation.js'); }}

 
  </body>
</html>