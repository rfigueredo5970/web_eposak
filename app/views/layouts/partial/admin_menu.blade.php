 
 <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="/esposak/" class="site_title"><i class="fa fa-paw"></i> <span>Eposak</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                        @if(Auth::user())
                            {{ HTML::image('/images/'.Auth::user()->picture,null,array('class' => 'img-circle profile_img'));}}
                        @else
                            <img src="/esposak/images/user.png" alt="..." class="img-circle profile_img">
                        @endif
                        </div>
                        <div class="profile_info">
                            <span>Bienvenido,</span>
                            
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>Acciones</h3>
                            <ul class="nav side-menu">
                                <li><a href="/esposak/admin"><i class="fa fa-home"></i> Inicio <span class=""></span></a>
                                </li>
                                <li><a><i class="fa fa-edit"></i> Crear Nuevo  <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/esposak/users/create">Usuario</a>
                                        </li>
                                        <li><a href="/esposak/articles/create">Artículo</a>
                                        </li>
                                        <li><a href="/esposak/todayesposak/create">Noticia del día</a>
                                        </li>
                                        <li><a href="/esposak/traveltype/create">Tipo de Viaje</a>
                                        </li>
                                        <li><a href="/esposak/page/create">Información de Página</a>
                                        </li>
                                        <li><a href="/esposak/projects/create">Proyecto Eposak</a>

                                        <li><a href="/esposak/travel/create">Viaje Eposak</a>
                                        </li>
                                        <li><a href="/esposak/passenger/create">Pasajero</a>
                                        </li>
                                       <!--  <li><a href="/esposak/collaborator/create">Colaborador</a> -->
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-bar-chart-o"></i> Listas <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/esposak/users">Usuarios</a>
                                        </li>
                                        <li><a href="/esposak/travel">Viajes Eposak</a>
                                        </li>
                                        <li><a href="/esposak/articles">Artículos</a>
                                        </li>
                                        <li><a href="/esposak/articles"">Noticia del día</a>
                                        </li>
                                        <li><a href="/esposak/page">Información de Página</a>
                                        </li>
                                        <li><a href="/esposak/projects">Proyectos Eposak</a>
                                        </li>
                                        <li><a href="/esposak/entrepeneur">Emprendedores Eposak</a>
                                        </li>
                                        <li><a href="/esposak/testimony">Testimonios</a>
                                        </li>
                                        <li><a href="/esposak/destiny">Destinos</a>
                                        </li>
                                        <li><a href="/esposak/passenger">Pasajeros</a>
                                        </li>
                                        <li><a href="/esposak/navbar">Secciones de Página</a>
                                        </li>
                                        <li><a href="/esposak/traveltype">Tipos de Viajes</a>
                                        </li>
                                        <li><a href="/esposak/allies">Aliados</a>
                                        </li>
                                        <li><a href="/esposak/collaborator">Colaboradores</a>
                                        </li>
                                        <li><a href="/esposak/contact">Voluntariados</a>
                                        </li>
                                        <li><a href="/esposak/netting">Redes Sociales</a>
                                        </li>
                                        <li><a href="/esposak/slider">Imágenes de Portada</a>
                                        </li>
                                        <li><a href="/esposak/bottomlink">Botones Finales</a>
                                        </li>
                                    </ul>
                                </li>

                                <li><a><i class="fa fa-bar-chart-o"></i> Listas Terminado <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/esposak/end_project">Proyectos Terminados</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                @if( Auth::user())
                                    {{ HTML::image('/images/'.Auth::user()->picture);}}{{ Auth::user()->user_name; }}
                                    <span class=" fa fa-angle-down"></span>
                                 @else
                                    <img src="/esposak/images/user.png" alt="">
                                    <span class=" fa fa-angle-down"></span>
                                @endif
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="javascript:;">Perfil</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Opciones</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Ayuda</a>
                                    </li>
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i>Salir</a>
                                    </li>
                                </ul>
                            </li>

                            <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="text-center">
                                            <a>
                                                <strong><a href="inbox.html">See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>