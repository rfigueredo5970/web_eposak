    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
          @endif
          @foreach($page as $p)
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <div class="container-fluid">
              <div class="row">
                <div class="col s12 m2 hide-on-med-only hide-on-small-only">
                  <a href="home" class="brand-logo">{{ HTML::image('/images/'.$p->logo,null,array('class' => 'center-block')); }}</a>
                </div>
                <div class="col s12 m2 hide-on-large-only ">
                  <a href="home" class="brand-logo ">{{ HTML::image('/images/'.$p->logo,null,array('class' => 'center-block')); }} </a>
                </div>
                <div class="col s7 m8">
                  <ul class="right hide-on-med-and-down">
                    <li><a href="esposak" class="verdeoscuro">{{$p->button_1_es}} </a></li>
                    <li><a href="comunidades" class="verdeclaro">{{$p->button_2_es}} </a></li>
                    <li><a href="fundacion" class="carne">{{$p->button_3_es}}</a></li>
                    <li><a href="contacto" class="carne2">{{$p->button_4_es}}</a></li>
                    @if(Auth::user())
                      <li><a href="#" class="naranja opcionmmenu regisuser">{{$p->button_6_es}}</a></li>
                    @else
                      <li><a href="#" class="naranja opcionmmenu regisuser">{{$p->button_5_es}}</a></li>
                    @endif
                      <li class="buscajs cerrarmegamenu"><a href="#" class="rojo"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                  </ul>
                </div>

                <!-- ESTO ESTA DANDO ERROR -->
                <div class="col s3 m2 redesnav">
                  <ul class="left hide-on-med-and-down" style="margin-top: -12px;">
                    @foreach($principal_netting as $p_n )
                      <li><a href="{{$p_n->cuenta}}"><i class="{{$p_n->icon}}" aria-hidden="true"></i></a></li>
                    @endforeach
                  </ul>
                </div>

                   <!-- ESTO ESTA DANDO ERROR -->

                <div class="idiomanav">
                  <ul class="hide-on-med-and-down">
                    <li>
                      <p>
                        {{-- <a href="" class="idiomafuera">EN</a> --}}
                        <span style="color=""> / </span>
                        <a href="" class="idiomadentro"> ES</a>
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!--/*::::::::::::::::PARTE MOVIL::::::::::::::::*/-->
            <ul class="side-nav" id="mobile-demo">
              <li><a href="eposak">{{$p->button_1_es}}</a></li>
              <li><a href="comunidades">{{$p->button_2_es}}</a></li>
              <li><a href="fundacion">{{$p->button_3_es}}</a></li>
              <li><a href="contacto">{{$p->button_4_es}}</a></li>
              <li><a href="#" class="opcionmmenu">{{$p->button_5_es}}</a></li>
              <li class="buscajs"><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
              <li><p><a href="">EN</a><a href="">ES</a></p></li>
            </ul>
          @endforeach
        </div>
      </nav>
    </div>