@extends('layouts.master')




  @section('content')
<!--:::::::::::::::FIN RESULTADO:::::::::::::::-->
  <!--:::::::::::::::INICIO VER VIDEO fullscreen:::::::::::::::-->
  <!--::::::INICIO SLIDER::::::-->
     <section class="slidepo">
    {{--  @if(Session::has('messages'))
      <p class="btn" style="float:left;position: absolute; z-index:10000;">{{ Session::get('messages') }}</p>
    @endif --}}
          <div class="slider fullscreen">
        <ul class="slides">
        {{--*/ $isFirst = true; /*--}}
        @foreach($slider as $s)  
          <li>
           {{ HTML::Image('/images/'.$s->url) }}
            {{{ $isFirst ? ' <div class="caption center-align">
              <p class="hide-on-med-only hide-on-large-only ver-video"><i class="fa fa-play-circle-o" aria-hidden="true"></i> VER VIDEO</p>
            </div> ' : '' }}}
            {{--*/ $isFirst = false; /*--}}
            <div class="caption center-align">
            </div> 
          </li>
        @endforeach
        </ul>

          <div class="sl-video hide-on-med-and-down">
              <a href="#modal1" class="modal-trigger">{{ HTML::image('maqueta/img/vervideo.png'); }}</a>
          </div>
          
          
              <center class="eligedestino">
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto porro, totam? Beatae cupiditate</h3>
                 <div class="col-1 valign-wrapper">
                   <p class="valign center-block">ELIGE TU DESTINO</p>   
                 </div>
                 <div class="col-2 valign-wrapper">
                     <p class="valign">DESTINO</p>
                 </div>
                 <div class="col-3 valign-wrapper">
                     <i class="fa fa-angle-down valign center-block"></i>
                 </div>                  
                      <div class="eligedestino2" style="display:none;">
                 <div class="col-1 valign-wrapper"></div>
                 <div class="col2">
                   @foreach($destiny as $d)
                       <a href= {{ route('destino',$d->id ) }}><p style="text-transform: uppercase;">{{$d->name_es}}</p></a>                     
                   @endforeach
                 </div>
                 <div class="col-3 valign-wrapper" style="border: none !important;"></div> 
              </div>        
              </center>          
          <center>
          <a href="#home" id="down" class="down-dos">
              <!--<h3>VER MÁS INFORMACIÓN</h3>-->
              <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          </center>
          </div>
      </section>
      <!--::::::FIN SLIDER::::::-->
      <!--:::::::INICIO BOTON SUBIR:::::::-->
<section class="btn-subir">
    <a href="">
        <h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
        <p>INICIO</p>
    </a>

    
</section>
<div id="home"></div> <!--/*salto de seccion*/-->
 <!--:::::::::::FIN BOTON SUBIR:::::::::::-->
<!--::::::::::INICIO MODAL VIDEO::::::::::-->
  <div id="modal1" class="modal modal-index">
    <div class="modal-p">
      <iframe width="100%" height="500" src="https://www.youtube.com/embed/xbsQWq8nSUg" frameborder="0" allowfullscreen class="framev"></iframe>
    </div>
  </div>
<!--:::::::::::FIN MODAL VIDEO:::::::::::-->
  <section class="home">     
      <div class="container">
          <div class="row">
              <div class="col s12">
                @foreach($travel_type_exp as $t_t_e)

                  <h1>{{$t_t_e->name_es}}<spam><a href="viajes/{{$t_t_e->slug}}">VER MÁS</a></spam></h1>
                  <p>{{$t_t_e->description_es}}</p>
                @endforeach
              </div>
          </div>
        </div>
         <div class="container-fluid">
          <div class="row">
              <div class="col s12">
              <!--:::::::::INICIO CARROUSEL:::::::::-->
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                 <div class="col s12 m8 offset-m2">
                       <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                {{--*/ $isFirst = true; /*--}}
                  @foreach($travel_exp as $t_e)
                    <div class="item{{{ $isFirst ? ' active' : '' }}}">
                      <!--/*:::::CONTENIDO CARROUSEL:::::*/-->
                <div class="comunidad-inter">
                    <div class="col s12 padding0 viaex" style="background-image:url({{ asset('/images/'.$t_e->multimedias[0]->name)}})">
                        <h1>{{$t_e->title_es}} <br>strip steak. Chicken pork loin</h1>
                        <h2>
                          @if($t_e->destiny->id == 2)
                          <i class="fa fa-map-marker" aria-hidden="true"></i>{{$t_e->destiny->name_es}}
                          @elseif($t_e->destiny->id == 3)
                          <i class="fa fa-map-marker" aria-hidden="true"></i>{{$t_e->destiny->name_es}}
                          @endif

                        </h2>
                        <div class="verdeprecio">
                            <p>PRECIO BS:{{$t_e->prices}}</p>
                        </div>
                        <div class="carnedia">
                            <p>{{$t_e->days}} DÌAS </p>
                        </div>
                    </div>
                </div>
                <!--:::::FIN CONTENIDO CARROUSEL:::::-->
                    </div>
                    {{--*/ $isFirst = false; /*--}}
                    @endforeach
                  </div>
                     
                 </div>

                  <!-- Controls -->
                  <a class="left carousel-control hide-on-small-only" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
                        <i class="fa fa-angle-left flecha" aria-hidden="true"></i>
                    </span>
                  </a>
                  <a class="right carousel-control hide-on-small-only" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
                        <i class="fa fa-angle-right flecha" aria-hidden="true"></i>
                    </span>
                  </a>
                </div>             
              </div>
          </div>
      </div>
  </section>
   <section class="home grish">     
      <div class="container">
          <div class="row">
              <div class="col s12">
                @foreach($travel_type_vol as $t_t_v)

                  <h1>{{$t_t_v->name_es}}<spam><a href="viajes/{{$t_t_v->slug}}">VER MÁS</a></spam></h1>
                  <p>{{$t_t_v->description_es}}</p>
                @endforeach
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col s12">
              <!--:::::::::INICIO CARROUSEL:::::::::-->
              <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
                 <div class="col s12 m8 offset-m2">
                       <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                {{--*/ $isFirst = true; /*--}}
                  @foreach($travel_vol as $t_v)
                    <div class="item{{{ $isFirst ? ' active' : '' }}}">
                      <!--/*:::::CONTENIDO CARROUSEL:::::*/-->
                <div class="comunidad-inter">
                    <div class="col s12 padding0 viaex" style="background-image:url({{ asset('/images/'.$t_v->multimedias[0]->name)}})">
                        <h1>{{$t_v->title_es}} <br>strip steak. Chicken pork loin</h1>
                        <h2>

                          @if($t_v->destiny->id == 2)
                          <i class="fa fa-map-marker" aria-hidden="true"></i>{{$t_v->destiny->name_es}}, EDO.MIRANDA
                          @else
                          <i class="fa fa-map-marker" aria-hidden="true"></i>{{$t_v->destiny->name_es}}, EDO.BOLIVAR
                          @endif

                        </h2>
                        <div class="verdeprecio">
                            <p>PRECIO BS:{{$t_v->prices}}</p>
                        </div>
                        <div class="carnedia">
                            <p>{{$t_v->days}} DÌAS </p>
                        </div>
                    </div>
                </div>
                <!--:::::FIN CONTENIDO CARROUSEL:::::-->
                    </div>
                    {{--*/ $isFirst = false; /*--}}
                    @endforeach
                  </div>
                     
                 </div>

                  <!-- Controls -->
                  <a class="left carousel-control hide-on-small-only" href="#carousel-example-generic2" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
                        <i class="fa fa-angle-left flecha" aria-hidden="true"></i>
                    </span>
                  </a>
                  <a class="right carousel-control hide-on-small-only" href="#carousel-example-generic2" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
                        <i class="fa fa-angle-right flecha" aria-hidden="true"></i>
                    </span>
                  </a>
                </div>             
              </div>
          </div>
      </div>
  </section>
  <section class="home padding0">
      <div class="full-width">
          <div class="row">
              <div class="col s12 m12 l6 padding0">
                  <div class="imgnoti" style="background-image:url({{ asset('/images/'.$articles->image)}})"></div>
              </div>
              <div class="col s12 m12 l6">
                  
                  <h2>{{$articles->title_es}}<a href="fundacion"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></h2>
                  <h6>{{$articles->created_at}} </h6>
                  <h3>Bacon ipsum dolor amet tongue turkey pork chop, strip steak picanha</h3>
                  <p style="word-wrap: break-word;" >{{$articles->description_es}} </p>
                  <h1><spam><a href="articulo/{{$articles->id}}">VER MÁS</a></spam></h1>
              </div>
          </div>
      </div>
  </section>
  <section class="home">     
      <div class="container">
          <div class="row">

              <div class="col s12">
                  <h1>al día con eposak</h1>
              </div>
          </div>
      </div>
  <div class="container-fluid">
	<div class="row">
            <!-- Carousel
            ================================================== -->            
            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner container">
                  {{--*/ $isFirst = true; /*--}}
                  @foreach($today as $t)
                    <div class="item{{{ $isFirst ? ' active' : '' }}}">
                        <div class="row">
                        @foreach($t as $i )
                          <div class="col s4 hide-on-small-only notiarriba" id="{{$i->id}}">
                              <div style="background-image:url({{ asset('/images/'.$i->image)}})"  class="img-dia-cerrado" id="q{{$i->id}}">
                              </div>
                              <div class="dia-cerrado" id="u{{$i->id}}">
                                  <h2 id="f{{$i->id}}" style="display:none;">{{$i->created_at}}</h2>
                                  <h1>{{$i->title_es}} </h1>
                                  <p id="p{{$i->id}}" style="display:none;">{{$i->description_es}}</p>
                                  <a href="noticia/{{$i->id}}"><h1><span>VER MÁS</span></h1></a>
                              </div>
                          </div>
                          @endforeach
                        </div>

                    </div>
                    {{--*/ $isFirst = false; /*--}}
                  @endforeach
                </div>
                     <!-- Controls -->
                  <a class="left carousel-control hide-on-small-only" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
                        <i class="fa fa-angle-left flecha" aria-hidden="true"></i>
                    </span>
                  </a>
                  <a class="right carousel-control hide-on-small-only" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
                        <i class="fa fa-angle-right flecha" aria-hidden="true"></i>
                    </span>
                  </a>
                             
            </div><!-- End Carousel --> 
        
    </div>
</div>
  </section>


<!--::::::::::MODAL VIDEO::::::::::-->
<!-- Modal Trigger -->
  <!--<a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>-->

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div>
<!--::::::::::MODAL VIDEO::::::::::-->
<!--:::::::INICIO BOTON SUBIR:::::::-->
<section class="btn-subir">
    <a href="">
        <h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
        <p>SUBIR</p>
    </a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer>
@stop