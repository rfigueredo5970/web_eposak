@extends('layouts.master')

  @section('content')
  <input   id="long-map" type="hidden" name="" value="{{$location_use->lng}}">
  <input   id="lat-map" type="hidden" name="" value="{{$location_use->lat}}">
    <!--::::::::::INICIO FUNDACION EPOSAK::::::::::-->

    <section class="contacto">
       <div class="contacto-banner" style="background-image:url({{ asset('/images/'.$navbar->image)}})">
            <div class="container">
                <div class="row">
                       <div class="col s12">
                            <h1>{{$navbar->name}}</h1>
                       </div>
                </div>
            </div>
        </div>
    </section>
     <section class="formviaje" style="margin-top: 26px;">
    @if(Session::has('msg'))
      <p class="btn" style="float:left;position: absolute;">{{ Session::get('msg') }}</p>
    @endif
    @if(Session::has('$validate'))
      @foreach($validate as $e)
        <p class="btn" style="float:left;position: absolute;">{{ Session::get('$e') }}</p>
      @endforeach
    @endif
      {{Form::open(array('url' => 'contact','method'=>'post', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}
          <div class="formcontacto">
              <div class="container">
                 <div class="row">
                     <div class="col s12 offset-m3 m6">
                        <div class="ttl-contac valign-wrapper">
                             <h1 class="valign"><i class="{{$contacto_section->icono}}" aria-hidden="true"></i> {{$contacto_section->titulo}}</h1>
                        </div>
                        <h4>{{$contacto_section->descripcion}}</h4>
                    </div>
                 </div>
                    <form action="{{url('contact')}}" method="POST">
                    <div class="row">
                          <div class="col s12 offset-m3 m6">
                           <div class="row">
                              <div class="input-field-4 col s12">
                                @if(Auth::user())
                                  <input id="first_name" name="name" type="text" class="validate" tabindex="1" value="{{ $voluntary->firstname}} ">
                                @else
                                  <input id="first_name" name="name" type="text" class="validate" tabindex="1">
                                @endif
                                <label for="first_name" class="label-epo-2">{{$contacto_section->input1}}</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="input-field-4 col s12">
                                @if(Auth::user())
                                  <input id="email" name="email" type="email" class="validate" tabindex="5" value="{{ $voluntary->email}}">
                                @else
                                  <input id="email" name="email" type="email" class="validate" tabindex="5">
                                @endif
                                <label for="email" class="label-epo-2">{{$contacto_section->input2}}</label>
                              </div>
                            </div>
                          </div>
                    </div>
                    <div class="row">
                       <div class="col s12 offset-m3 m6">
                            <div class="input-field-3">
                              <textarea id="textarea1" name="message" class="materialize-textarea textarea-2" tabindex="9"></textarea>
                              <label for="textarea1" class="textarea-label-2">{{$contacto_section->textarea}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 offset-m3 m6">
                            {{Form::submit($contacto_section->btn,array('class' => 'btn waves-effect waves-light btnenviar fa fa-check-circle-o" onclick="Materialize.toast("Su mensaje fue enviado con éxito", 4000)')) }}
                        </div>
                    </div>
                  </form>
                  <div class="row">
                      <div class="col s12 offset-m3 m6">
                          <h4>Puedes comunicarte con nosotros escribiendo a {{$follow_use->email}} o llamando al número {{$follow_use->phone_number}}</h4>
                    
                          <h2>síguenos en:</h2>
                          <ul>
                              @foreach($other_netting as $n)
                                <li><a href="{{$n->cuenta}}"><i class="{{$n->icon}}" aria-hidden="true"></i></a></li>
                              @endforeach
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      
      <section class="conta-map">
          <div class="container">
              <div class="row">
                  <div class="col s12 offset-m3 m6">
                      <h1>ubicación</h1>
                      <p>{{$location_use->address}}</p>
                  </div>
                  <div class="row">
                      <div style="height:400px" class="col-md-12">
                          <div id="map" style="width: 100%; height: 100%;"></div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
    <!--:::::::::::INICIO BANNER FOOTER:::::::::::-->
    <section class="comunidad">
            <div class="feposak-contac valign-wrapper" style="background-image:url({{ asset('/images/'.$bottom->image)}})">
               <div class="row">
                    <div class="col s12 m8 valign">
                        <p>{{$bottom->description_es}}</p>
                    </div>
                    <div class="col s12 m4 valign">
                        <a href="{{$bottom->link}}"><img src= {{ url("/maqueta/img/voluntariado.png") }} alt="" class="responsive-img center-block"></a>
                    </div>
                </div>    
            </div>
    </section>
    <!--::::::::::::::INICIO BOTON SUBIR::::::::::::::-->
    <section class="btn-subir">
        <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
        <p>SUBIR</p></a>
    </section>
    <!--:::::::::::::FOOTER:::::::::::::-->
    <footer>
        <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
    </footer>

  @stop