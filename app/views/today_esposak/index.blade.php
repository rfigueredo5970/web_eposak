@extends('layouts.master-admin')

@section('content')
           	    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Today Esposak <small> noticias del dia</small></h2>
                                    <div class="clearfix"></div>	
                                </div>
                                @foreach( $today as $t)
                                    <div class="col-md-55">
                                        <div class="thumbnail">
                                            <div class="image view view-first">
                                                {{ HTML::Image('/images/'.$t->image, null ,array('style' => 'width: 100% ;display: block')) }}
                                                <div class="mask no-caption">
                                                    <div class="tools tools-bottom">
                                                        <a href="/esposak/todayesposak/{{ $t->id }}/edit"><i class="fa fa-pencil"></i></a>
                                                		<a href="todayesposak/{{ $t->id }}">ir</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption">
                                                <p><strong>{{ $t->title_es }}</strong>
                                                </p>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
								@endforeach
                            </div>
                        </div>
                </div>
@stop