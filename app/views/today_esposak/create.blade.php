@extends('layouts.master-admin')

@section('content')
  <div class="x_panel">
    <div class="x_title">
        <h2>Nueva noticia <small>different form elements</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="/esposak/todayesposak" class="btn">volver</a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="display: block;">
        <br>
        {{Form::open(array('url' => 'todayesposak', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Título es<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('title_es',null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('title_es') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Título en<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::text('title_en',null,array('class' => 'form-control')) }}
                    <div style="color:red;" class="error">{{ $errors->first('title_en') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción es </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {{Form::textArea('description_es',null,array('class' => 'form-control' , 'style' => 'resize: none;')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                    <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                </div>
            </div> 
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción en</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::textArea('description_en',null,array('class' => 'form-control' , 'style' => 'resize: none;')) }}<ul class="parsley-errors-list" id="parsley-id-8061"></ul>
                   <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Imagen</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {{Form::file('image') }}<br/>
                   <div style="color:red;" class="error">{{ $errors->first('file') }}</div>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    
                    {{Form::submit('guardar',array('class' => 'btn btn-success')) }}
                </div>
            </div>

        {{Form::close() }}
    </div>
</div>
@stop