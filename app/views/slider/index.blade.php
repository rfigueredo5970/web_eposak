@extends('layouts.master-admin')

@section('content')
           	 <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                    <ul class="nav navbar-right panel_toolbox">
                                        <a class="btn btn-primary" href="slider/create">Agregar Imagen</a>
                                    </ul>
                                <div class="x_title">
                                    <h2>Imagenes de Pagina Principal<small> listado de imagenes</small></h2>
                                    
                                    <div class="clearfix"></div>	
                                </div>
                                    @foreach($slider as $s)
                                        <div class="col-md-55">
                                            <div class="thumbnail">
                                                <div class="image view view-first">
                                                    {{ HTML::Image('/images/'.$s->url, null ,array('style' => 'width: 100% ;display: block')) }}
                                                    <div class="mask no-caption">
                                                        <div class="tools tools-bottom">
                                                            <!--<a href="/esposak/slider/{{ $s->id }}/edit"><i class="fa fa-pencil"></i></a>-->
                                                    		<a href="slider/{{ $s->id }}">Ver</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="caption">
                                                    <p><strong>{{ $s->name }}</strong>
                                                    </p>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
									@endforeach

                                    </div>

                                </div>
                            </div>
                            @stop