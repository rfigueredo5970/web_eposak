@extends('layouts.master-admin')

@section('content')
        			<div class="x_panel" >
                        <div class="x_title">
                            <h2>{{ $slider->name }} </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/slider" class="btn" ><i>Back</i></a>
                                </li>
                                <li>
                                    {{Form::open(array('url'=> 'articles/'.$slider->id, 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>{{ $slider->name }} </h1><br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="text-center col-md-12" >
                                        {{ HTML::image('/images/'.$slider->url,null,array('style' =>'width: 100%'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop