@extends('layouts.master-admin')

@section('content')
<div class="x_panel" style="height:600px;">
    <div class="x_title">
        <h2>Pasajero: ' {{ $passenger->name }} '</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="/esposak/passenger" class="btn" ><i>Back</i></a>
            </li>
            <li  >
                {{ Form::open(array('url'=> 'passenger/'.$passenger->id, 'method' => 'delete')) }}
                {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                {{Form::close()}}
            </li>
        </ul>
        <div class="clearfix"></div>
        <div class="container well text-center " >    
            <h1>{{ $passenger->name }} </h1><br>
            <h3>{{ $passenger->email }}</h3><br>
            <br>
            <div class="row container-fluid " >
                <div class="col-md-6" >
                    <h6 style="opacity:0.50;">-----------------Numero Telefonico-----------------</h6>
                    <p style="word-wrap: break-word;" >{{ $passenger->phone_number }}</p>
                    <h6 style="opacity:0.50;">-----------------Viaje-----------------</h6>
                    <p style="word-wrap: break-word;" >{{$passenger->travels->title_es}}</p>
                </div>
                <div class="text-center col-md-4 row" >
                    <h6 style="opacity:0.50;">-----------------Estado del Pasajero-----------------</h6>
                    <p style="word-wrap: break-word;" >{{$passenger->status}}</p>
                </div> 
            </div>
        </div>
    </div>
</div>
@stop