@extends('layouts.master-admin')

@section('content')
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Pasajeros Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="passenger/create">Agregar nuevo Pasajero </a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Nombre</th>
                                            <th class="column-title">status</th>
                                            <th class="column-title">Viaje</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($passenger as $pasajero)
                                        <tr class="even pointer">
                                            <td class=" ">{{$pasajero->id}}</td>
                                            <td class=" ">{{$pasajero->name}}</td>
                                            <td class=" ">{{$pasajero->status}}</td>
                                            <td class=" ">{{$pasajero->travels->title_es}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="passenger/{{ $pasajero->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/passenger/{{ $pasajero->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop