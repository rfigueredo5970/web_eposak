@extends('layouts.master')

@section('content')
<!--:::::::::::::::FIN RESULTADO:::::::::::::::-->
<!--::::::::::INICIO COMUNIDAD KAMARATA::::::::::-->
<input   id="long-map" type="hidden" name="" value="{{$travel->destiny->lng}}">
<input   id="lat-map" type="hidden" name="" value="{{ $travel->destiny->lat}}">
<section class="individual">
  
   <div class="principalimg" style="background-image:url({{ asset('/images/'.$section->image)}})" >
        <div class="container">
            <div class="row">
                   <div class="col s12">
                        <a href= {{ URL::previous() }}><h6><i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR</h6></a>
                   </div>
            </div>
            <div class="row">
                   <div class="col s12">
                        <h1>{{$travel->title_es}}</h1>
                   </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col s12 m7">
                <div class="col s12">
                    <div class="titulogaleria z-depth-1">
                        <h1><i class="fa fa-camera" aria-hidden="true"></i> galería</h1>
                    </div>
                </div>
                <div class="col s12">
                   
                   
                   
                   {{-- <div class="gvideo">
                       <iframe width="100%" height="" src={{url("https://www.youtube.com/embed/xbsQWq8nSUg")}} frameborder="0" allowfullscreen class="framev"></iframe>
                   </div> --}}

                  <div class="galeimg" id="big-img" style="background-image:url({{ asset('/images/'.$travel->multimedias[rand(0,sizeof($travel->multimedias)-1)]->name)}})"></div>
                       
                   
                   
                </div>
                <div class="col s12">
                    
                  <div id="scroll" class="flexcroll">
                    <ul>
                    @foreach($travel->multimedias as $t)
                        <li class="relative selectImg" style="cursor:pointer;" data-img="{{$t->name}}">
                                <div class="expand">
                                    <i class="fa fa-expand" aria-hidden="true"></i>
                                </div>
                                {{ 
                                  HTML::Image('/images/'.$t->name, null ,array('class' => 'responsive-img center-block', 'style' => 'width: 150px ;display: block;     height: 103px;')) 
                                                    }}
                        </li>
                        
                    {{--     <li class="relative">
                                <div class="expand">
                                    <i class="fa fa-expand" aria-hidden="true"></i>
                                </div>
                                <img src= {{ url("img/miniatura.jpg") }} alt="" class="responsive-img center-block">
                        </li>
                        <li class="relative">
                                <div class="expand">
                                    <i class="fa fa-expand" aria-hidden="true"></i>
                                </div>
                                <img src= {{ url("img/miniatura.jpg") }} alt="" class="responsive-img center-block">
                        </li> --}}
                      @endforeach
                    </ul>
                </div>
                    
                </div>
                <div class="col s12 l4 fondo-descrip">
                    <h1>recomendaciones</h1>
                    <ul>
                        <li>{{$travel->recommendation_es}}</li>
                        <li></li>
                    </ul>
            </div>
            <div class="col s12 l4 fondo-descrip">
                    <h1>inluye</h1>
                    <ul>
                        <li>{{$travel->include_es}}</li>
                        <li></li>
                    </ul>
            </div>
            <div class="col s12 l4 fondo-descrip">
                    <h1>no incluye</h1>
                    <ul>
                        <li>{{$travel->dont_include_es}}</li>
                        <li></li>
                    </ul>
            </div>
            </div>
            <!--::::::::::::::::COLUMNA IZQUIERDA::::::::::::::::-->
            <div class="col s12 m5">
                <div class="col s12">
                    <div class="titulodias z-depth-1">
                        <h1>{{$travel->days}} DÍAS {{$travel->nights}} NOCHES</h1>
                    </div>
                    <div class="col s12">
                        <h3><strong>COSTO:</strong> {{$travel->prices}} (BS/$) P/P <span>({{count($pasajero)}} PERSONAS)</span></h3>
                    </div>
                    <div class="col s12">
                        <p>{{$travel->description_es}}</p>
                    </div>
                </div>
                <div class="col s6 m12  l6">
                    <p>Compartir en:</p>
                    <ul>

                        @foreach($secondary_netting as $p_n )
                      
                            <li><a target= "_blank" href={{$p_n->cuenta}} ><i class ="{{ $p_n->icon }} "   aria-hidden="true"></i></a></li> 

                        @endforeach
                    </ul>
                </div>
                <div class="col s6 m12 l6">
                    <a class="waves-effect waves-light btn" href= {{ route('form_viaje', $travel->id)}}><i class="fa fa-check-circle-o" aria-hidden="true"></i> RESERVA</a>
                </div>
                <div class="row" > 
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                         {{--*/ $isFirst = true; /*--}}
                            @foreach($itinerary->groupBy('day') as $iti)
                                <div class="item{{{ $isFirst ? ' active' : '' }}}">
                                    <div class="col s12">
                                       <div class="titulointine z-depth-1">
                                            <h1><i class="fa fa-list-ol" aria-hidden="true"></i> ITINERARIO</h1>
                                        </div>    
                                        <div class="itinerario">
                                            <h1>día {{$iti[0]->day}}<!--: “Comienza La Aventura”--></h1>
                                            @foreach($iti as $i)
                                                <ul>
                                                    <li>{{ date('h:i a',strtotime($i->start_at))}} <span>{{$i->description_es}}</span></li>
                                                </ul>
                                            @endforeach
                                        </div> 
                                    </div>
                                </div>
                                 {{--*/ $isFirst = false; /*--}}

                            @endforeach
                        </div>
                    </div>
                </div>
                <center>
                      <ul class="pagination" style="display: -webkit-inline-box;" >
                        <li class="waves-effect" ><a role="button" data-slide="prev"  href="#carousel-example-generic"><i class="material-icons">chevron_left</i></a></li>

                        @for ($i = 1; $i <= sizeof($itinerary->groupBy('day')); $i++)
                          <li class="waves-effect" data-target="#carousel-example-generic" data-slide-to="{{ $i -1 }}" class=""><a href="#!">{{ $i }}</a></li>
                        @endfor
                        <li class="waves-effect"><a href="#carousel-example-generic" role="button" data-slide="next"><i class="material-icons">chevron_right</i></a></li>
                      </ul>
                </center>
                <div class="col s12">
                  <div style="height:200px" class="col-md-12">
                      <div id="map" style="width: 100%; height: 100%;"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--/*::::::::INICIO TESTIMONIO::::::::*/-->
<section class="testimonio">
    <div class="container">
        <div class="row">
            <h1>TESTIMONIOS</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et natus rerum ducimus impedit repellendus, quasi sint suscipit enim tempore iste alias explicabo eaque in non labore, eum earum voluptate sequi? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium libero, vero fuga animi accusamus. Eum voluptas amet ratione voluptate fugit? Unde sit tempora laudantium veritatis ipsum quod ad commodi? Neque. <span>Nombre Aprellido</span></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, possimus expedita ipsam ex alias nisi optio, quo, dolores magni illo minima libero ratione, molestias magnam. Rerum inventore labore a perspiciatis. <span>Nombre y Apellido</span></p>
        </div>
    </div>
</section>
<!--/*::::::::FIN TESTIMONIO::::::::*/-->




<section class="btn-subir">
    <a href=""><h1 class="fa fa-chevron-up" aria-hidden="true"></h1>
    <p>SUBIR</p></a>
</section>
<!--:::::::::::::FOOTER:::::::::::::-->
<footer>
    <p>Eposak.org - Todos los derechos reservados. J-40113589-7 Desarrollado por <a href="">COMWARE.DIGITAL</a></p>
</footer>
@stop