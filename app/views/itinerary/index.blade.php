@extends('layouts.master-admin')

@section('content')
            <div class="" role="main">
               	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Destinos Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="itinerary/create">Crear nuevo Destino</a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Viaje (Es)</th>
                                            <th class="column-title">fecha</th>
                                            <th class="column-title">fecha y hora de comenzar</th>
                                            <th class="column-title">fecha y hora de finalizar</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($itinerary as $i)
                                        <tr class="even pointer">
                                            <td class=" ">{{$i->id}}</td>
                                            <td class=" ">{{$i->travel->title_es}}</td>
                                            <td class=" ">{{$i->date}}</td>
                                            <td class=" ">{{$i->start_at}}</td>
                                            <td class=" ">{{$i->end_at}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="itinerary/{{ $i->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/itinerary/{{ $i->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @stop