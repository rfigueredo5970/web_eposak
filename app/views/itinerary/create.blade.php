@extends('layouts.master-admin')

@section('content')
              <div class="x_panel">
                                <div class="x_title">
                                    <h2>Form Design <small>different form elements</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="/esposak/itinerary" class="btn">Back</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                    {{Form::open(array('url' => 'itinerary','route'=>'post', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}
                                        <!--{{print_r($travel)}}-->
                                        <div class="form-group filter">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Viaje<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="travel_id" class="form-control" onchange="getval(this);" >
                                                    <option value="">
                                                           SELECCIONE EL VIAJE
                                                    </option>
                                                    @foreach ($travel as $t)
                                                        <option value="{{$t->id}}">
                                                           {{$t->title_es}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div style="color:red;" class="error">{{ $errors->first('travel_id') }}</div>
                                            </div>
                                            </div>

                                        <div class="form-group filter">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fechas de viaje<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input disabled="disabled" id="traveldate" type="text" name="date" class="form-control" value="" >
                                            </div>
                                        </div>

                                        <div class="form-group filter">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Seleccione el dia del Viaje<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="date" type="date" name="date" class="form-control" value="" min="" max="" >
                                               <div style="color:red;" class="error">{{ $errors->first('date') }}</div>
                                            </div>
                                        </div>


                                        <div class="form-group filter">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Comienza<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="time" name="start_at" class="form-control" >
                                               <div style="color:red;" class="error">{{ $errors->first('start_at') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group filter">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Termina<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="time" name="end_at" class="form-control" >
                                               <div style="color:red;" class="error">{{ $errors->first('end_at') }}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               {{Form::textarea('description_es',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                               <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                                               
                                            </div>
                                        </div>


                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                            </div>
                                        </div>
                                    {{Form::close() }}
                                </div>
                            </div>
                            <script>
                                
                                $('#some_id').on('change', function() {
                                 alert('#some_id')//alert( this.value ); // or $(this).val()
                                });

                            </script>

                            <script type="text/javascript">
                                function getval(sel) {
                                   console.log(sel.value); 
                                   $.get("/esposak/datetravel/"+ sel.value, function(data, status){
                                        console.log(data)
                                        console.log(data.start_at);
                                        console.log(data.end_at);
                                        $('#date').attr({
                                            "min":data.start_at,
                                            "max":data.end_at
                                        });
                                        console.log(date);
                                        $('#traveldate').attr({
                                            "value":"Desde "+data.start_at+" Hasta "+data.end_at
                                        });
                                        $(":disabled").css("background-color", "#eeeeee");
                                    });
                                }
                            </script>
                            @stop
