@extends('layouts.master-admin')

@section('content')
                
                <!-- /top tiles -->
        			<div class="x_panel" style="height:600px;">
                        <div class="x_title">
                            <h2>itinerario: "{{$itinerary->date}} "</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a href="/esposak/itinerary" class="btn" ><i>Back</i></a>
                                </li>
                                <li  >
                                    {{Form::open(array('url'=> 'itinerary/'.$itinerary->id , 'method' => 'delete')) }}
                                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                                    {{Form::close()}}    
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="container well text-center " >    
                                <h1>Desde {{ $itinerary->start_at }} </h1><br>
                                <h3 style="margin-top:-10px;" >Hasta {{ $itinerary->end_at }} </h3>
                                <br>
                                <br>
                                <div class="row container-fluid " >
                                    <div class="col-md-6" >
                                        <h6 style="opacity:0.50;">-----------------Español-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $itinerary->description_es }}</p>
                                        <h6 style="opacity:0.50;">-----------------Ingles-----------------</h6>
                                        <p style="word-wrap: break-word;" >{{ $itinerary->description_en }}</p>
                                    </div>
                                </div>
                                <div class="row container-fluid " >
                                    <div class="text-center col-md-3" >
                                        <h6 style="opacity:0.50;">-----Viaje-----</h6>
                                        <p style="word-wrap: break-word;" >{{ $itinerary->travel->title_es}}</p>
                                        <h6 style="opacity:0.50;">-----Travel------</h6>
                                        <p style="word-wrap: break-word;" >{{ $itinerary->travel->title_es }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop