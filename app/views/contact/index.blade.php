@extends('layouts.master-admin')

@section('content')
            <!-- page content -->
            <div class="" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel container">
                            <div class="x_title">
                                <h2>Voluntariados Esposak!<small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                   <a class="btn btn-success" href="contact/create">Agregar nuevo Voluntario</a>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content  ">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Voluntariado</th>
                                            <th class="column-title">Correo Electronico</th>
                                            <th class="column-title"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($contact as $c)
                                        <tr class="even pointer">
                                            <td class=" ">{{$c->id}}</td>
                                            <td class=" ">{{$c->name}}</td>
                                            <td class=" ">{{$c->email}}</td>
                                            <td class=" last">
                                                <a class="btn btn-info btn-block" href="contact/{{ $c->id }}">Ver</a>
                                                <br>    
                                                <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/contact/{{ $c->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @stop   