@extends('layouts.master-admin')

@section('content')
            <!-- page content -->
            <div class="" role="main">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Voluntariado<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a href="/esposak/contact" class="btn">Back</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <br>
                        {{Form::open(array('url' => 'contact','route'=>'post', 'files' => true, null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('name',null,array('class' => 'form-control')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('name') }}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Correo Electronico<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{Form::text('email',null,array('class' => 'form-control')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('email') }}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Mensaje<span class="required">(Opcional)</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   {{Form::textarea('message',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                   <div style="color:red;" class="error">{{ $errors->first('message') }}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion Es <span class="required">(Opcional)</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   {{Form::textarea('description_es',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                   <div style="color:red;" class="error">{{ $errors->first('description_es') }}</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion En<span class="required">(Opcional)</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   {{Form::textarea('description_en',null,array('class' => 'form-control' , 'style' => 'resize: none;','size' => '30x5')) }}
                                   <div style="color:red;" class="error">{{ $errors->first('description_en') }}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">status</label>
                                <div class="col-md-6 col-sm-6    col-xs-12">
                                   {{Form::select('status', array(''=>'','activo' => 'activo', 'inactivo' => 'inactivo'),null,array('class' => 'form-control')) }}
                                    <div style="color:red;" class="error">{{ $errors->first('status') }}</div>
                                </div>
                            </div>



                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    
                                    {{Form::submit('guardar',array('class' => 'btn btn-success btn-block')) }}
                                </div>
                            </div>

                        {{Form::close() }}
                    </div>
                </div>
            </div>
            @stop
      
    