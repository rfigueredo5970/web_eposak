@extends('layouts.master-admin')

@section('content')
                <!-- /top tiles -->
    <div class="x_panel" style="height:600px;">
        <div class="x_title">
            <h2>Voluntario: ' {{ $contact->name }} '</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a href="/esposak/contact" class="btn" ><i>Back</i></a>
                </li>
                <li  >
                    {{Form::open(array('url'=> 'contact/'.$contact->id , 'method' => 'delete')) }}
                    {{Form::submit('Eliminar',array('class'=>'btn btn-danger'))}}
                    {{Form::close()}}    
                </li>
            </ul>
            <div class="clearfix"></div>
            <div class="container well text-center " >    
                <h1>{{ $contact->name }} </h1><br>
                <h3 style="margin-top:-10px;" >{{ $contact->email }} </h3>
                <br>
                <br>
                <div class="row container-fluid " >
                    <div class="col-md-8 col-md-offset-2" >
                        <h6 style="opacity:0.50;">-----------------Mensaje-----------------</h6>
                        <p style="word-wrap: break-word;" >{{ $contact->message }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop