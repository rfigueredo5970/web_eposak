@extends('layouts.master-admin')

@section('content')
               	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>botones y bases legales<small><a class="btn btn-success" href="/esposak/traveltype/create">Crear nuevo</a></small></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">ID </th>
                                            <th class="column-title">nombre español</th>
                                            <th class="column-title">nombre ingles</th>
                                            <th class="column-title"></th>
                                            <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $t_travel as $t_t)
                                        <tr class="even pointer">
                                            <td class=" ">{{$t_t->id }}</td>
                                            <td class=" ">{{$t_t->name_es}}</td>
                                            <td class=" ">{{$t_t->name_en}}</td>
                                            <td class=" last">
	                                            <a class="btn btn-info btn-block" href="/esposak/traveltype/{{ $t_t->id }}">ir</a>
	                                            <br>
	                                            <a style="margin-top:-19px;" class="btn btn-warning btn-block" href="/esposak/traveltype/{{ $t_t->id }}/edit">editar</a>
                                            </td>
                                        </tr>
                                        @endforeach    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @stop