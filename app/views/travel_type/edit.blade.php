@extends('layouts.master-admin')

@section('content')
                <!-- top tiles -->
                <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tipos de Viajes<small>formulario para editar una categoria de viajes</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="btn" href="/esposak/traveltype">back</a> 
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                    <br>
                                   
                                   {{Form::open(array('url' => 'traveltype/' . $t_travel->id, 'method' => 'put', null, 'class' => 'form-horizontal form-label-left;', null,'id' => 'demo-form2' )) }}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre (Es) <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name_es', $t_travel->name_es,array('class' => 'form-control'))}}<ul class="parsley-errors-list" id="parsley-id-7621"></ul>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nombre (En)<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('name_en', $t_travel->name_en,array('class' => 'form-control')) }}  <ul class="parsley-errors-list" id="parsley-id-8218"></ul>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                {{Form::submit('guardar',array('class'=>'btn btn-success btn-block'))}}
                                            </div>
                                        </div>

                                    {{Form::close() }}
                                </div>
                            </div>
                <!-- /top tiles -->
                @stop