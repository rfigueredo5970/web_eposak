@extends('layouts.master-admin')
@section('content')
   	<div class="row">
   		<div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" style="height:600px;">
                <div class="x_title">
                    <h2>Tipo de viaje</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a href="/esposak/traveltype" class="btn"> back</a>
                        </li>
                    </ul>
                    <div class="clearfix">
                    </div>
				</div>
				<h1>
					nombre en español
				</h1>
				<div>
					<h3>{{ $t_travel->name_es }} </h3>
				</div>
				<br>
				<h1>
					nombre en ingles
				</h1>
				<div>
					<h3>{{ $t_travel->name_en }} </h3>
				</div>
           	</div>
		</div>
    </div>
    @stop