<?php

class Pago123Controller extends \BaseController {

function new_collaborator($user, $project, $data)
{
   
   

 if($project)

    {
    $collaborator = new Collaborator; 
    $collaborator->name = $user ->full_name;
    $collaborator->phone_number = $user->phone;
    $collaborator->contribution =  (float) $data["contribution"]; 
    $collaborator->message_es = $data["message_es"];
    $collaborator->message_en = $data["message_en"];
    $collaborator->user_id = $user->id;
    $collaborator->project_id = $project->id;

   
    $collaborator->save();
     
  
    $res = $this->new_project_payment($collaborator->id, $project,$data["contribution"]); 

    
    
    $project_payment = Payments_project::find($res["id"]);  


    $response = array("id" => $collaborator->id,"project_payment_id"  =>  $project_payment->id);

    }else{
        $response = array("id" => false);
    }


    $user->collaborator()->save($collaborator);
    $project_payment->collaborator()->associate($collaborator);
    $collaborator->save();

    return $response;

}


function new_project_payment($collaborator_id, $project, $payment_amount)
{
   
        $project_payment =  new Payments_project;
        $project_payment->payment_amount = (float) $payment_amount;
        $digits = 11;
        $project_payment->confirmation_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $project_payment->status = false;
        $project_payment->collaborator_id = $collaborator_id;
        $project_payment->projects_id = $project->id;
        
        if($project_payment->save()){
             $response = array("status"=> true, "id" => $project_payment->id);
        }else{
          
          $response = array("error"=> "cann't save new project payment on database","status"=>false);
        }


       

        return $response;

}


function update_user($user, $userdata){
  
  $firstname = ucwords(strtolower(trim($userdata["firstname"])));
  $lastname  =  ucwords(strtolower(trim($userdata["lastname"]))); 
  $full_name = $firstname . " " . $lastname;  
  $nationality = strtolower(trim($userdata["nationality"]));
  $document =  trim($userdata["document"]);
  $phone = (int) $userdata["phone"];
                      
                     
  $user->firstname = $firstname;
  $user->lastname = $lastname;
  $user->full_name = $full_name;
  $user->nationality = $nationality;
  $user->document = $document;
  $user->phone= $phone;
  
  $user->save();
  
  return $user->id;
}  


function invoke () {
          
            $data = Input::only("project",
"message_es", "message_en", "contribution", "group1");
  
            $userdata = Input::only("firstname","lastname","nationality","phone","document");
  
           // print_r($userdata); die();

            $message_es = $data["message_es"];
            $message_es = $data["message_en"];
  
            

            if ($data["group1"] == "other" && !empty($data["contribution"]) )
            {

                $data["contribution"] = (float) $data["contribution"];
               
              if(!is_numeric($data["contribution"])){
                
                $data["contribution"] = NULL;
              }

            }elseif( $data["group1"] == "default") {

                  $data["contribution"] = (float) 50.00;  
              
            }else{

               $data["contribution"] = NULL;  
            }

        

            $project = Project::find($data["project"]); 
                  

            $user = Auth::user();
  
            $id = $this->update_user($user,$userdata);
            
            $user= User::find($id);
            
          
            $page = Details_page::all();

            $netting = Netting::all();
            
            $principal_netting = $netting->chunk(3)[1];
            
            $navbar = Section_navbar::find(15);

            $ip =  $_SERVER['REMOTE_ADDR']; 


           $res =  $this->new_collaborator($user,$project,$data);



        
        if ($res["id"]) {
            
            //print_r($user); die();

            $collaborator = Collaborator::find($res["id"]);
            
            $post_url = "http://123pago.net/msBotonDePago/index.jsp";
            
            $post_values = array(
                'nbproveedor' => 'FUNDACION ESTEBAN TORBAR',
                'nb' => $user->firstname,
                'ap' => $user->lastname,
                'cs' => '60d00d8a124e77b41b432a63076112f3',
                'ci' => $user->document,
                'em' => $user->email,
                'nai' => $res["project_payment_id"],
                'co' => "donation",
                'mt' =>  $collaborator->contribution,
                'tl' => $user->phone,
                'ip' => $ip,
                'ancho' => '190px',
            );


               // print_r( $post_values); die(); 
   


            $post_string = '';

            foreach ($post_values as $key => $value) {
                $post_string .= "$key=" . urlencode($value) . "&";
            }

            $post_string = rtrim($post_string, "& ");

            $request = curl_init($post_url);

            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($request, CURLOPT_VERBOSE, 1);
            //curl_setopt($request, CURLOPT_STDERR, fopen('tmp/logs/curlog.log', "w+"));

            //$post_response = null;
            $post_response = curl_exec($request);
            curl_close($request);
            
            //return $post_response;
       

        return View::make('botondepago-proyecto')
                            ->with('page', $page)
                            ->with('user', $user)
                            ->with('project', $project)
                            ->with('navbar', $navbar)
                            ->with('principal_netting', $principal_netting)
                            ->with('botonpago', $post_response)
                            ->with('data',$data);
                            
        }
        
       
    }


}
