<?php

class ContController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$contacto = Contacto::all();
		return View::make('contacto_form.index')->with('contacto', $contacto);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('contacto_form.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'titulo' => 'required',
			'icono' => 'required',
			'input1' => 'required',
			'input2' => 'required',
			'descripcion' => 'required',
			'textarea' => 'required',
			'btn' => 'required'
		);

		$validate = Validator::make(Input::all(), $rules, $messages);
			if ($validate->fails()) {
				return Redirect::back()->withErrors($validate);
			};


		$contacto = new Contacto;
		$contacto->titulo = Input::get('titulo'); 
		$contacto->icono = Input::get('icono'); 
		$contacto->input1 = Input::get('input1'); 
		$contacto->input2 = Input::get('input2'); 
		$contacto->descripcion = Input::get('descripcion'); 
		$contacto->textarea = Input::get('textarea'); 
		$contacto->btn = Input::get('btn'); 
		$contacto->save();

		return Redirect::to('contacto_form');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$contacto = Contacto::find($id);
		return View::make('contacto_form.show')->with('contacto', $contacto);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$contacto = Contacto::find($id);
		return View::make('contacto_form.edit')->with('contacto', $contacto);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$contacto = Contacto::find($id);
		$contacto->titulo = $input['titulo']; 
		$contacto->icono = $input['icono']; 
		$contacto->input1 = $input['input1']; 
		$contacto->input2 = $input['input2']; 
		$contacto->descripcion = $input['descripcion']; 
		$contacto->textarea = $input['textarea']; 
		$contacto->btn = $input['btn']; 
		$contacto->save();
		return Redirect::to('contacto_form/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$contacto = Contacto::find($id);
		$contacto->delete();
		return Redirect::to('contacto_form');
	}


}
