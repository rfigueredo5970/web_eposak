<?php

class FollowController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$follow = Follow::all();
		return View::make('follow.index')->with('follow', $follow);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('follow.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'phone_number' => 'required|min:3',
			'email' => 'required|email'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$follow = new Follow;
		$follow->phone_number = Input::get('phone_number'); 
		$follow->email = Input::get('email');
		$follow->save();
		return Redirect::to('follow');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$follow = Follow::find($id);
		return View::make('follow.show')->with('follow', $follow);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$follow = Follow::find($id);
		return View::make('follow.edit')->with('follow', $follow);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'phone_number' => 'required|min:3',
			'email' => 'required|email'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$netting = Netting::find($id);
		$netting->phone_number = $input['phone_number']; 
		$netting->email = $input['email'];
		$netting->save();
		return Redirect::to('netting/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$follow = Follow::find($id);
		$follow->delete();
		return Redirect::to('follow');
	}


}
