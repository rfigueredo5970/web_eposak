<?php

class DestinyController extends \BaseController {



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$destiny = Destiny::with('country')->get();
		return View::make('destiny.index')->with('destiny', $destiny);
	}

	


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$country = Country::lists('nombre', 'id');
		return View::make('destiny.create')->with( 'country',$country );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'name_en' => 'required|min:3',
			'name_es' => 'required|min:3',
			'description_en' => 'required|min:50',
			'description_es' => 'required|min:50',
			'image' => 'required',
			'country_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();
		}


		$destiny = new Destiny;
		$destiny->name_en = Input::get('name_en'); 
		$destiny->name_es = Input::get('name_es'); 
		$destiny->description_es = Input::get('description_es'); 
		$destiny->description_en = Input::get('description_en'); 
		$destiny->country_id = Input::get('country_id');
		$destiny->slug = $destiny->createSlug(Input::get('name_es'));
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."destiny".".".$ext;
		$file->move(public_path()."/images/", $name);
		$destiny->image = $name;
		$destiny->save();
		return Redirect::to('destiny');  	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$destiny = Destiny::find($id);
		return View::make('destiny.show')->with('destiny', $destiny);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$destiny = Destiny::find($id);
		$country = Country::lists('nombre', 'id');
		return View::make('destiny.edit')->with('destiny', $destiny)->with( 'country',$country );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{



		
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'name_en' => 'required|min:3',
			'name_es' => 'required|min:3',
			'description_en' => 'required|min:50',
			'description_es' => 'required|min:50',
			'image' => 'required',
			'country_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}  	




		$input = Input::all();
		$destiny = Destiny::find($id);
		$destiny->name_en  = $input['name_en']; 
		$destiny->name_es  = $input['name_es']; 
		$destiny->description_es  = $input['description_es']; 
		$destiny->description_en  = $input['description_en']; 
		$destiny->lat  = $input['lat']; 
		$destiny->lng  = $input['lng']; 
		$destiny->country_id  = $input['country_id'];
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."destiny".".".$ext;
		$file->move(public_path()."/images/", $name);
		$destiny->image = $name;
		if ($destiny->save()) {
			return Redirect::to('destiny/'.$id);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$destiny = Destiny::find($id);
		$destiny->delete();
		return Redirect::to('destiny');
	}


}
