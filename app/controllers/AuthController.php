<?php

class AuthController extends BaseController {

   /** public function showLoginFree()
    {
        // Verificamos si hay sesión activa
        if (Auth::check())
        {
            // Si tenemos sesión activa mostrará la página de inicio
            return Redirect::to('/');
        }
        // Si no hay sesión activa mostramos el formulario
        return View::make('/');
    }**/
    

    public function showLogin()
    {
        // Verificamos si hay sesión activa
        if (Auth::check())
        {
            // Si tenemos sesión activa mostrará la página de inicio
            if (Auth::user()->level === 'admin') {
                # code...
                //return Redirect::to('/admin');
                return Redirect::to('/admin');
            }
            else
            {
                echo('/');
               //return Redirect::to('/');
            }
        }
        // Si no hay sesión activa mostramos el formulario
        return View::make('login2');
    }


    public function postLogin()
    {
        // Obtenemos los datos del formulario
        $data = [
            'user_name' => Input::get('user_name'),
            'password' => Input::get('password'),

        ];


        // Verificamos los datos
        if (Auth::attempt($data , Input::get('remember'))) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
        {
           
            // Si nuestros datos son correctos mostramos la página de inicio
             if (Auth::user()->level === 'admin') {
                # code...
                return Redirect::to('/admin');
                //Response::json($data);
                //return Redirect::to('/');
            }
            else
            {
                
               return Redirect::back();
               //Response::json($data);
            }   
        }
           
        // Si los datos no son los correctos volvemos al login y mostramos un error
        return Redirect::back()->with('error_message', 'Invalid data')->withInput();
    }
  
   public function ajaxLogin()
    {
        // Obtenemos los datos del formulario
        $data = [
            'user_name' => Input::get('user_name'),
            'password' => Input::get('password'),

        ];


        // Verificamos los datos
        if (Auth::attempt($data , Input::get('remember'))) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
        {
         return Response::json(array('message' => 'Ha ingresado correctamente'),200);
            
        }else {
           
          $user = User::where('user_name',$data["user_name"])->first();
          
         if($user){
           
           return Response::json(array('error' => "La contraseña es inválida"),500);
           
         } else{
        // Si los datos no son los correctos volvemos al login y mostramos un error
              return Response::json(array('error' => 'El usuario no existe'),500);
          }
         
         }

    }
    public function logOut()
    {
        // Cerramos la sesión
        Auth::logout();
        // Volvemos al login y mostramos un mensaje indicando que se cerró la sesión
        return Redirect::to('/')->with('message', 'Su sesión fue cerrada correctamente');
    }


}