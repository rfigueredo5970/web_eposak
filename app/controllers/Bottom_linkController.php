<?php

class Bottom_linkController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$bottom = Bottom_link::with('section_navbar')->get();
		return View::make('bottom_link.index')->with('bottom', $bottom);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$navbar = Section_navbar::lists('name', 'id');
		return View::make('bottom_link.create')->with('navbar',$navbar);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			//'link' => 'required|min:3;',
			'image' => 'required|min:3',
			'description_es' => 'required|min:1',
			'description_en' => 'required|min:1',
			'section_navbar_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$bottom = new Bottom_link;
		$bottom->link = Input::get('link');
		$bottom->description_es = Input::get('description_es'); 
		$bottom->description_en = Input::get('description_en');
		$bottom->section_navbar_id = Input::get('section_navbar_id'); 
		
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."bottom".".".$ext;
		$file->move(public_path()."/images/", $name);
		$bottom->image = $name;


		if ($bottom->save()) {
			return Redirect::to('bottomlink'); 
		}else{
	      
	     	return Redirect::back()->withErrors($validate);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$bottom = Bottom_link::with('section_navbar')->find($id);
		return View::make('bottom_link.show')->with('bottom', $bottom);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bottom = Bottom_link::find($id);
		$navbar = Section_navbar::lists('name', 'id');
		return View::make('bottom_link.edit')->with('bottom', $bottom)->with('navbar',$navbar);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'link' => 'required|min:3;',
			'image' => 'required|min:3',
			'description_es' => 'required|min:1',
			'description_en' => 'required|min:1',
			'section_navbar_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		};

		$input = Input::all();
		$bottom = Bottom_link::find($id);
		$bottom->link = $input['link']; 
		$bottom->description_es = $input['description_es']; 
		$bottom->description_en = $input['description_en'];
		$bottom->section_navbar_id = $input['section_navbar_id'];


		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."bottom".".".$ext;
		$file->move(public_path()."/images/", $name);
		$bottom->image = $name;

		$bottom->save();
		return Redirect::to('bottomlink');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*
	public function destroy($id)
	{
		$bottom = Bottom_link::find($id);
		$bottom->delete();
		return Redirect::to('bottomlink');
	}
	*/


}
