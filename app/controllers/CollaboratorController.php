<?php

class CollaboratorController extends \BaseController {

	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$collaborator = Collaborator::with('user','project')->get();
		return View::make('collaborator.index')->with('collaborator', $collaborator);
		//echo $collaborator[0]->project->title_es;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = User::lists('user_name', 'id');
		return View::make('form_proyecto/'.$id)->with('user',$user);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'message_es' => 'min:3;',
			'message_en' => 'min:3;',
			'user_id' => 'required',
			'project_id' => 'required'
		);

		/**
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		};*/

		
		$user = Auth::user();
		$collaborator = new Collaborator;
		$collaborator->message_es = Input::get('message_es');
		$collaborator->message_en = Input::get('message_en'); 
		$collaborator->contribution = Input::get('contribution');
		$collaborator->name = $user->firstname;
		$collaborator->user_id = $user->id;
		$collaborator->project_id = Input::get('project');

		//echo $collaborator;

		if ($collaborator->save()) {
		//print_r($_POST);
			return Redirect::to('/')->with('messages','se ha generado exitosamente'); 
		}
		/*

		
		*/


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$collaborator = Collaborator::with('users')->find($id);
		return View::make('collaborator.show')->with('collaborator', $collaborator);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$collaborator = Collaborator::find($id);
		$user = User::lists('user_name', 'id');
		return View::make('collaborator.edit')->with('collaborator', $collaborator)->with('user',$user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'message_es' => 'required|min:3;',
			'message_en' => 'required|min:3;',
			'phone_number' => 'required|min:7|numeric',
			'name' => 'required|min:1',
			'nationality' => 'required|min:1',
			'email' => 'required|email',
			'user_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		};

		$input = Input::all();
		$collaborator = Collaborator::find($id);
		$collaborator->message_es = $input['message_es']; 
		$collaborator->message_en = $input['message_en']; 
		$collaborator->phone_number = $input['phone_number'];
		$collaborator->name = $input['name'];
		$collaborator->nationality = $input['nationality'];
		$collaborator->email = $input['email'];
		$collaborator->user_id = $input['user_id'];

		$collaborator->save();
		return Redirect::to('collaborator');



	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$Collaborator = Collaborator::find($id);
		$Collaborator->delete();
		return Redirect::to('collaborator');
	}


}
