<?php

class TravelTypeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$t_travel = Travel_type::all();
		return View::make('travel_type.index')->with('t_travel', $t_travel);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('travel_type.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$t_travel = new Travel_type;
		$t_travel->name_es = Input::get('name_es'); 
		$t_travel->name_en = Input::get('name_en');
		$t_travel->save();
		return Redirect::to('traveltype');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$t_travel = Travel_type::find($id);
		return View::make('travel_type.show')->with('t_travel', $t_travel);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$t_travel = Travel_type::find($id);
		return View::make('travel_type.edit')->with('t_travel', $t_travel);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$t_travel = Travel_type::find($id);
		$t_travel->name_es = $input['name_es']; 
		$t_travel->name_en = $input['name_en'];
		$t_travel->save();
		return Redirect::to('traveltype/'.$id); 

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$t_travel = Travel_type::find($id);
		$t_travel->delete();
		return Redirect::to('traveltype');
	}


}
