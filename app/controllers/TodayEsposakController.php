<?php

class TodayEsposakController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$today = Today_esposak::all();
		return View::make('today_esposak.index')->with('today', $today);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('today_esposak.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){

		$messages = array(
			'required' => '*Este campo es obligatorio.',
			'min' => '*Mínimo :min caracteres.',
			'mimes' => '*Debe ingresar una extensión de imagen válida.',
			'numeric' => '*Es necesario que ingrese carecteres numéricos.'
		);

		$rules = array(

			'title_es' => 'required|min:3;',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:1',
			'description_en' => 'required|min:1',
			'image' => 'required'

			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}
		{
			$today = new Today_esposak;
			$today->title_es = Input::get('title_es'); 
			$today->title_en = Input::get('title_en');
			$today->description_es = Input::get('description_es');
			$today->description_en = Input::get('description_en');
			$file = Input::file('image');
			$ext = $file->getClientOriginalExtension();
			$name = "image".time()."todayes".".".$ext;
			$file->move(public_path()."/images/", $name);
			$today->image = $name;
			$today->save();
			return Redirect::to('todayesposak');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$today = Today_esposak::find($id);
		return View::make('today_esposak.show')->with('today', $today);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$today = Today_esposak::find($id);
		return View::make('today_esposak.edit')->with('today', $today);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Este campo es obligatorio.',
			'min' => '*Mínimo :min caracteres.',
			'mimes' => '*Debe ingresar una extensión de imagen válida.',
			'numeric' => '*Es necesario que ingrese carecteres numéricos.'
		);

		$rules = array(

			'title_es' => 'required|min:3;',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:1',
			'description_en' => 'required|min:1',
			'image' => 'required'
			
		);


		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$today = Today_esposak::find($id);
		$today->title_es = $input['title_es']; 
		$today->title_en = $input['title_en']; 
		$today->description_es = $input['description_es']; 
		$today->description_en = $input['description_en'];		 
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".".".time().$ext;
		$file->move(public_path()."/images/", $name);
		$today->image = $name;

		$today->save();
		return Redirect::to('todayesposak/'.$id);
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$today = Today_esposak::find($id);
		$today->delete();
		return Redirect::to('todayesposak');
	}
}
