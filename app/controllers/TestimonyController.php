<?php

class TestimonyController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$testimony = Testimony::all();
		return View::make('testimony.index')->with('testimony', $testimony);
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('testimony.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$testimony = new Testimony;
		$testimony->title_en = Input::get('title_en'); 
		$testimony->title_es = Input::get('title_es'); 
		$testimony->description_en = Input::get('description_en'); 
		$testimony->description_es = Input::get('description_es'); 
		$testimony->save();

		return Redirect::to('testimony');  
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$testimony = Testimony::find($id);
		return View::make('testimony.show')->with('testimony', $testimony);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$testimony = Testimony::find($id);
		return View::make('testimony.edit')->with('testimony', $testimony);
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$testimony = Testimony::find($id);
		$testimony->title_en = $input['title_en']; 
		$testimony->title_es = $input['title_es']; 
		$testimony->description_en = $input['description_en']; 
		$testimony->description_es = $input['description_es']; 
		$testimony->save();
		return Redirect::to('testimony/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$testimony = Testimony::find($id);
		$testimony->delete();
		return Redirect::to('testimony');
	}


}
