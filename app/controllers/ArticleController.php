<?php

class ArticleController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$articles = Article::all();
		return View::make('articles.index')->with('articles', $articles);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('articles.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'title_es' => 'required|min:3',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:50',
			'description_en' => 'required|min:50',
			'image' => 'required|mimes:jpeg,jpg,png,JPG,gif'
		);

		$validate = Validator::make(Input::all(), $rules, $messages);
			if ($validate->fails()) {
				return Redirect::back()->withErrors($validate);
			};




		$article = new Article;
		$article->title_es = Input::get('title_es'); 
		$article->title_en = Input::get('title_en'); 
		$article->description_es = Input::get('description_es'); 
		$article->description_en = Input::get('description_en'); 
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."article".".".$ext;
		$file->move(public_path()."/images/", $name);
		$article->image = $name;
		
		if ($article->save()) {
			return Redirect::to('articles');  
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
		}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$article = Article::find($id);
		return View::make('articles.show')->with('article', $article);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$article = Article::find($id);
		return View::make('articles.edit')->with('article', $article);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'title_es' => 'required|min:3',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:50',
			'description_en' => 'required|min:50',
			'image' => 'required|mimes:jpeg,jpg,png,JPG,gif'
		);

		$validate = Validator::make(Input::all(), $rules, $messages);
			if ($validate->fails()) {
				return Redirect::back()->withErrors($validate);
			};


		$input = Input::all();
		$article = Article::find($id);
		$article->title_es = $input['title_es']; 
		$article->title_en = $input['title_en']; 
		$article->description_es = $input['description_es']; 
		$article->description_en = $input['description_en'];		 
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time().".".$ext;
		$file->move(public_path()."/images/", $name);
		$article->image = $name;

		
		
		if ($article->save()) { 
			return Redirect::to('articles/'.$id);
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
		}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$article = Article::find($id);
		$article->delete();
		return Redirect::to('articles');
	}


}
