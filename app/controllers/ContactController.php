<?php

class ContactController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('auth',array('only' => ['index','show','edit','create']));
		$this->beforeFilter('admin',array('only' => ['index','show','edit','create']));
	}
	public function index()
	{
		$contact = Voluntary::all();
		return View::make('contact.index')->with('contact', $contact);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('contact.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */



	public function store()
	{
			// print_r($_POST);
			// die;

		$messages = array(
			'required' => '*Este campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Máximo :max carácteres.',
			'mimes' => '*Debe ingresar una extensión de imagen válida.',
			'numeric' => '*Es necesario que ingrese carecteres numéricos.',
			'email' => '*Es necesario que ingrese una direccion de correo válida.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'email' => 'required|email'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}
		// Captura de la data de los inputs
		$contact = new Voluntary;
		$contact->message = Input::get('message'); 
		$contact->name = Input::get('name');
		$contact->email = Input::get('email');


		if(Auth::user() && Auth::user()->level == 'admin'){//Auth::user()->level == 'admin') {
			return Redirect::to('contact');
		}
		else{
			// Configuracion de envio de correo
			// Template de correo en views/emails/contact

			Mail::send('emails.contact', array('name' => $contact->name, 'email' => $contact->email, 'body_email' => $contact->message), function($message)
		{
		    $message->to(  $_ENV['MAIL_CONTACT'] , 'eposak')->subject('Correo de Contacto');

		});
			return Redirect::back()->with('msg','tu mensaje fue enviado exitosamente');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$contact = Voluntary::find($id);
		return View::make('contact.show')->with('contact', $contact);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$contact = Voluntary::find($id);
		return View::make('contact.edit')->with('contact', $contact);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$messages = array(
			'required' => '*Este campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Máximo :max carácteres.',
			'mimes' => '*Debe ingresar una extensión de imagen válida.',
			'numeric' => '*Es necesario que ingrese carecteres numéricos.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'email' => 'required|email',
			'status' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$contact = Voluntary::find($id);
		$contact->message = $input['message']; 
		$contact->name = $input['name'];
		$contact->email = $input['email'];
		$contact->email = $input['description_es'];
		$contact->email = $input['description_en'];
		$contact->email = $input['status'];
		$contact->save();
		return Redirect::to('contact/'.$id); 
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$contact = Voluntary::find($id);
		$contact->delete();
		return Redirect::to('contact');
	}
}
