<?php

class FreeController extends \BaseController {

	public function indexFree()
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)->first();
		$destiny = Destiny::with('country')->get();
		$travel_type_exp=Travel_type::where('id','1')->get();
		$travel_exp = Travel::with('travel_type','multimedias')->where('travel_type_id','1')->get();
		$travel_type_vol=Travel_type::where('id','2')->get();
		$travel_vol = Travel::with('travel_type','multimedias')->where('travel_type_id','2')->get();
		$articles = Article::orderBy('id', 'desc')->first();
		$today_eposak = Today_esposak::all();
		$today = $today_eposak->chunk(3);
		$slider = Slider::all();
		/****/
		return View::make('index')
							->with('page', $page)
							->with('destiny', $destiny)
							->with('travel_type_exp',$travel_type_exp)
							->with('travel_exp',$travel_exp)
							->with('travel_type_vol',$travel_type_vol)
							->with('travel_vol',$travel_vol)
							->with('articles', $articles)
							->with('today', $today)
							->with('slider', $slider)
							->with('principal_netting', $principal_netting);

	}
	
	public function eposak_section()
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$section_eposak = Section_navbar::find(7);
		$information = General_information::with('section_navbar')->get();
		$aliados = Allie::all();
		/**
							**/
		return View::make('eposak')
							->with('page', $page)
							->with('information', $information)
							->with('aliados', $aliados)
							->with('section_eposak', $section_eposak)
							->with('principal_netting', $principal_netting)

							;


	}

	public function comunidades()
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$navbar = Section_navbar::find(1);
		$destiny = Destiny::with('country','travel')->get();
		$travel_type = Travel_type::with('travel')->get();

		$bottom_link = Bottom_link::with('section_navbar')->where('section_navbar_id','3')->get();
		$bottom= $bottom_link[0];//se obtiene el primer elemento de la tabla bottom_link con el section_navbar_id numero 2

		$destiny_travel_kamarata = Destiny::with('travel')->where('id','3')->get();
		$imagen_destiny_travel_kamarata = $destiny_travel_kamarata[0]->travel[0]->multimedias[rand(0,sizeof($destiny_travel_kamarata[0]->travel[0]->multimedias)-1)]->name;

		
		$destiny_travel_birongo = Destiny::with('travel')->where('id','2')->get();
		$imagen_destiny_travel_birongo = $destiny_travel_birongo[0]->travel[0]->multimedias[rand(1,sizeof($destiny_travel_birongo[0]->travel[0]->multimedias)-1)]->name;


		/**
							**/
		return View::make('comunidades')
							->with('page', $page)
							->with('navbar', $navbar)
							->with('destiny', $destiny)
							->with('imagen_destiny_travel_kamarata', $imagen_destiny_travel_kamarata)
							->with('imagen_destiny_travel_birongo', $imagen_destiny_travel_birongo)
							->with('travel_type', $travel_type)
							->with('bottom', $bottom)
							->with('principal_netting', $principal_netting)
							;
	}

	public function fundacion()
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$bottom_link = Bottom_link::with('section_navbar')->where('section_navbar_id','2')->get();
		$bottom= $bottom_link[0];//se obtiene el primer elemento de la tabla bottom_link con el section_navbar_id numero 2
		$today = Today_esposak::all();
		//$today = Today_esposak::orderBy('id', 'desc')->first();//se obtiene el ultimo registro de la tabla today_esposak
		$navbar = Section_navbar::find(2);
		$destiny = Destiny::with('country','project')->get();
		$destiny_kamarata = Destiny::with('project')->orderBy('id', 'desc')->first();
		$destiny_birongo = Destiny::with('project')->orderBy('id', 'asc')->first();

		$imagen_destiny_project_kamarata = $destiny_kamarata->project[0]->multimedias[rand(0,sizeof($destiny_kamarata->project[0]->multimedias)-1)]->name;

		$imagen_destiny_project_birongo = $destiny_birongo->project[0]->multimedias[rand(1,sizeof($destiny_birongo->project[0]->multimedias)-1)]->name;
		

					
		return View::make('fundacion')
							->with('page', $page)
							->with('navbar', $navbar)
							->with('today', $today)
							->with('destiny', $destiny)
							->with('imagen_destiny_project_kamarata', $imagen_destiny_project_kamarata)
							->with('imagen_destiny_project_birongo', $imagen_destiny_project_birongo)
							->with('bottom', $bottom)
							->with('principal_netting', $principal_netting)
							;

	}

	public function proyecto_kam($id)
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$project_emprender = Project::where('destiny_id',$id)->where('type','emprendimiento')->with('entrepreneurs')->get();
		$project_comunitario = Project::where('destiny_id',$id)->where('type','comunitario')->get();
		$destiny = Destiny::find($id);
		$section = Section_navbar::find(14);
		$bottom = Bottom_link::find(9);



						
		return View::make('proyecto-kamarata')
							->with('page', $page)
							->with('project_emprender', $project_emprender)
							->with('project_comunitario', $project_comunitario)
							->with('section', $section)
							->with('destiny', $destiny)
							->with('bottom', $bottom)
							->with('principal_netting', $principal_netting)
							;
	}

	public function destino($slug)
	{
		$destiny = Destiny::where('slug', $slug)->first();
		
		$id = $destiny->id;
		
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];

		$travel_exp = Travel::with('destiny','travel_type')->where('destiny_id', $id)->where('travel_type_id','1')->paginate(3);
		$travel_vol = Travel::with('destiny','travel_type')->where('destiny_id', $id)->where('travel_type_id','2')->paginate(3);


		$travel_type = Travel_type::all();

		$bottom = Bottom_link::with('section_navbar')->find(6);//se obtiene el boton de redireccion de la seccion llamada VIAJES DE VOLUNTARIADO

		return View::make('kamarata')
								->with('page', $page)
								->with('travel_type', $travel_type)
								->with('travel_exp', $travel_exp)
								->with('travel_vol', $travel_vol)
								->with('destiny', $destiny)
								->with('bottom', $bottom)
								->with('principal_netting', $principal_netting);
		
		
		
	}

	public function viajes($slug,$order_by = null)
	{  	
				if($order_by == NULL) {
					
						$order_by = "start_at";
					
				}
		
		$travel_type = Travel_type::where('slug', 'LIKE',$slug)->orWhere('id',$slug)->first();
		$travel= Travel::with('travel_type','destiny')->where('travel_type_id',$travel_type->id)->orderBy($order_by)->skip(0)->take(3)->get();//se obtienen todos los viajes que pertenezcan a la categoria viajes Voluntariados
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];

		if($travel_type->id == 1){
			$section = Section_navbar::find(4);
			$bottom = Bottom_link::with('section_navbar')->find(4);//se obtiene la seccion llamada VIAJES EXPERIENCIALES
		}

		if($travel_type->id == 2){
			$section = Section_navbar::find(5);
			$bottom = Bottom_link::with('section_navbar')->find(5);//se obtiene la seccion llamada VIAJES EXPERIENCIALES
		}
		
		
		switch ($order_by) {
    	case "start_at":
      	$order = "fecha de salida";
        break;
    	case "end_at":
        $order = "fecha de regreso";
        break;
    	case "title_es":
        $order = "Nombre del proyecto";
        break;
			case "prices":
        $order = "Monto del proyecto";
        break;
			default:
				$order = "fecha de salida";
}

		return View::make('viajesexperenciales')
							->with('page', $page)
							->with('section', $section)
							->with('travel_type', $travel_type)
							->with('travel', $travel)
							->with('bottom', $bottom)
							->with('slug', $slug)
							->with('order',$order)
							->with('principal_netting', $principal_netting) ;

	}

	public function masviajes($type_of_travel)
	{ 
		$order_by = "start_at";
		$travel= Travel::with('destiny', 'multimedias')->where('travel_type_id',$type_of_travel)->orderBy($order_by)->paginate(3);
		 
		return Response::json($travel);
	}
	
	public function loadTravelByDestiny($type_of_travel, $destiny_id)
	{ 
		$order_by = "start_at";
		$travel= Travel::with('destiny', 'multimedias')->where('travel_type_id',$type_of_travel)->where('destiny_id',$destiny_id)->orderBy($order_by)->paginate(3);
		 
		return Response::json($travel);
	}	

	public function viaje_individual($slug)
	{
		$travel = Travel::with('multimedias','itinerary','destiny')->where("slug",$slug)->first();
		
		$pasajero = Passenger::with('travels')->where('travels_id',$travel->id)->get();
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)->first();
		$secondary_netting = $netting->chunk(3)[1];

		$itinerary = Itinerary::orderBy('day','asc')->where('travel_id',$travel->id)->get();
		$section = Section_navbar::find(12);



		return View::make('viajeindividual')
							->with('page', $page)
							->with('travel', $travel)
							->with('pasajero', $pasajero)
							->with('section', $section)
							->with('principal_netting', $principal_netting)
							->with('secondary_netting', $secondary_netting)
							->with('itinerary', $itinerary)
							;

	}

	public function articulo($id)
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$article = Article::find($id);
		//echo $article->image;

		return View::make('articulo')
							->with('page', $page)
							->with('article', $article)
							->with('principal_netting', $principal_netting)
							;
	}

	public function lonuevo($id)
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$today = Today_esposak::find($id);
		//echo $today->image;

		return View::make('noticia')
							->with('page', $page)
							->with('today', $today)
							->with('principal_netting', $principal_netting)
							;

	}

	public function contacto()
	{

		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$navbar = Section_navbar::find(3);
		$contacto_section = Contacto::find(1);

		$other_netting =  $netting->chunk(3)[0];

		$location = Location::all();
		$location_use = $location[0] ;
		$follow = Follow::all();
		$follow_use = $follow[0];
		$bottom = Bottom_link::with('section_navbar')->find(1);//se obtiene el boton de redireccion de la seccion llamada VIAJES DE VOLUNTARIADO

		$voluntary = Auth::user();

		//echo $voluntary;

		return View::make('contacto')
							->with('page', $page)
							->with('contacto_section', $contacto_section)
							->with('other_netting', $other_netting)
							->with('location_use', $location_use)
							->with('follow_use', $follow_use)
							->with('navbar', $navbar)
							->with('bottom', $bottom)
							->with('principal_netting', $principal_netting)
							->with('voluntary', $voluntary)
							;
	}

	public function exito()
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$navbar = Section_navbar::find(16);
		$project_emprender= Project::where('status','end')->where('type','emprendimiento')->get();//Todos los proyectos terminados de tipo emprendimiento.
		$project_comunitario= Project::where('status','end')->where('type','comunitario')->get();//Todos los proyectos terminados de tipo comunitario.
		$bottom = Bottom_link::find(10);

		/*
				*/			
		return View::make('exito')
							->with('page', $page)
							->with('navbar', $navbar)
							->with('project_emprender', $project_emprender)
							->with('project_comunitario', $project_comunitario)
							->with('bottom', $bottom)
							->with('principal_netting', $principal_netting)
							;
							
	}

	public function voluntariado()
	{

		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$navbar = Section_navbar::find(11);
		$general =  General_information::where('section_navbar_id',$navbar->id)->get();
		$voluntary = Voluntary::all();
		$bottom = Bottom_link::where('section_navbar_id',$navbar->id)->get();
		$bottom_l = $bottom[0];

		// return var_dump($general); die;
		return View::make('voluntariados')
							->with('page', $page)
							->with('voluntary', $voluntary)
							->with('navbar', $navbar)
							->with('general', $general)
							->with('bottom_l', $bottom_l)
							->with('principal_netting', $principal_netting)
							;

	}

	public function proyecto_comunitario($id)
	{	
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$netting = Netting::all();
		$project = Project::find($id);
		$section = Section_navbar::find(13);


		
		

		
		return View::make('proyecto-comunitario')
							->with('page', $page)
							->with('project', $project)
							->with('section', $section)
							->with('principal_netting', $principal_netting)
							;

	}
	
	public function form_proyecto($id)
	{
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$user = Auth::user();
		$project = Project::find($id);
		$navbar = Section_navbar::find(15);

		return View::make('formulario-proyecto')
							->with('page', $page)
							->with('user', $user)
							->with('project', $project)
							->with('navbar', $navbar)
							->with('principal_netting', $principal_netting)
							;
	

	}
	
	public function form_viaje($id) 
	{ 
		$page = Details_page::all();
		$netting = Netting::all();
		$principal_netting = $netting->chunk(3)[1];
		$user = Auth::user();
		$viaje = Travel::find($id);

		return View::make('formularioviaje')
							->with('page', $page)
							->with('user', $user)
							->with('principal_netting', $principal_netting)
							->with('viaje', $viaje)
							;
	
	}

}
