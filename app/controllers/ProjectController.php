<?php
	

class ProjectController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$projects = Project::with('multimedias','destiny','entrepreneurs')->get();
		return View::make('projects.index')->with('project', $projects);
		//echo $projects;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$country = Country::lists('nombre', 'id');
		$destiny = Destiny::lists('name_es', 'id');
		$media = Multimedia::lists('id','multimediable_id');	
		return View::make('projects.create')->with( 'country',$country )->with('media',$media)->with( 'destiny',$destiny );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{	


		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'unique' => ' Ya existe un proyecto con este nombre. Por favor intente con otro',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'title_es' => 'unique:projects|required|min:3;',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:1',
			'lugar' => 'required|min:3',
			'total_cost' => 'required|numeric',
			'description_en' => 'required|min:1',
			'objective_en' => 'required',
			'objective_es' => 'required',
			'community_benefits_es' => 'required',
			'community_benefits_en' => 'required',
			'country_id' => 'required',
			'name' => 'required',
			'destiny_id' => 'required',
			'final_project_en' => 'required',
			'status' => 'required',
			'type' => 'required',
			'final_project_es' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}


		$project = new Project;
		$project->country_id = Input::get('country_id');
		$project->title_en = Input::get('title_en'); 
		$project->lugar = Input::get('lugar'); 
		$project->title_es = Input::get('title_es'); 
		$project->description_en = Input::get('description_en'); 
		$project->description_es = Input::get('description_es'); 
		$project->total_cost = Input::get('total_cost'); 
		$project->objective_es = Input::get('objective_es'); 
		$project->objective_en = Input::get('objective_en'); 
		$project->community_benefits_es = Input::get('community_benefits_es'); 
		$project->community_benefits_en = Input::get('community_benefits_en'); 
		$project->final_project_es = Input::get('final_project_es'); 
		$project->final_project_en = Input::get('final_project_en');

		$project->status = Input::get('status');
		$project->status = Input::get('type');

		$project->destiny_id = Input::get('destiny_id');
		$project->save();
		
		$files = Input::file('name');
		if (is_array($files) || is_object($files))
		{
			foreach ( $files as $file){

				$multimedia = new Multimedia;
				$ext = $file->getClientOriginalName();
				$name = $file->getClientOriginalName().time().$ext;
				$file->move(public_path()."/images/",$name);
				$multimedia->name = $name;
				$project->multimedias()->save($multimedia);
				//var_dump(sizeof($multimedia));
			};
		}
		return Redirect::to('projects');

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function changeStatus($id){
		$input = Input::all();
		$project = Project::find($id);
		$project->status = $input['status'];
		if ($project->save()) {
			return View::make('projects.show')->with('project', $project);
		}else{

		echo "hay un error";
		}

	}

	public function show($id)
	{
		$project =  Project::with('multimedias','entrepreneurs')->find($id);
		

		/**/
		return View::make('projects.show')->with('project', $project);
		/**/
		/**if (isset($project->entrepreneurs)) {
			//echo $project->entrepreneurs->name;
			echo "si hay";
		}else{
			echo "mamalo";
		}**/
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = Project::find($id);
		$country = Country::lists('nombre', 'id');
		$destiny = Destiny::lists('name_es', 'id');
		$media = Multimedia::lists('id','multimediable_id');	
		return View::make('projects.edit')->with('project', $project)->with( 'country',$country )->with('media',$media)->with('destiny',$destiny);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'unique' => ' Ya existe un proyecto con este nombre. Por favor intente con otro',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'title_es' => 'required|min:3;',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:1',
			'lugar' => 'required|min:3',
			'total_cost' => 'required|numeric',
			'description_en' => 'required|min:1',
			'objective_en' => 'required',
			'objective_es' => 'required',
			'community_benefits_es' => 'required',
			'community_benefits_en' => 'required',
			'country_id' => 'required',
			'name' => 'required',
			'destiny_id' => 'required',
			'final_project_en' => 'required',
			'status' => 'required',
			'type' => 'required',
			'final_project_es' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		

		$input = Input::all();
		$project = Project::find($id);
		$project->title_es = $input['title_es']; 
		$project->title_en = $input['title_en']; 
		$project->description_es = $input['description_es']; 
		$project->description_en = $input['description_en'];
		$project->total_cost = $input['total_cost'];
		$project->objective_es = $input['objective_es'];
		$project->objective_en = $input['objective_en'];
		$project->community_benefits_es = $input['community_benefits_es'];
		$project->community_benefits_en = $input['community_benefits_en'];
		$project->final_project_es = $input['final_project_es'];
		$project->final_project_en = $input['final_project_en'];
		$project->status = $input['status'];
		$project->type = $input['type'];
		$project->destiny_id = $input['destiny_id'];
		$project->save();
		$files = Input::file('name');
		
		if (is_array($files) || is_object($files))
		{
			foreach ( $files as $file){

				$multimedia = new Multimedia;
				$ext = $file->getClientOriginalName();
				$name = $file->getClientOriginalName().time().$ext;
				$file->move(public_path()."/images/",$name);
				$multimedia->name = $name;
				$project->multimedias()->save($multimedia);
				//var_dump(sizeof($multimedia));
			};
		}
		return Redirect::to('projects');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$project = Project::find($id);
		$project->delete();
		return Redirect::to('projects');
	}

	public function end()
	{
		$project = Project::where('status','end')->with('multimedias','destiny')->get();
		return View::make('projects.end_project')->with('project', $project);	
		echo $project;
	}


}
