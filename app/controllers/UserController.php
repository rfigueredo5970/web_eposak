<?php

class UserController extends \BaseController {

	
	public function __construct()
	{
		$this->beforeFilter('auth',array('only' => ['index','show','edit','create']));
		$this->beforeFilter('admin',array('only' => ['index','show','edit','create']));
		//$this->beforeFilter('admin',array('only' => array('store ,create') ));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		return View::make('users.index')->with('users', $users);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}
	public function register()
	{
		return View::make('index');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'email' => 'Debe ingresar una dirección de correo electrónico válida',
			'email.required' => 'Debe ingresar una dirección de correo electrónico',
			'email.unique' => 'Ya existe un usuario con este correo electŕonico',
			'user_name.required' => 'Debe ingresar nombre de usuario',
			'user_name.unique' => 'Ya existe un usuario con este nombre de usuario',
			'password.required' => 'Debe introducir una contraseña',
			'password_confirmation.required' => 'Debe confirmar la contraseña',
			'min' => 'Mínimo :min carácteres.',
			'max' => 'Maximo :max carácteres.',
			'mimes' => 'Debe ingresar una extensión de imagen válida.',
			'size' => 'La cadena debe tener :size caracteres.',
			'confirmed' => 'Error al confirmar contraseña (verifique que sean idénticas).',
			'numeric' => 'Es necesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'user_name' => 'required|min:3|unique:users',
			'email' => 'required|email|min:3|unique:users',
			'password' => 'required|min:3|confirmed',
			'password_confirmation' => 'required|min:3',
			'document' => 'min:3;',
			'phone' => 'numeric'
			
		);

		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$user = new User;
		$user->user_name = Input::get('user_name'); 
		$user->email = Input::get('email'); 
		$user->code = str_random(30);
		$user->validated = false;
		
		if (!empty(Input::get('balance'))) {
			$user->balance = Input::get('balance'); 
		}else{
			$user->balance = 0;
		}

		if (!empty(Input::get('document'))) {
			$user->document = Input::get('document'); 
		}else{
			$user->document = '0';
		}

		if (!empty(Input::get('level'))) {
			$user->level = Input::get('level'); 
		}else{
			$user->level = 'free';
		}

		$user->full_name = Input::get('full_name');
		$user->phone = Input::get('phone');
		
		$user->password = Hash::make(Input::get('password'));
		$user->password = Hash::make(Input::get('password_confirmation'));
		
		if (Input::file('picture') > 0) {
			$file = Input::file('picture');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."user".".".$ext;
		$file->move(public_path()."/images/", $name);
		$user->picture = $name;
		}
		$user->save();
		//Response::json($user);
		
		if ($user->level === 'admin') {
			return Redirect::to('users');
		}
		else
		{
			$data = array('name' => $user->name, 'email' => $user->email,'code'=>$user->code, 'id' => $user->id);
			
			Mail::send('emails.confirmation',$data , function($message) use($data)
		{	
			$message->from('no-reply@eposak.org','Eposak.org');
		    $message->to(  $data['email'] , $data['name'])->subject('Correo de Confirmación Eposak');

		});


			return Redirect::back()->with('message','se envió un correo de confirmación');
			Session::flash('message', 'fue registrado perfectamente!'); 
			Session::flash('alert-class', 'alert-info'); 


		}
			/*
			*/

		/*
		return Response::json($user);

		*/  
	}
	
	public function store_ajax()
	{
		
		
		$messages = array(
			'email' => 'Debe ingresar una dirección de correo electrónico válida',
			'email.required' => 'Debe ingresar una dirección de correo electrónico',
			'email.unique' => 'Ya existe un usuario con este correo electŕonico',
			'user_name.required' => 'Debe ingresar nombre de usuario',
			'user_name.unique' => 'Ya existe un usuario con este nombre de usuario',
			'password.required' => 'Debe introducir una contraseña',
			'password_confirmation.required' => 'Debe confirmar la contraseña',
			'min' => 'Mínimo :min carácteres.',
			'max' => 'Maximo :max carácteres.',
			'mimes' => 'Debe ingresar una extensión de imagen válida.',
			'size' => 'La cadena debe tener :size caracteres.',
			'confirmed' => 'Error al confirmar contraseña (verifique que sean idénticas).',
			'numeric' => 'Es necesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'user_name' => 'required|min:3|unique:users',
			'email' => 'required|email|min:3|unique:users',
			'password' => 'required|min:3|confirmed',
			'password_confirmation' => 'required|min:3',
			'document' => 'min:3;',
			'phone' => 'numeric'
			
		);
		

		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			
			return Response::json( array("error" =>$validate->messages()->first() ) ,500 );


		}

		$user = new User;
		$user->user_name = Input::get('user_name'); 
		$user->email = Input::get('email'); 
		$user->code = str_random(30);
		$user->validated = false;
		
		if (!empty(Input::get('balance'))) {
			$user->balance = Input::get('balance'); 
		}else{
			$user->balance = 0;
		}

		if (!empty(Input::get('document'))) {
			$user->document = Input::get('document'); 
		}else{
			$user->document = '0';
		}

		if (!empty(Input::get('level'))) {
			$user->level = Input::get('level'); 
		}else{
			$user->level = 'free';
		}

		$user->full_name = Input::get('full_name');
		$user->phone = Input::get('phone');
		
		$user->password = Hash::make(Input::get('password'));
		$user->password = Hash::make(Input::get('password_confirmation'));
		
		if (Input::file('picture') > 0) {
			$file = Input::file('picture');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."user".".".$ext;
		$file->move(public_path()."/images/", $name);
		$user->picture = $name;
		}
		$user->save();
		
		
		if ($user->level === 'admin') {
			
			return Response::json(array('message' => 'El usuario fue registrado'),200);
		}
		else
		{
			$data = array('name' => $user->name, 'email' => $user->email,'code'=>$user->code, 'id' => $user->id);
			
			Mail::send('emails.confirmation',$data , function($message) use($data)
		{	
			$message->from('no-reply@eposak.org','Eposak.org');
		    $message->to(  $data['email'] , $data['name'])->subject('Correo de Confirmación Eposak');

		});

			return Response::json(array('message' => 'Se envió un correo de confirmación'),200);
			Session::flash('message', 'fue registrado perfectamente!'); 
			Session::flash('alert-class', 'alert-info'); 
		}
			/*
			*/

		/*
		return Response::json($user);

		*/  
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		return View::make('users.show')->with('user', $user);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		return View::make('users.edit')->with('user', $user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => 'Mínimo :min carácteres.',
			'max' => 'Maximo :max carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'size' => 'La cadena debe tener :size caracteres.',
			'numeric' => 'Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'user_name' => 'required|min:3;',
			'document' => 'required|min:3;',
			'firstname' => 'alpha',
			'lastname' => 'alpha',
			'phone' => 'numeric',
			'password' => 'min:3|confirmed',
			'password_confirmation' => 'min:3'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}
		$input = Input::all();
		$user = User::find($id);
		$user->user_name = $input['user_name']; 
		$user->firstname = $input['firstname']; 
		$user->lastname = $input['lastname'];
		$user->password = Hash::make(Input::get('password'));
		$user->password = Hash::make(Input::get('password_confirmation'));
		$fullname = $user->firstname." ".$user->lastname;
		$user->full_name =$fullname; 
		$user->balance = $input['balance']; 
		$user->document = $input['document'];
		
		if ($user->email) {
			$user->email = $input['email'];
		}else{
			$user->email = Input::get('email');
		}
		if ($user->phone) {
			$user->phone = $input['phone'];
		}else{
			$user->phone = Input::get('phone');
		}

		if ($user->address) {
			$user->address = $input['address'];
		}else{
			$user->address = Input::get('address');
		}

		if ($user->birthdate) {
			$user->birthdate = $input['birthdate'];
		}else{
			$user->birthdate = Input::get('birthdate');
		}
		
		
		$user->bio = Input::get('bio');


		if ($file = Input::file('picture')) {
			$file = Input::file('picture');
			$ext = $file->getClientOriginalExtension();
			$name = "image".time()."user".".".$ext;
			$file->move(public_path()."/images/", $name);
			$user->picture = $name;
		}
		
		//return Response::json($user);
		$user->save();
		return Redirect::to('users/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();
		return Redirect::to('users');
	}

	public function validate_user($id,$code)
	{
		$user = User::find($id);
		

		if($user)
		{ 
				
			if($code == $user->code)
			{
				
				$user->validated = true;
				$user->code = null;
				$user->save();
			
			
				Mail::send('emails.welcome', array('name' => $user->user_name, 'email' => $user->email), function($message) use ($user)
				{	
					$message->from('no-reply@eposak.org','Eposak.org');
		    		$message->to(  $user->email , 'eposak')->subject('Bienvenido a Eposak');

				});


         	return Redirect::route('home')->with('message','su usuario fue validado');

			}else{
				
					return  View::make('codenotvalid');


			}

		}else{


         
         return Redirect::route('home')->with('message','No se existe el usuario');
		}
	}

}
