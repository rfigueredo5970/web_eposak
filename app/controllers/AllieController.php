<?php

class AllieController extends \BaseController {
	
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$allies = Allie::all();
		return View::make('allies.index')->with('allies', $allies);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('allies.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'url_page' => 'required|min:3;',
			'name' => 'required|min:3',
			'image' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$allies = new Allie;
		$allies->url_page = Input::get('url_page'); 
		$allies->name = Input::get('name');
		 
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."allies".".".$ext;
		$file->move(public_path()."/images/", $name);
		$allies->image = $name;
		
		if ($allies->save()) {
			return Redirect::to('allies');  
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$allies = Allie::find($id);
		return View::make('allies.show')->with('allies', $allies);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$allies = Allie::find($id);
		return View::make('allies.edit')->with('allies', $allies);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'url_page' => 'required|min:3;',
			'name' => 'required|min:3',
			'image' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$allies = Allie::find($id);
		$allies->url_page = $input['url_page']; 
		$allies->name = $input['name']; 	 
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time().".".$ext;
		$file->move(public_path()."/images/", $name);
		$allies->image = $name;

		
		
		if ($allies->save()) { 
			return Redirect::to('allies/'.$id);
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$allies = Allie::find($id);
		$allies->delete();
		return Redirect::to('allies');
	}


}
