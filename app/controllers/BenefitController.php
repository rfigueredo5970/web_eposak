<?php

class BenefitController extends \BaseController {


	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$benefit = Benefit::with('project')->get();
		return View::make('benefit.index')->with('benefit', $benefit);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$project = Project::lists('title_es', 'id');
		return View::make('benefit.create')->with('project',$project);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.'
		);

		$rules = array(

			'project_id' => 'required',
			'description_es' => 'required|min:5',
			'description_en' => 'required|min:5'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$benefit = new Benefit;
		$benefit->description_es = Input::get('description_es'); 
		$benefit->description_en = Input::get('description_en');
		$benefit->project_id = Input::get('project_id'); 

		
		if ($benefit->save()) {
			return Redirect::to('benefit'); 
		}else{
	      
	     	return Redirect::back()->withErrors($validate);
		}


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$benefit = Benefit::with('project')->find($id);
		return View::make('benefit.show')->with('benefit', $benefit);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$benefit = Benefit::find($id);
		$project = Project::lists('title_es', 'id');
		return View::make('benefit.edit')->with('benefit', $benefit)->with('project',$project);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.'
		);

		$rules = array(

			'project_id' => 'required',
			'description_es' => 'required|min:5',
			'description_en' => 'required|min:5'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$benefit = Benefit::find($id);
		$benefit->description_es = $input['description_es']; 
		$benefit->description_en = $input['description_en'];
		$benefit->project_id = $input['project_id']; 

		$benefit->save();
		return Redirect::to('benefit');
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$benefit = Benefit::find($id);
		$benefit->delete();
		return Redirect::to('benefit');
	}


}
