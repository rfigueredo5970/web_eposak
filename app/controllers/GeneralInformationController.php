<?php

class GeneralInformationController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$general = General_information::with('section_navbar')->get();
		return View::make('general_information.index')->with('general', $general);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$navbar = Section_navbar::lists('title_es', 'id');
		return View::make('general_information.create')->with('navbar',$navbar);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'title_es' => 'required|min:3;',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:1',
			'description_en' => 'required|min:1',
			'url_multimedia' => 'min:5',
			'section_navbar_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		};

		$general = new General_information;
		$general->title_es = Input::get('title_es'); 
		$general->title_en = Input::get('title_en'); 
		$general->description_es = Input::get('description_es'); 
		$general->description_en = Input::get('description_en'); 
		$general->section_navbar_id = Input::get('section_navbar_id');
		$general->url_multimedia = Input::get('url_multimedia');
		
		if ($general->save()) {
			return Redirect::to('generalinformation');
		}else{
	      
	      	return Redirect::back()->withErrors($validate);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$general = General_information::with('section_navbar')->find($id);
		return View::make('general_information.show')->with('general', $general);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$general = General_information::find($id);
		$navbar = Section_navbar::lists('title_es', 'id');
		return View::make('general_information.edit')->with('general', $general)->with('navbar',$navbar);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'title_es' => 'required|min:3;',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:1',
			'description_en' => 'required|min:1',
			'url_multimedia' => 'min:5',
			'section_navbar_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		};

		$input = Input::all();
		$general = General_information::find($id);
		$general->title_es = $input['title_es']; 
		$general->title_en = $input['title_en']; 
		$general->description_es = $input['description_es']; 
		$general->description_en = $input['description_en'];
		$general->url_multimedia = $input['url_multimedia'];
		$general->section_navbar_id = $input['section_navbar_id'];

		$general->save();
		return Redirect::to('generalinformation');


	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$general = General_information::find($id);
		$general->delete();
		return Redirect::to('generalinformation');
	}


}
