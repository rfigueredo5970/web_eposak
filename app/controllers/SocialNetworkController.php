<?php

class SocialNetworkController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$sotial = Social_networks::all();
		return View::make('sotial.index')->with('sotial', $sotial);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sotial.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'email' => 'required|email',
			'icon' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$sotial = new Social_networks;
		$sotial->name = Input::get('name');
		$sotial->email = Input::get('email');
		 
		$file = Input::file('icon');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."icon".".".$ext;
		$file->move(public_path()."/images/", $name);
		$sotial->icon = $name;
		
		if ($sotial->save()) {
			return Redirect::to('sotialred');  
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$sotial = Social_networks::find($id);
		return View::make('sotial.show')->with('sotial', $sotial);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$sotial = Social_networks::find($id);
		return View::make('sotial.edit')->with('sotial', $sotial);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'email' => 'required|email',
			'name' => 'required|min:3',
			'icon' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$sotial = Social_networks::find($id); 
		$sotial->name = $input['name'];	 
		$sotial->email = $input['email'];	 
		$file = Input::file('icon');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."icon".".".$ext;
		$file->move(public_path()."/images/", $name);
		$sotial->icon = $name;

		
		
		if ($sotial->save()) { 
			return Redirect::to('sotialred/'.$id);
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sotial = Social_networks::find($id);
		$sotial->delete();
		return Redirect::to('sotialred');
	}


}
