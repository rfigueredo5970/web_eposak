<?php

class SliderController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$slider = Slider::all();
		return View::make('slider.index')->with('slider', $slider);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('slider.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'name' => 'required|min:3'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$slider = new Slider;
		$slider->name = Input::get('name');
		 
		$file = Input::file('url');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."url".".".$ext;
		$file->move(public_path()."/images/", $name);
		$slider->url = $name;
		
		if ($slider->save()) {
			return Redirect::to('slider');  
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$slider = Slider::find($id);
		return View::make('slider.show')->with('slider', $slider);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$slider = Slider::find($id);
		return View::make('slider.edit')->with('slider', $slider);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			
			'name' => 'required|min:3'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$slider = Slider::find($id); 
		$slider->name = $input['name'];	 
		$file = Input::file('url');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."url".".".$ext;
		$file->move(public_path()."/images/", $name);
		$slider->url = $name;

		
		
		if ($slider->save()) { 
			return Redirect::to('slider/'.$id);
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$slider = Slider::find($id);
		$slider->delete();
		return Redirect::to('sliderred');
	}


}
