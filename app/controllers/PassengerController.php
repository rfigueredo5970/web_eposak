<?php

class PassengerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('auth',array('only' => ['index','show','edit','create']));
		$this->beforeFilter('admin',array('only' => ['index','show','edit','create']));
	}
	public function index()
	{
		$passenger = Passenger::with('travels')->get();
		return View::make('passenger.index')->with('passenger', $passenger);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$travel = Travel::lists('title_es', 'id');
		return View::make('passenger.create')->with('travel',$travel);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'name' => 'required|min:3;',
			'email' => 'required|min:3',
			'phone_number' => 'required|numeric',
			'travels_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}


		$passenger = new Passenger;
		$passenger->name = Input::get('name');
		$passenger->last_name = Input::get('last_name');
		$passenger->email = Input::get('email'); 
		$passenger->phone_number = Input::get('phone_number');
		$passenger->travels_id = Input::get('travels_id'); 
         
         $travel = Travel::find(Input::get('travels_id'));




			$data = array('name' => $passenger->name, 'last_name'=>$passenger->last_name ,'email' => $passenger->email, 'phone_number' => $passenger->phone_number ,'title_es' => $travel->title_es, 'title_en' => $travel->title_en, 'start_at' =>  $travel->start_at, 'end_at' => $travel->end_at, 'status' => $travel->status, 'days' => $travel->days, "nights" => $travel->nights, 'prices' => $travel->prices, 'destiny' => $travel->destiny->name_es  );


			
			Mail::send('emails.travel_reservation', $data , function($message) use ($data)
		{
		    $message->to(  $_ENV['MAIL_CONTACT'] , 'eposak')->subject('Reserva de Viaje');

		});
			
		

		
		if ($passenger->save()) {
			return Redirect::to('/')->with('message','Su reserva se ha generado exitosamente');
		}


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$passenger = Passenger::with('travels')->find($id);
		return View::make('passenger.show')->with('passenger', $passenger);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$passenger = Passenger::find($id);
		$travel = Travel::lists('title_es', 'id');
		return View::make('passenger.edit')->with('passenger', $passenger)->with('travel',$travel);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'name' => 'required|min:3;',
			'email' => 'required|min:3',
			'phone_number' => 'required|numeric',
			'travels_id' => 'required|min:1'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}



		$input = Input::all();
		$passenger = Passenger::find($id);
		$passenger->name = $input['name']; 
		$passenger->email = $input['email']; 
		$passenger->phone_number = $input['phone_number'];
		$passenger->travels_id = $input['travels_id'];
		$passenger->save();
		
		return Redirect::to('passenger');



	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$passenger = Passenger::find($id);
		$passenger->delete();
		return Redirect::to('passenger');
	}


}
