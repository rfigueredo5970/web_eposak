<?php

class DetailsPageController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$page = Details_page::all();
		return View::make('detail_page.index')->with('page', $page);	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('detail_page.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{


		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Mínimo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'button_1_en' => 'required|min:3|max:50;',
			'button_1_es' => 'required|min:3|max:50;',
			'button_2_en' => 'required|min:3|max:50;',
			'button_2_es' => 'required|min:3|max:50;',
			'button_3_en' => 'required|min:3|max:50;',
			'button_3_es' => 'required|min:3|max:50;',
			'button_4_en' => 'required|min:3|max:50;',
			'button_4_es' => 'required|min:3|max:50;',
			'button_5_en' => 'required|min:3|max:50;',
			'button_5_es' => 'required|min:3|max:50;',
			'legal_rights_en' => 'required|min:10;',
			'legal_rights_es' => 'required|min:10;',
			'logo' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();
			}


		$page = new Details_page;
		$page->button_1_en = Input::get('button_1_en'); 
		$page->button_1_es = Input::get('button_1_es'); 
		$page->button_2_en = Input::get('button_2_en'); 
		$page->button_2_es = Input::get('button_2_es'); 
		$page->button_3_en = Input::get('button_3_en'); 
		$page->button_3_es = Input::get('button_3_es'); 
		$page->button_4_en = Input::get('button_4_en'); 
		$page->button_4_es = Input::get('button_4_es'); 
		$page->button_5_en = Input::get('button_5_en'); 
		$page->button_5_es = Input::get('button_5_es'); 
		$page->legal_rights_en = Input::get('legal_rights_en'); 
		$page->legal_rights_es = Input::get('legal_rights_es'); 
		$file = Input::file('logo');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time().".".$ext;
		$file->move(public_path()."/images/", $name);
		$page->logo = $name;
		$page->save();

		return Redirect::to('page');  
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$page = Details_page::find($id);
		return View::make('detail_page.show')->with('page', $page);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Details_page::find($id);
		return View::make('detail_page.edit')->with('page', $page);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{


		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Mínimo :max carácteres.'
		);

		$rules = array(

			'button_1_en' => 'required|min:3|max:50;',
			'button_1_es' => 'required|min:3|max:50;',
			'button_2_en' => 'required|min:3|max:50;',
			'button_2_es' => 'required|min:3|max:50;',
			'button_3_en' => 'required|min:3|max:50;',
			'button_3_es' => 'required|min:3|max:50;',
			'button_4_en' => 'required|min:3|max:50;',
			'button_4_es' => 'required|min:3|max:50;',
			'button_5_en' => 'required|min:3|max:50;',
			'button_5_es' => 'required|min:3|max:50;',
			'legal_rights_en' => 'required|min:10;',
			'legal_rights_es' => 'required|min:10;',
			'logo' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();
			}




		$input = Input::all();
		$page = Details_page::find($id);
		$page->button_1_en  = $input['button_1_en']; 
		$page->button_1_es  = $input['button_1_es']; 
		$page->button_2_en  = $input['button_2_en']; 
		$page->button_2_es  = $input['button_2_es']; 
		$page->button_3_en  = $input['button_3_en']; 
		$page->button_3_es  = $input['button_3_es']; 
		$page->button_4_en  = $input['button_4_en']; 
		$page->button_4_es  = $input['button_4_es']; 
		$page->button_5_en  = $input['button_5_en']; 
		$page->button_5_es  = $input['button_5_es']; 
		$page->legal_rights_en  = $input['legal_rights_en']; 
		$page->legal_rights_es  = $input['legal_rights_es']; 
		$file = Input::file('logo');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time().".".$ext;
		$file->move(public_path()."/images/", $name);
		$page->logo = $name;
		$page->save();
		return Redirect::to('page/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Details_page::find($id);
		$page->delete();
		return Redirect::to('page');
	}


}
