<?php

class SectionNavbarController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$navbar = Section_navbar::all();
		return View::make('section_navbar.index')->with('navbar', $navbar);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('section_navbar.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'title_es' => 'required|min:3',
			'title_en' => 'required|min:3',
			'description_es' => 'min:50',
			'description_en' => 'min:50',
			'url_section' => 'min:3',
			'image' => 'mimes:jpeg,jpg,png,jpg',
			'imagen_2' => 'mimes:jpeg,jpg,png,jpg'
		);

		$validate = Validator::make(Input::all(), $rules, $messages);
			if ($validate->fails()) {
				return Redirect::back()->withErrors($validate)->withInput();
			};

			

		$navbar = new Section_navbar;
		$navbar->name = Input::get('name'); 
		$navbar->title_es = Input::get('title_es'); 
		$navbar->title_en = Input::get('title_en'); 
		$navbar->description_es = Input::get('description_es'); 
		$navbar->description_en = Input::get('description_en'); 
		$navbar->url_section = Input::get('url_section'); 
		
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time()."navbar".".".$ext;
		$file->move(public_path()."/images/", $name);
		$navbar->image = $name;
		
		if ($file = Input::file('imagen_2')) {
			$file = Input::file('imagen_2');
			$ext = $file->getClientOriginalExtension();
			$name = "image".time()."navbar".".".$ext;
			$file->move(public_path()."/images/", $name);
			$navbar->imagen_2 = $name;
		}

		$navbar->save();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$navbar = Section_navbar::find($id);
		return View::make('section_navbar.show')->with('navbar', $navbar);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$navbar = Section_navbar::find($id);
		return View::make('section_navbar.edit')->with('navbar', $navbar);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'title_es' => 'required|min:3',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:50',
			'description_en' => 'required|min:50',
			'url_section' => 'required|min:3',
			'image' => 'mimes:jpeg,jpg,png,JPG,gif'
		);

		$validate = Validator::make(Input::all(), $rules, $messages);
			if ($validate->fails()) {
				return Redirect::back()->withErrors($validate)->withInput();
			};


		$input = Input::all();
		$navbar = Section_navbar::find($id);
		$navbar->name = $input['name']; 
		$navbar->title_es = $input['title_es']; 
		$navbar->title_en = $input['title_en']; 
		$navbar->description_es = $input['description_es']; 
		$navbar->description_en = $input['description_en'];		 
		$navbar->url_section = $input['url_section'];
		if ($file = Input::file('image')) {		 
		$file = Input::file('image');
		$ext = $file->getClientOriginalExtension();
		$name = "image".time().".".$ext;
		$file->move(public_path()."/images/", $name);
		$navbar->image = $name;
		}
		if ($file = Input::file('imagen_2')) {
			$file = Input::file('imagen_2');
			$ext = $file->getClientOriginalExtension();
			$name = "image".time()."navbar".".".$ext;
			$file->move(public_path()."/images/", $name);
			$navbar->imagen_2 = $name;
		}
		

		
		
		if ($navbar->save()) { 
			return Redirect::to('navbar/'.$id);
			}else{
	      
	      return Redirect::back()->withErrors($validate);
			}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/**
	public function destroy($id)
	{
		$navbar = Section_navbar::find($id);
		$navbar->delete();
		return Redirect::to('navbar');
	}
	*/


}
