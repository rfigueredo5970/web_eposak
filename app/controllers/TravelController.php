<?php

class TravelController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{

		$travel = Travel::with('destiny','travel_type','multimedias','itinerary')->get();
		return View::make('travel.index')->with('travel', $travel);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$destiny = Destiny::lists('name_es', 'id');
		$travel_type = Travel_type::lists('name_es','id');
		$media = Multimedia::lists('id','multimediable_id');
		return View::make('travel.create')->with( 'destiny',$destiny )->with('travel_type',$travel_type)->with('media',$media);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'after' => '*La Fecha de Finalizar el viaje no puede ser anterior a la Fecha de Comenzar',
			'unique' => ' Ya existe un viaje con este nombre. Por favor intente con otro',
			'numeric' => 'El número telefónico solo debe contener dígitos.'

		);

		$rules = array(

			'title_es' => 'unique:travels|required|min:3',
			'title_en' => 'required|min:3',
			'description_en' => 'required|min:50',
			'description_es' => 'required|min:50',
			'recommendation_es' => 'required',
			'recommendation_en' => 'required',
			'include_es' => 'required',
			'include_en' => 'required',
			'dont_include_es' => 'required',
			'dont_include_en' => 'required',
			'start_at' => 'required|date',
			'end_at' => 'required|date',
			'prices' => 'required',
			'travel_type_id' => 'required',
			'destiny_id' => 'required'			
		);


		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}




		$travel = new Travel;
		$travel->travel_type_id = Input::get('travel_type_id');
		$travel->destiny_id = Input::get('destiny_id');
		$travel->title_en = Input::get('title_en');
		$travel->title_es = Input::get('title_es'); 
		$travel->description_en = Input::get('description_en'); 
		$travel->description_es = Input::get('description_es'); 
		$travel->recommendation_es = Input::get('recommendation_es'); 
		$travel->recommendation_en = Input::get('recommendation_en'); 
		$travel->dont_include_es = Input::get('include_es'); 
		$travel->include_es = Input::get('include_es'); 
		$travel->include_en = Input::get('include_en'); 
		$travel->dont_include_es = Input::get('dont_include_es'); 
		$travel->dont_include_en = Input::get('dont_include_en'); 
		$travel->start_at = Input::get('start_at'); 
		$travel->end_at = Input::get('end_at'); 
		$travel->days = Input::get('days'); 
		$travel->nights = Input::get('nights');
		$travel->prices = Input::get('prices');
		$travel->slug = $travel->createSlug(Input::get('title_es'));
		
		$date = new DateTime($travel->start_at) ;
		$date2 = new DateTime( $travel->end_at ); 
		$date->setTimezone(new DateTimeZone('GMT')); 
		$interval = $date->diff( $date2 );
		$travel->days = $interval->format('%a');
		
		if (time($travel->end_at) < '13:00:00 GMT') {
			$travel->nights = $interval->format('%a');
			$travel->nights = $travel->nights -1;
		}else{
			$travel->nights = $interval->format('%a');
		}

		$travel->save();
		$files = Input::file('name');
		if (is_array($files) || is_object($files))
		{
			foreach ( $files as $file){

				$multimedia = new Multimedia;
				$ext = $file->getClientOriginalName();
				$name = "image".time()."multi".".".$ext;
				$file->move(public_path()."/images/",$name);
				$multimedia->name = $name;
				$travel->multimedias()->save($multimedia);
				//var_dump(sizeof($multimedia));
			};
		}
		return Redirect::to('travel');




	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$travel =  Travel::with('multimedias','travel_type','destiny')->find($id);
		return View::make('travel.show')->with('travel', $travel);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function edit($id)
	{
		$travel = Travel::find($id);
		$destiny = Destiny::lists('name_es', 'id');
		$travel_type = Travel_type::lists('name_es','id');
		$media = Multimedia::lists('id','multimediable_id');
		return View::make('travel.edit')->with('travel',$travel )->with( 'destiny',$destiny )->with('travel_type',$travel_type)->with('media',$media);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$messages = array(
			'required' => 'Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => 'Debe ingresar una extencion de imagen válida.',
			'unique' => ' Ya existe un viaje con este nombre. Por favor intente con otro',
			'numeric' => 'El número telefónico solo debe contener dígitos.'
		);

		$rules = array(

			'title_es' => 'required|min:3',
			'title_en' => 'required|min:3',
			'description_en' => 'required|min:50',
			'description_es' => 'required|min:50',
			'recommendation_es' => 'required',
			'recommendation_en' => 'required',
			'include_es' => 'required',
			'include_en' => 'required',
			'dont_include_es' => 'required',
			'dont_include_en' => 'required',
			'start_at' => 'required',
			'end_at' => 'required|after:start_at',
			'prices' => 'required',
			'travel_type_id' => 'required',
			'destiny_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$travel = Travel::find($id);
		$travel->title_es = $input['title_es']; 
		$travel->title_en = $input['title_en']; 
		$travel->description_es = $input['description_es']; 
		$travel->description_en = $input['description_en'];
		$travel->recommendation_es = $input['recommendation_es'];
		$travel->recommendation_en = $input['recommendation_en'];
		$travel->include_es = $input['include_es'];
		$travel->include_en = $input['include_en'];
		$travel->dont_include_es = $input['dont_include_es'];
		$travel->dont_include_en = $input['dont_include_en'];
		$travel->start_at = $input['start_at'];
		$travel->end_at = $input['end_at'];
		$travel->prices = $input['prices'];
		$travel->travel_type_id = $input['travel_type_id'];
		$travel->destiny_id = $input['destiny_id'];
		$travel->save();
		$files = Input::file('name');
		if (is_array($files) || is_object($files))
		{
			foreach ( $files as $file){

				$multimedia = new Multimedia;
				$ext = $file->getClientOriginalName();
				$name = "image".time()."multi".".".$ext;
				$file->move(public_path()."/images/",$name);
				$multimedia->name = $name;
				$travel->multimedias()->save($multimedia);
				// if($travel->multimedias()->save($multimedia)){
				// 	$name_img = DB::table('multimedia')->where('multimediable_type', 'travel')->where('multimediable_id', $travel->id)->first();
				// 	$url = public_path()."/images/";
				// 	File::delete($url.$name_img->name);
				// }
				//var_dump(sizeof($multimedia));
			};
		}
		$travel->multimedias()->save($multimedia);

		

		return Redirect::to('travel');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$travel = Travel::find($id);
		$travel->delete();
		return Redirect::to('travel');
	}


}
