<?php

class ItineraryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$itinerary = Itinerary::with('travel')->get();
		return View::make('itinerary.index')->with('itinerary', $itinerary);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	

	public function getDateTravel($id)
	{
		$date = Travel::find($id);
		return Response::json($date);
	}
 
	public function create()
	{
		$travel = Travel::all();

		return View::make('itinerary.create')->with('travel',$travel);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$messages = array(
			
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.',
			'date_format' => '*El formato Requerido es de horas y minutos Ejemplo(00:00)',
			'after' => '*La hora de Finalizar la Actividad no puede ser anterior a la hora de Comenzar',
			'unique_with' => '*aksdjalskdjalskdj',
			'unique' => '*ya existe un registro con esta fecha y hora'
		);

		$rules = array(

			
			'date' => 'required',
			'start_at' => 'required|date_format: H:i|unique_with:itinerary,travel_id,day',
			'end_at' => 'required|date_format: H:i|after:start_at|',
			'description_es' => 'required|min:10',
			'travel_id' => 'required',
			"concat_uni" => "unique",
		);
		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$itinerary = new Itinerary;
		$itinerary->travel_id = Input::get('travel_id');
		$itinerary->date = Input::get('date');


		$start = $itinerary->date.Input::get('start_at');
		$itinerary->start_at = date("Y-m-d  H:i", strtotime($start));
		
		$end = $itinerary->date.Input::get('end_at');
		$itinerary->end_at = date("Y-m-d  H:i", strtotime($end));

		
		$date1=date_create($itinerary->start_at);
		$date2=date_create($itinerary->travel->start_at);
		$diff=date_diff($date2,$date1);

		$itinerary->day = $diff->format("%d");

		$itinerary->description_es = Input::get('description_es');
		$itinerary->save();
		return Response::json($itinerary);
		
		/**
		$inicio = $itinerary->travels->start_at;
		$fin = $itinerary->travels->end_at;

		$fechaaamostar = $inicio;
		while(strtotime($fin) >= strtotime($inicio))
		{
			if(strtotime($fin) != strtotime($fechaaamostar))
			{
				echo "$fechaaamostar<br />";
				$fechaaamostar = date("Y-m-d", strtotime($fechaaamostar ." + 1 day"));
			}
			else
			{
				echo "$fechaaamostar <br />";
				break;
			}
		}
		echo "<br/>".$inicio;
		echo "<br/>".$fin;
		echo "<br/>".$itinerary;
		*/


		/**
		$itinerary = new Itinerary;
		$itinerary->date = Input::get('date');
		$itinerary->description_en = Input::get('description_en');
		$itinerary->description_es = Input::get('description_es');
		$itinerary->start_at = Input::get('start_at'); 
		$itinerary->end_at = Input::get('end_at');
		$itinerary->travels_id = Input::get('travels_id'); 

		if ($itinerary->save()) {
			return Redirect::to('itinerary'); 
		}else{
	      
	     	return Redirect::back()->withErrors($validate);
		}
		*/
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$itinerary = Itinerary::with('travel')->find($id);
		return View::make('itinerary.show')->with('itinerary', $itinerary);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$itinerary = Itinerary::find($id);
		$travel = Travel::lists('title_es', 'id');
		return View::make('itinerary.edit')->with('itinerary', $itinerary)->with('travel',$travel);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'date' => 'required;',
			'start_at' => 'required',
			'end_at' => 'required',
			'description_en' => 'required|min:30',
			'description_es' => 'required|min:30',
			'travels_id' => 'required'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}


		$input = Input::all();
		$itinerary = Itinerary::find($id);
		$itinerary->date = $input['date']; 
		$itinerary->description_en = $input['description_en']; 
		$itinerary->description_es = $input['description_es']; 
		$itinerary->start_at = $input['start_at']; 
		$itinerary->description_en = $input['description_en'];
		$itinerary->travel_id = $input['travels_id'];
		$itinerary->save();
		
		return Redirect::to('itinerary');
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$itinerary = Itinerary::find($id);
		$itinerary->delete();
		return Redirect::to('itinerary');
	}


}
