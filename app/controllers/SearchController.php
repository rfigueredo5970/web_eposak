<?php

use Illuminate\Support\Facades\Paginator;

use Illuminate\Support\Collection as Collection;


class SearchController extends \BaseController {
	
	private $data = array();
	private $data_encoded = array();
	
	private function Busca($clave)
	{

							// BUSQUEDA DE DESTINOS

					$destiny = Destiny::where('name_es', 'LIKE', '%'.$clave.'%')->get();
					// Consulta de destinos en idioma español

					if($destiny)
					{
						foreach ($destiny as $item) {
							array_push($this->data, [	'title' => $item->name_es, 
																				'description' => $item->description_es,
																				'url' => url('/destino/' . $item->id)
																			]);
						}
					}

					$destiny = Destiny::where('name_en', 'LIKE', '%'.$clave.'%')->get();
						// Consulta de destinos en idioma ingles

					if($destiny)
					{
						foreach ($destiny as $item) {
							array_push($this->data, [	'title' => $item->name_en, 
																				'description' => $item->description_en,
																				'url' => url('/destino/' . $item->id)
																			]);
						}
					}

					// BUSQUEDA DE NOTICIAS

					$news = Today_esposak::where('title_es', 'LIKE', '%'.$clave.'%')->get();
						// Consulta de noticias en idioma español

					if($news)
					{
						foreach ($news as $item) {
							array_push($this->data, [	'title' => $item->title_es,
																				'description' => $item->description_es, 
																				'url' => url('/noticia/' . $item->id)
																			]);
						}
					}

					$news = Today_esposak::where('title_en', 'LIKE', '%'.$clave.'%')->get();
						// Consulta de noticias en idioma ingles

					if($news)
					{
						foreach ($news as $item) {
							array_push($this->data, [	'title' => $item->title_en,
																				'description' => $item->description_en, 
																				'url' => url('/noticia/' . $item->id)
																			]);
						}
					}

					// BUSQUEDA DE TIPOS DE VIAJE (VISTA DE COMUNIDADES)

					$travel_type = Travel_type::where('name_es', 'LIKE', '%'.$clave.'%')->get();
						// Consulta de comunidades en idioma español

					if($travel_type)
					{
						foreach ($travel_type as $item) {
							array_push($this->data, [	'title' => $item->name_es,
																				'description' => $item->description_es, 
																				'url' => url('/viajes/' . $item->id)
																			]);
						}
					}

					$travel_type = Travel_type::where('name_en', 'LIKE', '%'.$clave.'%')->get();
					// Consulta de comunidades en idioma ingles


					if($travel_type)
					{
						foreach ($travel_type as $item) {
							array_push($this->data, [	'title' => $item->name_en,
																				'description' => $item->description_en, 
																				'url' => url('/viajes/' . $item->id)
																			]);
						}
					}
				
			return $this->data;
	}
	
	
	
	public function search()
	{

		$clave = $_POST["clave"];
        
		if(empty($clave))
		{
			$data = array( "status" => "failed" , "message" => "ingrese palabra a buscar." );
                return Response::json( $data , 500 , [ ] , JSON_UNESCAPED_UNICODE );
		}
		else
		{
			 $datSend = $this->Busca($clave);
			if(count($datSend) > 0)
			{
				
				 $perPage = 3;
				 $totalItems = count($datSend);
				 $paginator = Paginator::make($datSend , $totalItems, $perPage);
				  $paginator_json = $paginator->toJson();
				$data = array( "status" => "success" , "message" => "Resultado de ".$clave,"data"=>	$paginator_json );
                return Response::json( $data , 200 , [ ] , JSON_UNESCAPED_UNICODE );
			}
			else{
				$data = array( "status" => "failed" , "message" => "No hay resultado de ".$clave);
                return Response::json( $data , 404 , [ ] , JSON_UNESCAPED_UNICODE );
			}
			
		}
		
        
		
	}
	


}
