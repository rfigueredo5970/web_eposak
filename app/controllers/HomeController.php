<?php

class HomeController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		$travel_experienciales = Travel::with('travel_type','multimedias')->where('travel_type_id','1')->get();
		$t_ex = $travel_experienciales->chunk(3);
		
		$travel_voluntariados = Travel::with('travel_type','multimedias')->where('travel_type_id','2')->get();
		$t_vo = $travel_voluntariados->chunk(3);

		$destiny = Destiny::all();

		/**
							**/
		return View::make('hello')
							->with('t_ex',$t_ex)
							->with('t_vo',$t_vo)
							->with('destiny',$destiny)
							;
		//echo $destiny;
		//echo($t_vo[0]);
		//echo $t_ex[0];
	}

}
