<?php

class EntrepreneurController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function __construct()
	{
		$this->beforeFilter('admin');
	}


	public function index()
	{
		$entrepreneur = Entrepreneur::with('project')->get();
		return View::make('entrepeneur.index')->with('entrepreneur', $entrepreneur);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$project = Project::where('type','emprendimiento')->lists('title_es', 'id');
		return View::make('entrepeneur.create')->with('project',$project);
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.'
		);

		$rules = array(

			'project_id' => 'required',
			'name' => 'required|min:3',
			'last_name' => 'required|min:3',
			'description_es' => 'required|min:3',
			'description_en' => 'required|min:3'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$entrepreneur = new Entrepreneur;
		$entrepreneur->name = Input::get('name'); 
		$entrepreneur->last_name = Input::get('last_name');
		$entrepreneur->description_es = Input::get('description_es');
		$entrepreneur->description_en = Input::get('description_en');
		$entrepreneur->project_id = Input::get('project_id');
		if (Input::file('image') > 0) {
			
			$file = Input::file('image');
			$ext = $file->getClientOriginalExtension();
			$name = $file->getClientOriginalName().time().$ext;
			$file->move(public_path()."/images/", $name);
			$entrepreneur->image = $name;
		}


		
		if ($entrepreneur->save()) {
			return Redirect::to('entrepeneur'); 
		}else{
	      
	     	return Redirect::back()->withErrors($validate);
		}


	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$entrepreneur = Entrepreneur::with('project')->find($id);
		return View::make('entrepeneur.show')->with('entrepreneur', $entrepreneur);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$entrepreneur = Entrepreneur::find($id);
		$project = Project::where('type','emprendimiento')->lists('title_es', 'id');
		return View::make('entrepeneur.edit')->with('entrepreneur', $entrepreneur)->with('project',$project);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.'
		);

		$rules = array(

			'project_id' => 'required',
			'name' => 'required|min:3',
			'last_name' => 'required|min:3',
			'description_es' => 'required|min:3',
			'description_en' => 'required|min:3'
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$entrepreneur = Entrepreneur::find($id);
		$entrepreneur->name = $input['name']; 
		$entrepreneur->last_name = $input['last_name'];
		$entrepreneur->project_id = $input['project_id']; 
		$entrepreneur->description_es = $input['description_es']; 
		$entrepreneur->description_en = $input['description_en']; 

		if (Input::file('image') > 0) {
			
			$file = Input::file('image');
			$ext = $file->getClientOriginalExtension();
			$name = $file->getClientOriginalName().time().$ext;
			$file->move(public_path()."/images/", $name);
			$entrepreneur->image = $name;
		}

		$entrepreneur->save();
		return Redirect::to('entrepeneur');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$entrepreneur = Entrepreneur::find($id);
		$entrepreneur->delete();
		return Redirect::to('entrepeneur');
	}


}
