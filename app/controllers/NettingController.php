<?php

class NettingController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('admin');
	}
	public function index()
	{
		$netting = Netting::all();
		return View::make('netting.index')->with('netting', $netting);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('netting.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'cuenta' => 'min:11',
			'icon' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$netting = new Netting;
		$netting->name = Input::get('name'); 
		$netting->cuenta = Input::get('cuenta'); 
		$netting->icon = Input::get('icon');
		$netting->save();
		return Redirect::to('netting');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$netting = Netting::find($id);
		return View::make('netting.show')->with('netting', $netting);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$netting = Netting::find($id);
		return View::make('netting.edit')->with('netting', $netting);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
			'required' => '*Éste campo es obligatorio.',
			'min' => '*Mínimo :min carácteres.',
			'max' => '*Maximo :max carácteres.',
			'mimes' => '*Debe ingresar una extencion de imagen válida.',
			'numeric' => '*Es nesesario que ingrese carecteres numericos.'
		);

		$rules = array(

			'name' => 'required|min:3',
			'cuenta' => 'min:11',
			'icon' => 'required'
			
		);

		
		$validate = Validator::make(Input::all(), $rules, $messages);
		if ($validate->fails()) {
			return Redirect::back()->withErrors($validate)->withInput();

		}

		$input = Input::all();
		$netting = Netting::find($id);
		$netting->name = $input['name']; 
		$netting->cuenta = $input['cuenta']; 
		$netting->icon = $input['icon'];
		$netting->save();
		return Redirect::to('netting/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$netting = Netting::find($id);
		$netting->delete();
		return Redirect::to('netting');
	}


}
