<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageableIdToMultimedia extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('multimedia', function($table)
		{
    		

		});	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('multimedia', function($table) {

        	$table->dropColumn('imageable_id');
        	$table->dropColumn('imageable_type');

    	});
	}

}
