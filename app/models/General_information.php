<?php
class General_information extends Eloquent {
	
	protected $table = 'general_information';

	 public function section_navbar()
    {
        return $this->belongsTo('Section_navbar');
    }

}