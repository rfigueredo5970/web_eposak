<?php
class Project extends Eloquent {
	
	protected $table = 'projects';


    protected $fillable = array('title_en', 'title_es', 'description_en','description_es','total_cost','final_project_es','final_project_en','objective_es','objective_es','objective_en','country_id','community_benefits_es','community_benefits_es');




public function collaborator()
    {
        return $this->hasmany('collaborator');
    }

	 public function benefit()
    {
        return $this->hasMany('Benefit');
    }

    public function entrepreneurs()
    {
         return $this->hasOne('Entrepreneur');
    }
    
    public function multimedias()
    {
        return $this->morphMany('Multimedia', 'multimediable');
    }
    public function country()
    {
        return $this->belongsTo('Country');
    }
    public function destiny()
    {
        return $this->belongsTo('Destiny');
    }


}