<?php
class Entrepreneur extends Eloquent {
	
	protected $table = 'entrepreneurs';

	public function project()
    {
        return $this->belongsTo('Project');
    }

}