<?php
class Benefit extends Eloquent {
	
	protected $table = 'benefit';

	public function project()
    {
        return $this->belongsTo('Project');
    }

}