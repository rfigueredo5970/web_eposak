<?php
class City extends Eloquent {
	
	protected $table = 'cities';

	 public function state()
    {
        return $this->belongsTo('State');
    }

}