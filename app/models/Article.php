<?php
class Article extends Eloquent {
	
	protected $table = 'article';


	public static function validate($input){

		$rules = array(

			'title_es' => 'required|min:3',
			'title_en' => 'required|min:3',
			'description_es' => 'required|min:50',
			'description_en' => 'required|min:50',
			'image' => 'required|mimes:jpeg,jpg,png,JPG,gif'
		);

		$message = array(
			
			'required' => 'este campo es obligatorio' ,
			'min:3'   => 'la longitud minima debe ser de 3 caracteres',
			'min:50'  =>  'la longitud minima debe ser de 50 caracteres',
			'mimes:jpeg,jpg,png,JPG,gif' => 'solo se aceptan formatos jpeg,jpg,png,JPG,gif',
			'min:1' => 'la longitud minima debe ser 1px',
			'max:300' => 'la longitud maxima de la imagen debe ser de 300px'
			);

		return Validator::make($input,$rules, $message);
	}

}