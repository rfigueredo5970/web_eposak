<?php
class Travel_type extends Eloquent {
	
	protected $table = 'travel_type';

	public function travel()
    {
        return $this->hasMany('Travel');
    }

}