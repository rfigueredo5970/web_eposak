<?php
class Section_navbar extends Eloquent {
	
	protected $table = 'section_navbar';

    public function General_information()
    {
        return $this->hasMany('general_information');
    }
    
	public function bottom_link()
    {
        return $this->hasMany('Bottom_link');
    }

}