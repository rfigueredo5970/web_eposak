<?php
class Country extends Eloquent {
	
	protected $table = 'country';

	public function destiny()
    {
        return $this->hasOne('Travel');
    }
    public function state()
    {
        return $this->hasMany('State');
    }
    public function project()
    {
        return  $this->hasMany('project');
    }

}