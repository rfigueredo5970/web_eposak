<?php
class Itinerary extends Eloquent {
	
	protected $table = 'itinerary';

	public function travel()
    {
       return $this->belongsTo('Travel');
    }
}