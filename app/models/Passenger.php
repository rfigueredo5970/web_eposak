<?php
class Passenger extends Eloquent {
	
	protected $table = 'passengers';

	public function travels()
    {
        return $this->belongsTo('Travel');
    }

}