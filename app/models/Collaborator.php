<?php
class Collaborator extends Eloquent {
	
	protected $table = 'collaborator';

    public function payment_project()
    {
        return $this->hasOne('Payments_project');
    }
    
	public function user()
    {
        return $this->belongsTo('User');
    }
    public function project()
    {
        return $this->belongsTo('Project');
    }
    

}