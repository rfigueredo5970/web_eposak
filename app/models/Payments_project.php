<?php
class Payments_project extends Eloquent {
	
	protected $table = 'payments_project';

	public function project()
    {
        return $this->belongsTo('Project');
    }
    public function collaborator()
    {
        return $this->belongsTo('collaborator');
    }

}