<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::post('prueba', 'Pago123Controller@new_collaborator'); // Verificar datos

Route::post('busqueda', 'SearchController@search'); // Verificar datos
//Route::get('/', 'AuthController@showLoginFree'); // Mostrar login administrador
//Route::get('login/new', 'AuthController@hola'); // Mostrar login

Route::get('/','FreeController@indexFree');

Route::get('/', array('uses' => 'FreeController@indexFree',
                                        'as' => 'home'));

Route::get('home','FreeController@indexFree');

Route::get('eposak', array('uses' => 'FreeController@eposak_section',
                                        'as' => 'eposak'));
Route::get('comunidades', array('uses' => 'FreeController@comunidades',
                                        'as' => 'comunidades'));
Route::get('fundacion', array('uses' => 'FreeController@fundacion',
                                        'as' => 'fundacion'));
Route::get('contacto', array('uses' => 'FreeController@contacto',
                                        'as' => 'contacto'));
Route::get('validate/{id}/{code}',array('uses' => 'UserController@validate_user',
							'as' => 'validate_user'
	));
Route::get('proyectos/{id}', array('uses' => 'FreeController@proyecto_kam',
                                        'as' => 'proyecto_kam'));
Route::get('kamarata', array('uses' => 'FreeController@kamarata',
                                        'as' => 'kamarata'));
Route::get('birongo', array('uses' => 'FreeController@birongo',
                                        'as' => 'birongo'));
Route::get('viajes/{id}', array('uses' => 'FreeController@viajes',
                                        'as' => 'viajes'));

Route::get('viajes/{id}/order/{order}', array('uses' => 'FreeController@viajes',
                                        'as' => 'viajes_order'));

Route::get('masviajes/{id}',array('uses' => 'FreeController@masviajes',
									'as' => 'masviajes'));

Route::get('loadTravelByDestiny/{type_of_travel}/{destiny_id}',array('uses' => 'FreeController@loadTravelByDestiny',
									'as' => 'loadTravelByDestiny'));

Route::get('destino/{slug}', array('uses' => 'FreeController@destino',
                                        'as' => 'destino'));

Route::get('viaje/individual/{id}', array('uses' => 'FreeController@viaje_individual',
                                        'as' => 'viaje_individual'));
Route::get('voluntariado', array('uses' => 'FreeController@voluntariado',
                                        'as' => 'voluntariado'));
Route::get('noticia/{id}', array('uses' => 'FreeController@lonuevo',
                                        'as' => 'noticia'));
Route::get('articulo/{id}', array('uses' => 'FreeController@articulo',
                                        'as' => 'articulo'));

Route::get('exito', array('uses' => 'FreeController@exito',
                                        'as' => 'exito'));

Route::get('proyecto_comunitario/{id}', array('uses' => 'FreeController@proyecto_comunitario',
                                        'as' => 'proyecto_comunitario'));

Route::get('form_viaje/{id}', array('uses' => 'FreeController@form_viaje',
                                        'as' => 'form_viaje'));

Route::post('collaborate', array('uses' => 'Pago123Controller@invoke',
                                        'as' => 'collaborate'));

Route::post('busqueda', 'SearchController@search'); // Metodos de consultas para busqueda

Route::get('login', 'AuthController@showLogin'); // Mostrar login administrador
//Route::get('login/new', 'AuthController@hola'); // Mostrar login

Route::post('login', 'AuthController@postLogin'); // Verificar datos

Route::post('login_ajax', 'AuthController@ajaxLogin');
 
/*Rutas privadas solo para usuarios autenticados*/
Route::resource('passenger','PassengerController');

Route::resource('users','UserController');
Route::post('registration_form','UserController@store_ajax');
Route::resource('contact','ContactController');

Route::group(['before' => 'auth'], function()
{	
	Route::get('form_proyecto/{id}',  array('uses' => 'FreeController@form_proyecto',
	                                        'as' => 'form_proyecto'));
    Route::get('/admin', 'HomeController@showWelcome'); // Vista de inicio
	Route::resource('articles','ArticleController');
	Route::resource('navbar','SectionNavbarController');
	Route::resource('testimony','TestimonyController');
	Route::resource('traveltype','TravelTypeController');
	Route::resource('todayesposak','TodayEsposakController');
	Route::resource('projects','ProjectController');
	Route::resource('changestatus','ProjectController@changeStatus');
	Route::resource('menu','SectionNavbarController');
	Route::resource('page','DetailsPageController');
	Route::resource('destiny','DestinyController');
	Route::resource('bottomlink','Bottom_linkController');
	Route::resource('generalinformation','GeneralInformationController');
	Route::resource('itinerary','ItineraryController');
	Route::resource('benefit','BenefitController');
	Route::resource('allies','AllieController');
	Route::resource('sotialred','SocialNetworkController');  /// ojo social?
	Route::resource('travel','TravelController');
	Route::resource('multimedia','MultimediaController');
	Route::resource('collaborator','CollaboratorController');
	Route::resource('contacto_form','ContController');
	Route::resource('netting','NettingController');
	Route::resource('location','LocationController');
	Route::resource('follow','FollowController');
	Route::resource('slider','SliderController');
	Route::resource('entrepeneur','EntrepreneurController');
	Route::get('datetravel/{id}','ItineraryController@getDateTravel');
	Route::get('end_project','ProjectController@end');
	Route::get('logout', 'AuthController@logOut');

});