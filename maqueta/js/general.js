var centerelige = function (){
    
    var ancho = $(".slidepo").width();    
    var anchodes = $(".eligedestino").width();    
    var resta = (ancho - anchodes)/2;
    
    $(".eligedestino").css("left",resta);
    
    
}

//ancho de img para la seccion rapida de noticia
    
var anchonoti = function (){
    
    var ancho = $(".imgnoti").width();
    var alto = ancho*0.5;

    $(".imgnoti").css("height",alto);
    
} 

var anchogale = function (){
    
    var anchoga = $(".galeimg").width();
    var altoga = anchoga*0.7;
    
    $(".galeimg").css("height",altoga);
}
        
        

$(document).ready(function(){
    
    centerelige();
    anchonoti();
    anchogale();
    
    $(window).resize(function(){
    
       centerelige();
       anchonoti();
       anchogale();
                     
    });
    
    $(".notiarriba").mouseover(function(){
        
        var lovento = $(this).attr("id");
        
        $("#q"+lovento).removeClass("img-dia-cerrado");
        $("#q"+lovento).addClass("img-dia-left");
        
        $("#u"+lovento).removeClass("dia-cerrado");
        $("#u"+lovento).addClass("dia-left");
        
        $("#f"+lovento).css("display","block");
        $("#p"+lovento).css("display","block");
        
    });
    
    $(".notiarriba").mouseout(function(){
        
        var lovento = $(this).attr("id");
        
        $("#q"+lovento).removeClass("img-dia-left");
        $("#q"+lovento).addClass("img-dia-cerrado"); 
        
        $("#u"+lovento).removeClass("dia-left");
        $("#u"+lovento).addClass("dia-cerrado"); 
        
        $("#f"+lovento).css("display","none");
        $("#p"+lovento).css("display","none");
        
    });
    
    $(document).on("click",".fa-angle-down",function(){
                
        $(".eligedestino2").slideToggle();
        
    });

    var a = 0;    
    $(document).on("click",".masymenos",function(){

        var plus = $(this).attr("id");
        
        
        if(a == 0){
            
            $(".s"+plus).text("-");
//            $(".s"+plus).replaceWith("<strong>-</strong>");
//            $(this).find('strong').addClass("s"+plus);
            $("#f"+plus).slideToggle();

            a = 1;
            
        }else{
            
            $(".s"+plus).text("+");
//            $(".s"+plus).replaceWith("<strong>+</strong>");
//            $(this).find('strong').addClass("s"+plus);
            $("#f"+plus).slideToggle();

            a = 0;

        }  

    });
    
    // sciprt para galeria de video
    
   
    
    $(document).on("click",".expand",function(){
        
        var galeimg = $(".galeimg").html();
        
            $(".gvideo").html(galeimg);
        
                    
        
    });
      
    
});